import { useItemPageStyles } from '../../hooks/use-styles';
import {
  Box,
  Container,
  Fade,
  Button as MUIButtom,
  IconButton,
  Paper,
  Typography,
} from '@material-ui/core';
import Alert from '../notifications/Alert';
import Button from '../styled-components/Button';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  selectCurrentUser,
  selectUserAssignments,
} from '../../store/selectors';
import { getUserAssignments } from '../../store/actions/assignment-actions';
import { ChevronLeft, ChevronRight, Event, Home } from '@material-ui/icons';
import { Calendar as BigCalendar, momentLocalizer } from 'react-big-calendar';
import moment from 'moment';
import 'moment/locale/hr';
import 'react-big-calendar/lib/css/react-big-calendar.css';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import { useHistory } from 'react-router';
import { calculateColour } from '../../utils/classroom-theme-colour';
import { STUDENT } from '../../utils/role-names';
import Loading from '../loading/Loading';

const useStyles = makeStyles(theme => ({
  calendar: {
    '& .rbc-header': {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      border: 'none !important',
    },
    '& .rbc-month-view': {
      '& .rbc-event': {
        paddingLeft: '6px !important',
      },
      '& .rbc-month-header': {
        minHeight: 50,
      },
      border: 'none',
      boxShadow:
        '0px 3px 5px -1px rgb(0 0 0 / 20%), 0px 6px 10px 0px rgb(0 0 0 / 14%), 0px 1px 18px 0px rgb(0 0 0 / 12%)',
      borderRadius: 5,
      '& .rbc-month-row': {
        '& .rbc-event': {
          backgroundColor: theme.palette.primary.main,
          '&.rbc-selected, &:focus': {
            outline: 'none',
          },
        },
        borderTop: '1px solid rgba(224, 224, 224, 1)',
      },
      '& .rbc-day-bg + .rbc-day-bg': {
        borderLeft: '1px solid rgba(224, 224, 224, 1)',
      },
      '& .rbc-date-cell': {
        color: theme.palette.text.primary,
        '&.rbc-now': {
          color: theme.palette.primary.main,
          borderBottom: `2px solid ${theme.palette.primary.main}`,
        },
      },
      '& .rbc-day-bg': {
        backgroundColor: theme.palette.secondary.light,
        '&.rbc-off-range-bg': {
          backgroundColor: 'rgba(224, 224, 224, 1)',
        },
      },
    },
    '& .rbc-time-view': {
      border: 'none',
      boxShadow:
        '0px 3px 5px -1px rgb(0 0 0 / 20%), 0px 6px 10px 0px rgb(0 0 0 / 14%), 0px 1px 18px 0px rgb(0 0 0 / 12%)',
      borderRadius: 5,
      '& .rbc-time-header-content': {
        border: 'none',
      },

      '& .rbc-time-header-cell': {
        minHeight: 50,
      },
      '& .rbc-allday-cell': {
        display: 'none',
      },
      '& .rbc-header': {
        backgroundColor: `${theme.palette.primary.main} !important`,
        '&.rbc-today': {
          backgroundColor: theme.palette.secondary.light,
        },
      },
      '& .rbc-time-content, .rbc-timeslot-group': {
        border: 'none',
      },
      '& .rbc-day-slot': {
        '& .rbc-timeslot-group': {
          borderRight: '1px solid rgba(224, 224, 224, 1)',
          borderBottom: '1px solid rgba(224, 224, 224, 1)',
          zIndex: 1,
        },
      },
      '& .rbc-day-slot .rbc-time-slot': {
        backgroundColor: theme.palette.secondary.light,
        borderTop: 'none',
      },
      '& .rbc-event': {
        border: 'none !important',
        zIndex: 200,
        '& .rbc-event-content': {
          paddingTop: 1,
        },
        '& .rbc-event-label': {
          marginLeft: -43,
          marginRight: 7,
          overflow: 'hidden',
        },
      },
      '& .rbc-current-time-indicator': {
        height: 2,
        backgroundColor: theme.palette.primary.main,
      },
    },
  },
}));

const localizer = momentLocalizer(moment);

const CalendarToolbar = ({ views, onNavigate, label, onView }) => {
  const handleSelect = event => {
    onView(event.target.parentNode.value);
  };

  return (
    <Box display={'flex'} justifyContent={'space-between'}>
      <Box>
        <IconButton onClick={() => onNavigate('PREV')}>
          <ChevronLeft style={{ color: '#fff' }} />
        </IconButton>
        <IconButton onClick={() => onNavigate('TODAY')}>
          <Home style={{ color: '#fff' }} />
        </IconButton>
        <IconButton onClick={() => onNavigate('NEXT')}>
          <ChevronRight style={{ color: '#fff' }} />
        </IconButton>
      </Box>

      <Typography
        variant="body1"
        style={{
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'center',
        }}
      >
        {label}
      </Typography>
      <Box minWidth={144}>
        {views.length > 1 &&
          views.map(value => (
            <MUIButtom
              key={value}
              value={value}
              onClick={handleSelect}
              style={{ color: '#fff' }}
            >
              {value === 'month' ? 'MJESEC' : 'TJEDAN'}
            </MUIButtom>
          ))}
      </Box>
    </Box>
  );
};
const Calendar = () => {
  const dispatch = useDispatch();
  const { role } = useSelector(selectCurrentUser);
  const {
    data: assignments,
    loading,
    error,
  } = useSelector(selectUserAssignments);

  useEffect(() => {
    dispatch(getUserAssignments());
  }, [dispatch]);

  const history = useHistory();

  const { title, container, paper, body } = useItemPageStyles();
  const { calendar } = useStyles();

  const eventStyleGetter = event => ({
    style: {
      backgroundColor:
        role.name === STUDENT && event.submitted
          ? '#c4c4c4'
          : calculateColour(
              event.classroom &&
                event.classroom.name.charCodeAt(0) +
                  event.classroom.name.charCodeAt(1),
            ),
      paddingLeft: event.start.getHours() < 10 ? 11 : 5,
    },
  });
  return (
    <Container maxWidth="xl" className={container}>
      <Alert
        message={'Problem tijekom učitavanja zadataka'}
        severity={'error'}
        shouldOpen={!!error}
      />
      <Fade in={true} timeout={500}>
        <Paper className={paper} elevation={8}>
          <Box className={title}>
            <Button icon={<Event />} />
            <Typography variant={'h3'}>Kalendar</Typography>
          </Box>

          <Box
            className={clsx(calendar, body)}
            style={{ padding: '0 40px 40px 40px' }}
          >
            <BigCalendar
              localizer={localizer}
              events={assignments.map(a => ({
                id: a.id,
                title: a.title,
                start: moment(a.activeUntil).toDate(),
                end: moment(a.activeUntil).toDate(),
                classroom: a.classroom,
                submitted: a.submitted,
              }))}
              views={{ month: true, week: true }}
              components={{ toolbar: CalendarToolbar }}
              onSelectEvent={event => {
                history.push(`assignment/${event.id}`);
              }}
              eventPropGetter={eventStyleGetter}
            />
            {loading && <Loading color={'#fff'} minHeight={160} size={70} />}
          </Box>
        </Paper>
      </Fade>
    </Container>
  );
};

export default Calendar;
