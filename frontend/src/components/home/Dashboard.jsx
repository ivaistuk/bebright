import { makeStyles } from '@material-ui/core/styles';
import { Box, Fade } from '@material-ui/core';
import { FixedButton } from '../styled-components/FixedButton';
import { useDispatch, useSelector } from 'react-redux';
import * as ROLES from '../../utils/role-names';
import { useEffect, useMemo, useState } from 'react';

import { Add } from '@material-ui/icons';
import {
  selectClassroomCreatedStatus,
  selectClassroomDeletedStatus,
  selectCurrentUser,
  selectCurrentUserClassrooms,
} from '../../store/selectors';
import Loading from '../loading/Loading';
import Info from '../notifications/Info';
import {
  createClassroom,
  getUserClassrooms,
} from '../../store/actions/classroom-actions';
import ClassroomPreview from '../classroom/components/ClassroomPreview';
import ClassroomForm from '../classroom/forms/ClassroomForm';
import { dashboardAlerts } from '../../utils/notifications/alerts';
import AlertGroup from '../notifications/AlertGroup';

const useStyles = makeStyles(theme => ({
  contaniner: {
    padding: theme.spacing(5),
    display: 'flex',
    justifyContent: ({ items }) => (items > 4 ? 'center' : 'start'),
    flexWrap: 'wrap',
  },
}));

const Dashboard = () => {
  const { role } = useSelector(selectCurrentUser);
  const dispatch = useDispatch();
  const {
    loading: classroomsLoading,
    data,
    error: getClassroomsError,
  } = useSelector(selectCurrentUserClassrooms);
  const { loading: classroomCreating, error: createClassroomError } =
    useSelector(selectClassroomCreatedStatus);

  const { status: classroomDeletedStatus, error: deleteClassroomError } =
    useSelector(selectClassroomDeletedStatus);

  const alerts = useMemo(
    () =>
      dashboardAlerts(
        !!getClassroomsError,
        !!createClassroomError,
        !!deleteClassroomError,
        classroomDeletedStatus,
      ),
    [
      classroomDeletedStatus,
      createClassroomError,
      deleteClassroomError,
      getClassroomsError,
    ],
  );

  useEffect(() => {
    if (!classroomCreating) dispatch(getUserClassrooms());
  }, [classroomCreating, dispatch]);

  const [openForm, setOpenForm] = useState(false);

  const handleOpenForm = () => {
    setOpenForm(true);
  };
  const handleCloseForm = () => {
    setOpenForm(false);
  };

  const handleSubmit = values => {
    const classInfo = {
      name: values.classroom,
      picture: values.picture,
    };

    dispatch(createClassroom(classInfo));
    dispatch(getUserClassrooms());
  };

  const { contaniner } = useStyles({ items: data.length });
  if (classroomsLoading || classroomCreating) return <Loading />;

  return (
    <>
      <Fade in={true} timeout={500}>
        <Box className={contaniner}>
          {data.map(c => (
            <ClassroomPreview
              title={c.name}
              link={`/classroom/${c.id}`}
              key={c.id}
            />
          ))}
          <Info
            message={'Nemate niti jednu aktivnu učionicu'}
            shouldOpen={data && data.length === 0}
          />
          {role.name !== ROLES.STUDENT && (
            <FixedButton
              top="89vh"
              right="3vw"
              onClick={handleOpenForm}
              buttonContent={<Add />}
              title="Stvori učionicu"
            />
          )}
        </Box>
      </Fade>
      <ClassroomForm
        open={openForm}
        closeForm={handleCloseForm}
        onSubmit={handleSubmit}
        formTitle={'Nova učionica'}
        submitTitle={'Napravi učionicu'}
      />
      <AlertGroup data={alerts} />
    </>
  );
};

export default Dashboard;
