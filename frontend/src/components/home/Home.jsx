import { useState } from 'react';
import { useSelector } from 'react-redux';
import * as ROLES from '../../utils/role-names';

import SideBar from '../sidebar/SideBar';
import TopBar from '../topbar/TopBar';
import StudentSideBarElements from '../sidebar/elements/StudentSideBarElements';
import EducatorSideBarElements from '../sidebar/elements/EducatorSideBarElements';
import { makeStyles } from '@material-ui/core/styles';
import {
  selectCurrentUser,
  selectRegisteredStatus,
} from '../../store/selectors';
import Alert from '../notifications/Alert';
import { RESET_REGISTER_STATUS } from '../../store/types/auth-action-types';
import { Box } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  spacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: '100vh',
    overflow: 'auto',
    display: 'flex',
    flexDirection: 'column',
  },
  container: {
    display: 'flex',
  },
}));

function Home({ component, title }) {
  const user = useSelector(selectCurrentUser);
  const { status } = useSelector(selectRegisteredStatus);
  const [open, setOpen] = useState(false);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const { content, container, spacer } = useStyles();

  return (
    <Box className={container}>
      <Alert
        message={'Uspješna registracija'}
        severity={'success'}
        shouldOpen={status}
        dispatchActionOnClose={{ type: RESET_REGISTER_STATUS }}
      />
      <SideBar
        handleClose={handleDrawerClose}
        isOpen={open}
        elements={
          user.role.name === ROLES.STUDENT ? (
            <StudentSideBarElements />
          ) : (
            <EducatorSideBarElements />
          )
        }
      />
      <TopBar handleOpen={handleDrawerOpen} isOpen={open} title={title} />
      <main className={content}>
        <Box className={spacer} />
        <Box
          overflow={'auto'}
          flexGrow={1}
          display={'flex'}
          flexDirection={'column'}
        >
          {component}
        </Box>
      </main>
    </Box>
  );
}

export default Home;
