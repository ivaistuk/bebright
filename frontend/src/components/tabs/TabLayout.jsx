import { Tab, Tabs } from '@material-ui/core';
import { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';

const TabPanel = ({ children, value, index, ...other }) => {
  return (
    <div
      role={'tabpanel'}
      hidden={value !== index}
      id={`tab-panel-${index}`}
      {...other}
    >
      {value === index && children}
    </div>
  );
};

const TabLayout = ({ data = [], indicatorColor = '#fff' }) => {
  const [value, setValue] = useState(0);

  const classes = makeStyles(theme => ({
    tabs: {
      '& .MuiTabs-indicator': {
        backgroundColor: indicatorColor,
      },
    },
  }))();

  const handleValueChange = (event, value) => {
    setValue(value);
  };

  return (
    <>
      <Tabs
        value={value}
        onChange={handleValueChange}
        centered
        className={classes.tabs}
      >
        {data.map((e, index) => (
          <Tab label={e.title} key={`tab-${index}`} />
        ))}
      </Tabs>
      {data.map((e, index) => (
        <TabPanel value={value} index={index} key={`tab-layout-${index}`}>
          {e.component}
        </TabPanel>
      ))}
    </>
  );
};

export default TabLayout;
