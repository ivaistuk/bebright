import { Box, Fab } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Close, Add, Edit } from '@material-ui/icons';

const useStyles = makeStyles(theme => ({
  container: {
    position: 'sticky',
    display: 'flex',
    justifyContent: 'center',
    bottom: 15,
    width: '100%',
    '& button': {
      margin: '0 5px',
      color: theme.palette.secondary.light,
    },
  },
}));

const TableActionButtons = () => {
  const classes = useStyles();
  return (
    <Box className={classes.container}>
      <Fab size={'large'} style={{ backgroundColor: 'green' }}>
        <Add />
      </Fab>
      <Fab size={'large'} style={{ backgroundColor: '#abaf28' }}>
        <Edit />
      </Fab>
      <Fab size={'large'} style={{ backgroundColor: 'red' }}>
        <Close />
      </Fab>
    </Box>
  );
};

export default TableActionButtons;
