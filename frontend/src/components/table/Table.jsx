import React, { useMemo } from 'react';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import MUITable from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { Box, Fade, Paper, Tooltip } from '@material-ui/core';
import { Add, Delete, Done, Edit, Face } from '@material-ui/icons';
import Button from '../styled-components/Button';
import { useSelector } from 'react-redux';
import { selectCurrentUser } from '../../store/selectors';
import { EDUCATOR, STUDENT } from '../../utils/role-names';

const useStyles = makeStyles(theme => ({
  paper: {
    margin: '20px',
  },
  container: ({ color }) => ({
    maxHeight: '100%',
    '& .MuiTableCell-root': {
      borderBottom: 'none',
    },
    '&  .MuiTableCell-head': {
      fontSize: '1rem',
      color: theme.palette.secondary.light,
      backgroundColor: color,
    },
  }),
}));

const Table = ({
  rows = [],
  columns = [],
  color: themeColor,
  handleAddRow,
  handleEditRow,
  handleDeleteRow,
  handleRowClick,
  style,
  reverse = false,
  mark = false,
  nohead = false,
}) => {
  const { role, id } = useSelector(selectCurrentUser);
  const theme = useTheme();
  const color = useMemo(
    () => (themeColor ? themeColor : theme.palette.primary.main),
    [theme.palette.primary.main, themeColor],
  );

  const body = useMemo(() => {
    return rows.map((row, index) => (
      <TableRow
        hover={!row.submitted}
        key={row.id}
        style={{ backgroundColor: row.submitted && 'rgba(213,212,212,0.52)' }}
        onClick={handleRowClick && (() => handleRowClick(row.id))}
      >
        {columns.map(
          column =>
            (!column.show || column.show({ role })) && (
              <TableCell key={column.id} align={column.align}>
                {column.format
                  ? column.format({ row, color, index: index + 1 })
                  : row[column.id]}
              </TableCell>
            ),
        )}
        {(role.name === EDUCATOR || reverse) && (
          <>
            {(handleEditRow || handleDeleteRow) && (
              <TableCell align={'right'} style={{ maxWidth: 120, height: 73 }}>
                <Box display={'flex'} justifyContent={'flex-end'}>
                  {handleEditRow && (
                    <Button
                      color={color}
                      size={'small'}
                      icon={<Edit />}
                      onClick={() => handleEditRow(row.id)}
                    />
                  )}
                  {handleDeleteRow && row.id !== id && (
                    <Button
                      color={'#c34d4d'}
                      size={'small'}
                      icon={<Delete />}
                      onClick={() => handleDeleteRow(row.id)}
                      style={{ marginLeft: 6 }}
                    />
                  )}
                </Box>
              </TableCell>
            )}
            {mark && (
              <TableCell align={'right'} style={{ maxWidth: 120, height: 73 }}>
                {row.id === id && (
                  <Tooltip title={'Ovo ste vi'}>
                    <span>
                      <Button size={'small'} color={color} icon={<Face />} />
                    </span>
                  </Tooltip>
                )}
              </TableCell>
            )}
          </>
        )}
        {role.name === STUDENT && !reverse && (
          <TableCell align={'right'} style={{ maxWidth: 80, height: 73 }}>
            {row.submitted && (
              <Box display={'flex'} justifyContent={'flex-end'}>
                <Tooltip title={'Predano'}>
                  <span>
                    <Button size={'small'} color={color} icon={<Done />} />
                  </span>
                </Tooltip>
              </Box>
            )}
          </TableCell>
        )}
      </TableRow>
    ));
  }, [
    color,
    columns,
    handleDeleteRow,
    handleEditRow,
    handleRowClick,
    id,
    mark,
    reverse,
    role,
    rows,
  ]);

  const header = useMemo(
    () => (
      <TableRow>
        {columns.map(
          column =>
            (!column.show || column.show({ role })) && (
              <TableCell key={column.id} style={{ minWidth: column.minWidth }}>
                {column.label}
              </TableCell>
            ),
        )}

        {(role.name === EDUCATOR || reverse) && !handleRowClick && (
          <TableCell align={'right'} style={{ width: 120, height: 73 }}>
            {handleAddRow && (
              <Button
                color={color}
                size={'small'}
                icon={<Add />}
                onClick={handleAddRow}
              />
            )}
          </TableCell>
        )}
        {role.name === STUDENT && !reverse && (
          <TableCell align={'right'} style={{ width: 80, height: 73 }} />
        )}
      </TableRow>
    ),
    [color, columns, handleAddRow, handleRowClick, reverse, role],
  );

  const classes = useStyles({ color });
  return (
    <Fade in={true} timeout={500}>
      <Paper className={classes.paper} elevation={10} style={{ ...style }}>
        <TableContainer className={classes.container}>
          <MUITable>
            {!nohead && <TableHead>{header}</TableHead>}
            <TableBody>{body}</TableBody>
          </MUITable>
        </TableContainer>
      </Paper>
    </Fade>
  );
};

export default Table;
