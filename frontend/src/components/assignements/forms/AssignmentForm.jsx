import { useFormik } from 'formik';
import * as Yup from 'yup';
import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  TextField,
} from '@material-ui/core';
import StyledButton from '../../styled-components/Button';
import { FileCopy } from '@material-ui/icons';
import { useMemo, useRef } from 'react';
import { useAppFormStyles } from '../../../hooks/use-styles';
import clsx from 'clsx';

const AssignmentForm = ({
  open = false,
  closeForm,
  onSubmit,
  formTitle = '',
  submitTitle = '',
  edit = false,
  color,
  values = {
    title: '',
    activeUntil: '',
    description: '',
    fileName: '',
    points: '',
    file: null,
  },
}) => {
  const fileInput = useRef(null);
  const formik = useFormik({
    initialValues: { ...values },
    validateOnChange: false,
    validateOnMount: false,
    validateOnBlur: false,
    validationSchema: Yup.object({
      title: Yup.string().required(),
    }),
    onSubmit: values => {
      onSubmit(values);
      closeForm();
    },
  });

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const disabled = useMemo(() => edit && formik.values.fileName !== '', [edit]);

  const { background, input, text, disabledInput } = useAppFormStyles({
    color,
  });
  const handleFileChange = e => {
    const reader = new FileReader();
    reader.onload = e => {
      const array = new Int8Array(e.target.result);
      const bytes = [];
      for (let i = 0; i < array.length; i++) bytes.push(array[i]);
      formik.setFieldValue('file', bytes.length > 0 ? bytes : '');
    };
    const file = e.target.files[0];
    if (!disabled) formik.setFieldValue('fileName', file ? file.name : '');
    reader.readAsArrayBuffer(file);
  };

  return (
    <Dialog open={open} onClose={closeForm} maxWidth={'sm'} fullWidth>
      <DialogTitle className={background}>{formTitle}</DialogTitle>
      <form className={'force-secondary-color'} onSubmit={formik.handleSubmit}>
        <DialogContent className={background}>
          <DialogContentText className={text}>
            Stvorite zadatak za vašu učionicu koji će moći vidjeti svi učenici i
            organizatori dodani u vašu učionicu.
          </DialogContentText>
          <TextField
            id="title"
            name="title"
            margin="normal"
            value={formik.values.title}
            onChange={formik.handleChange}
            autoComplete="title"
            label={'Naziv'}
            autoFocus
            fullWidth
            variant={'outlined'}
            required
            className={input}
            InputLabelProps={{
              shrink: true,
            }}
          />
          <TextField
            id="description"
            name="description"
            margin="normal"
            value={formik.values.description}
            onChange={formik.handleChange}
            autoComplete="description"
            label={'Opis Zadatka'}
            multiline
            fullWidth
            rows={5}
            variant={'outlined'}
            className={input}
            InputLabelProps={{
              shrink: true,
            }}
          />
          <TextField
            id="points"
            name="points"
            margin="normal"
            value={formik.values.points}
            onChange={formik.handleChange}
            type={'number'}
            label="Max bodovi"
            fullWidth
            variant={'outlined'}
            className={input}
            InputLabelProps={{
              shrink: true,
            }}
          />
          <TextField
            id="activeUntil"
            name="activeUntil"
            margin="normal"
            value={formik.values.activeUntil}
            onChange={formik.handleChange}
            label="Krajnji Rok"
            type="datetime-local"
            fullWidth
            variant={'outlined'}
            InputLabelProps={{
              shrink: true,
            }}
            className={input}
          />
          <Box marginTop={2} display={'flex'} justifyContent={'space-between'}>
            <StyledButton
              onClick={() => {
                if (fileInput) fileInput.current.click();
              }}
              color={color}
              icon={<FileCopy />}
            />
            <TextField
              id="fileName"
              name="fileName"
              margin="none"
              value={formik.values.fileName}
              onChange={formik.handleChange}
              label="Naziv Datoteke"
              variant={'outlined'}
              InputLabelProps={{
                shrink: true,
              }}
              className={clsx(input, disabled && disabledInput)}
              style={{ width: '87%' }}
              disabled={disabled}
              placeholder={'Nema datoteke'}
            />
          </Box>
          <input
            id="fileInput"
            name="fileInput"
            onChange={handleFileChange}
            type="file"
            style={{ display: 'none' }}
            ref={fileInput}
          />
        </DialogContent>
        <DialogActions className={background}>
          <Button type="submit" variant="text" className={text}>
            {submitTitle}
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  );
};

export default AssignmentForm;
