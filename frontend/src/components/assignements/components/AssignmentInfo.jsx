import { Box, Fade, Link, Paper, Tooltip, Typography } from '@material-ui/core';
import { trimDate } from '../../../utils/date-formatter';
import React from 'react';
import Button from '../../styled-components/Button';
import {
  AccessTime,
  Description,
  ExitToApp,
  List,
  VerticalAlignTop,
} from '@material-ui/icons';
import { useItemInfoStyles } from '../../../hooks/use-styles';
import { useHistory } from 'react-router';
import { PRODUCTION_URL } from '../../../utils/configure-axios';

const AssignmentInfo = ({ color, assignment, classroom }) => {
  const { panel, paragraph, heading, field, title } = useItemInfoStyles({
    color,
  });
  const history = useHistory();
  return (
    <Box display={'flex'} justifyContent={'center'} padding={3} paddingTop={3}>
      <Fade in={true} timeout={500}>
        <Paper className={panel} elevation={20}>
          <Box display={'flex'} padding={'15px'} marginBottom={'5px'}>
            <Tooltip title={'Vidi učionicu'} placement={'top'}>
              <div>
                <Button
                  size={'medium'}
                  color={color}
                  icon={<ExitToApp />}
                  onClick={() =>
                    history.push(`/classroom/${classroom && classroom.id}`)
                  }
                />
              </div>
            </Tooltip>
            <Typography variant={'h5'} className={title}>
              {`ZADATAK IZ UČIONICE ${
                classroom && classroom.name.toUpperCase()
              }`}
            </Typography>
          </Box>
          <Box className={field}>
            <Box display={'flex'}>
              <Button size={'small'} color={color} icon={<List />} />
              <Typography variant={'h6'} className={heading}>
                OPIS
              </Typography>
            </Box>
            <Typography
              variant={'body2'}
              className={paragraph}
              style={{ minHeight: 110 }}
            >
              {assignment ? (
                assignment.description !== '' ? (
                  assignment.description
                ) : (
                  <i style={{ color: 'rgba(152,151,151,0.77)' }}>
                    ( Opsi nije dodan )
                  </i>
                )
              ) : (
                <i style={{ color: 'rgba(152,151,151,0.77)' }}>
                  ( Opsi nije dodan )
                </i>
              )}
            </Typography>
          </Box>
          <Box display={'flex'}>
            <Box className={field} maxWidth={'50%'}>
              <Box display={'flex'}>
                <Button size={'small'} color={color} icon={<AccessTime />} />
                <Typography variant={'h6'} className={heading}>
                  ROK ZA PREDAJU
                </Typography>
              </Box>
              <Typography variant={'body2'} className={paragraph}>
                {assignment && assignment.activeUntil ? (
                  trimDate(assignment.activeUntil)
                ) : (
                  <i style={{ color: 'rgba(152,151,151,0.77)' }}>
                    ( Nije zadano )
                  </i>
                )}
              </Typography>
            </Box>
            <Box className={field} maxWidth={'50%'}>
              <Box display={'flex'}>
                <Button size={'small'} color={color} icon={<Description />} />
                <Typography variant={'h6'} className={heading}>
                  DATOTEKA
                </Typography>
              </Box>
              <Typography variant={'body2'} className={paragraph}>
                {assignment && assignment.fileName ? (
                  <Link
                    href={`${PRODUCTION_URL}/assignment/${assignment.id}/file/download`}
                    download
                    style={{ color: color }}
                  >
                    {assignment.fileName}
                  </Link>
                ) : (
                  <i style={{ color: 'rgba(152,151,151,0.77)' }}>
                    ( Nema priloženih datoteka )
                  </i>
                )}
              </Typography>
            </Box>
          </Box>
          <Box className={field}>
            <Box display={'flex'}>
              <Button
                size={'small'}
                color={color}
                icon={<VerticalAlignTop />}
              />
              <Typography variant={'h6'} className={heading}>
                MAX. BODOVI
              </Typography>
            </Box>
            <Typography variant={'body2'} className={paragraph}>
              {assignment && assignment.points ? (
                assignment.points
              ) : (
                <i style={{ color: 'rgba(152,151,151,0.77)' }}>
                  ( Neograničeno )
                </i>
              )}
            </Typography>
          </Box>
        </Paper>
      </Fade>
    </Box>
  );
};

export default AssignmentInfo;
