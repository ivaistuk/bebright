import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  selectAssignment,
  selectAssignmentDeletedStatus,
  selectAssignmentEditedStatus,
  selectCurrentUser,
  selectUserAssignments,
} from '../../../store/selectors';
import { EDUCATOR } from '../../../utils/role-names';
import { Box, Container, Fade, Paper, Typography } from '@material-ui/core';
import Alert from '../../notifications/Alert';
import { useItemPageStyles } from '../../../hooks/use-styles';
import {
  deleteAssignment,
  editAssignment,
  getAssignment,
  getUserAssignments,
} from '../../../store/actions/assignment-actions';
import AssignmentForm from '../forms/AssignmentForm';
import ConfirmDialog from '../../notifications/ConfirmDialog';
import { adjustDateForNativeInput } from '../../../utils/date-formatter';
import Table from '../../table/Table';
import { taskColumns } from '../../../utils/columns-template';
import {
  RESET_ASSIGNEMENT_DELETE_STATUS,
  RESET_ASSIGNEMENT_EDIT_STATUS,
} from '../../../store/types/assignment-action-types';
import Button from '../../styled-components/Button';
import { Assignment } from '@material-ui/icons';
import Loading from '../../loading/Loading';

const resetAssignmentCrud = dispatch => {
  dispatch({ type: RESET_ASSIGNEMENT_DELETE_STATUS });
  dispatch({ type: RESET_ASSIGNEMENT_EDIT_STATUS });
};

const Tasks = () => {
  const dispatch = useDispatch();
  const { role } = useSelector(selectCurrentUser);
  const {
    data: assignments,
    error: assignmentsError,
    loading,
  } = useSelector(selectUserAssignments);
  const { data: assignment, loading: fetchingAssignment } =
    useSelector(selectAssignment);
  const { status: edited } = useSelector(selectAssignmentEditedStatus);
  const { status: deleted } = useSelector(selectAssignmentDeletedStatus);

  useEffect(() => {
    if (edited || deleted) {
      dispatch(getUserAssignments());
      resetAssignmentCrud(dispatch);
    }
  }, [deleted, dispatch, edited, role]);

  useEffect(() => {
    dispatch(getUserAssignments());
  }, [dispatch]);

  const [openAssignmentForm, setOpenAssignmentForm] = useState(false);
  const handleOpenAssignmentForm = () => {
    setOpenAssignmentForm(true);
  };
  const handleCloseAssingmentForm = () => {
    setOpenAssignmentForm(false);
  };

  const handleAssignmentEdit = values => {
    const assignmentData = {
      id: assignment.id,
      title: values.title,
      description: values.description,
      points: values.points === '' ? null : Number(values.points),
      activeUntil: values.activeUntil,
      fileName: values.fileName,
    };

    dispatch(
      editAssignment(assignmentData, values.file !== '' ? values.file : null),
    );
  };

  const [confirmDelete, setConfirmDelete] = useState(false);
  const handleOpenDeleteDialog = () => {
    setConfirmDelete(true);
  };
  const handleCloseDeleteDialog = () => {
    setConfirmDelete(false);
  };
  const handleDeleteAssignment = () => {
    dispatch(deleteAssignment(assignment.id, assignment.classroom.id));
    handleCloseDeleteDialog();
  };

  const { title, paper, body, container } = useItemPageStyles();
  return (
    <Container maxWidth={'xl'} className={container}>
      <Alert
        message={'Problem tijekom učitavanja zadatka'}
        severity={'error'}
        shouldOpen={!!assignmentsError}
      />
      <Fade in={true} timeout={500}>
        <Paper className={paper} elevation={8}>
          <Box className={title}>
            <Button icon={<Assignment />} />
            <Typography variant={'h3'}>
              {role.name === EDUCATOR
                ? 'Upravljanje zadatcima'
                : 'Lista zadataka'}
            </Typography>
          </Box>
          <Box className={body}>
            <Table
              rows={assignments}
              columns={taskColumns}
              handleEditRow={id => {
                dispatch(getAssignment(id));
                handleOpenAssignmentForm();
              }}
              handleDeleteRow={id => {
                dispatch(getAssignment(id));
                handleOpenDeleteDialog();
              }}
            />
            {loading && <Loading color={'#fff'} minHeight={160} size={70} />}
          </Box>
        </Paper>
      </Fade>
      {openAssignmentForm && !fetchingAssignment && (
        <AssignmentForm
          open={true}
          edit={true}
          closeForm={handleCloseAssingmentForm}
          onSubmit={handleAssignmentEdit}
          formTitle={'Uredi Zadatak'}
          submitTitle={'Uredi Zadatak'}
          values={
            assignment && {
              title: assignment.title,
              description: assignment.description,
              activeUntil: adjustDateForNativeInput(assignment.activeUntil),
              points: assignment.points ? Number(assignment.points) : '',
              fileName: assignment.fileName ? assignment.fileName : '',
            }
          }
        />
      )}
      <ConfirmDialog
        open={confirmDelete && !fetchingAssignment}
        handleClose={handleCloseDeleteDialog}
        onConfirm={handleDeleteAssignment}
        title={'Ukloniti zadatak?'}
        text={'Nakon ove akcije nije više moguće vratiti zadatak!'}
      />
    </Container>
  );
};

export default Tasks;
