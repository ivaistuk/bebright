import {
  Box,
  Container,
  Fade,
  Paper,
  Tooltip,
  Typography,
} from '@material-ui/core';
import { useItemPageStyles } from '../../../hooks/use-styles';
import { useDispatch, useSelector } from 'react-redux';
import {
  selectAssignment,
  selectAssignmentEditedStatus,
  selectCurrentUser,
  selectStudentSubmissions,
} from '../../../store/selectors';
import { EDUCATOR, STUDENT } from '../../../utils/role-names';
import {
  Delete,
  Edit,
  Assignment as AssignmentIcon,
  Done,
} from '@material-ui/icons';
import ConfirmDialog from '../../notifications/ConfirmDialog';
import React, { useEffect, useMemo, useState } from 'react';
import {
  deleteAssignment,
  editAssignment,
  getAssignment,
} from '../../../store/actions/assignment-actions';
import AssignmentForm from '../forms/AssignmentForm';
import { adjustDateForNativeInput } from '../../../utils/date-formatter';
import { useHistory, useParams } from 'react-router';
import Button from '../../styled-components/Button';
import Alert from '../../notifications/Alert';
import { calculateColour } from '../../../utils/classroom-theme-colour';
import TabLayout from '../../tabs/TabLayout';
import AssignmentInfo from '../components/AssignmentInfo';
import UserSubmissionsTable from '../../submissions/tables/UserSubmissionsTable';
import TaskSubmissionsTable from '../../submissions/tables/TaskSubmissionsTable';
import { RESET_ASSIGNEMENT_EDIT_STATUS } from '../../../store/types/assignment-action-types';
import { getStudentSubmissions } from '../../../store/actions/submission-actions';

const Assignment = () => {
  const dispatch = useDispatch();
  const { role } = useSelector(selectCurrentUser);
  const { data: assignment, error: getAssignmentError } =
    useSelector(selectAssignment);
  const { status: edited } = useSelector(selectAssignmentEditedStatus);
  const { id: assignmentId } = useParams();
  const history = useHistory();

  useEffect(() => {
    dispatch(getAssignment(assignmentId));
  }, [dispatch, assignmentId]);

  useEffect(() => {
    if (edited) {
      dispatch(getAssignment(assignmentId));
      dispatch({ type: RESET_ASSIGNEMENT_EDIT_STATUS });
    }
  }, [edited, dispatch, assignmentId]);

  const [openAssignmentForm, setOpenAssignmentForm] = useState(false);
  const handleOpenAssignmentForm = () => {
    setOpenAssignmentForm(true);
  };
  const handleCloseAssingmentForm = () => {
    setOpenAssignmentForm(false);
  };

  const handleAssignmentEdit = values => {
    const assignmentData = {
      id: assignment.id,
      title: values.title,
      description: values.description,
      points: values.points === '' ? null : Number(values.points),
      activeUntil: values.activeUntil,
      fileName: values.fileName,
    };

    dispatch(
      editAssignment(assignmentData, values.file !== '' ? values.file : null),
    );
  };

  const [confirmDelete, setConfirmDelete] = useState(false);
  const handleOpenDeleteDialog = () => {
    setConfirmDelete(true);
  };
  const handleCloseDeleteDialog = () => {
    setConfirmDelete(false);
  };
  const handleDeleteAssignment = () => {
    dispatch(deleteAssignment(assignment.id, assignment.classroom.id));
    history.goBack();
  };

  const [submitted, setSubmitted] = useState(false);
  const { data: submissions } = useSelector(selectStudentSubmissions);
  useEffect(() => {
    if (role.name === STUDENT) dispatch(getStudentSubmissions(assignmentId));
  }, [assignmentId, dispatch, role.name]);

  useEffect(() => {
    if (submissions.length > 0) setSubmitted(true);
    else if (submissions.length === 0) setSubmitted(false);
  }, [submissions]);

  const color = useMemo(() => {
    if (assignment)
      return calculateColour(
        assignment.classroom &&
          assignment.classroom.name.charCodeAt(0) +
            assignment.classroom.name.charCodeAt(1),
      );
  }, [assignment]);
  const { container, paper, body, title } = useItemPageStyles({
    color,
  });

  return (
    <Container maxWidth="xl" className={container}>
      <Alert
        message={'Problem tijekom učitavanja zadatka'}
        severity={'error'}
        shouldOpen={!!getAssignmentError}
      />
      <Fade in={true} timeout={500}>
        <Paper className={paper} elevation={8}>
          <Box className={title}>
            <Button color={color} icon={<AssignmentIcon />} />
            <Typography variant={'h3'}>
              {assignment && assignment.title}
            </Typography>
            {role.name === EDUCATOR && (
              <>
                <Button
                  onClick={handleOpenAssignmentForm}
                  size={'medium'}
                  color={color}
                  icon={<Edit />}
                />
                <Button
                  onClick={handleOpenDeleteDialog}
                  color={'#c34d4d'}
                  size={'medium'}
                  icon={<Delete />}
                />
              </>
            )}
            {role.name === STUDENT && submitted && (
              <Tooltip title={'Predano'}>
                <span>
                  <Button size={'medium'} color={color} icon={<Done />} />
                </span>
              </Tooltip>
            )}
          </Box>

          <Box className={body}>
            <TabLayout
              data={[
                {
                  title: 'Pojedinosti',
                  component: (
                    <AssignmentInfo
                      color={color}
                      assignment={assignment}
                      classroom={assignment && assignment.classroom}
                    />
                  ),
                },
                role.name === STUDENT
                  ? {
                      title: 'Vaše predaje',
                      component: (
                        <UserSubmissionsTable
                          assignmentId={assignmentId}
                          color={color}
                        />
                      ),
                    }
                  : {
                      title: 'Sve predaje',
                      component: (
                        <TaskSubmissionsTable
                          assignmentId={assignmentId}
                          color={color}
                        />
                      ),
                    },
              ]}
            />
          </Box>
        </Paper>
      </Fade>

      {openAssignmentForm && (
        <AssignmentForm
          open={true}
          edit={true}
          closeForm={handleCloseAssingmentForm}
          onSubmit={handleAssignmentEdit}
          formTitle={'Uredi Zadatak'}
          submitTitle={'Uredi Zadatak'}
          color={color}
          values={
            assignment && {
              title: assignment.title,
              description: assignment.description,
              activeUntil: adjustDateForNativeInput(assignment.activeUntil),
              points: assignment.points ? Number(assignment.points) : '',
              fileName: assignment.fileName ? assignment.fileName : '',
            }
          }
        />
      )}

      <ConfirmDialog
        open={confirmDelete}
        handleClose={handleCloseDeleteDialog}
        onConfirm={handleDeleteAssignment}
        color={color}
        title={'Ukloniti zadatak?'}
        text={'Nakon ove akcije nije više moguće vratiti zadatak!'}
      />
    </Container>
  );
};

export default Assignment;
