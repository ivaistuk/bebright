import { makeStyles } from '@material-ui/core/styles';
import { Fab, Tooltip } from '@material-ui/core';
import { Add } from '@material-ui/icons';
export function FixedButton({
  top = 0,
  right = 0,
  size = 'large',
  onClick,
  buttonContent = <Add />,
  color = '',
  title,
  square = false,
  noShadow = false,
}) {
  const classes = makeStyles(theme => ({
    absoluteContainer: {
      position: 'absolute',
      top: top,
      right: right,
      '& .MuiFab-root': {
        borderRadius: square ? 0 : '50%',
        boxShadow: noShadow
          ? 'none'
          : '0px 3px 5px -1px rgb(0 0 0 / 20%), 0px 6px 10px 0px rgb(0 0 0 / 14%), 0px 1px 18px 0px rgb(0 0 0 / 12%)',
      },
    },
  }))();

  return (
    <Tooltip title={title} placement="top">
      <div className={classes.absoluteContainer}>
        <Fab
          color={'primary'}
          onClick={onClick}
          size={size}
          style={{ backgroundColor: color }}
        >
          {buttonContent}
        </Fab>
      </div>
    </Tooltip>
  );
}
