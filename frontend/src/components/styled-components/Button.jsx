import { Add } from '@material-ui/icons';
import { Fab } from '@material-ui/core';
import React from 'react';
import { useTheme } from '@material-ui/core/styles';

const Button = ({
  onClick,
  color,
  size = 'large',
  icon = <Add />,
  style,
  disabled = false,
  ...rest
}) => {
  const theme = useTheme();
  return (
    <Fab
      onClick={onClick}
      size={size}
      style={{
        backgroundColor: color ? color : theme.palette.primary.main,
        color: theme.palette.secondary.light,
        cursor: disabled ? 'default' : 'pointer',
        ...style,
      }}
      disabled={disabled}
      {...rest}
    >
      {icon}
    </Fab>
  );
};

export default Button;
