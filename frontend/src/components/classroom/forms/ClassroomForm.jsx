import { useFormik } from 'formik';
import * as Yup from 'yup';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  TextField,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const ClassroomForm = ({
  open = false,
  closeForm,
  onSubmit,
  formTitle = '',
  submitTitle = '',
  color,
  values = {
    classroom: '',
    picture: null,
  },
}) => {
  const formik = useFormik({
    initialValues: { ...values },
    validateOnChange: false,
    validateOnMount: false,
    validateOnBlur: false,
    validationSchema: Yup.object({
      classroom: Yup.string().required(),
    }),
    onSubmit: values => {
      onSubmit(values);
      closeForm();
    },
  });

  const classess = makeStyles(theme => ({
    background: {
      backgroundColor: color ? color : theme.palette.primary.main,
      color: theme.palette.secondary.light,
    },
    text: {
      color: theme.palette.secondary.light,
    },
    input: {
      '& .Mui-focused': {
        '& fieldset': {
          border: `1px solid ${theme.palette.secondary.light} !important`,
          color: `${theme.palette.secondary.light} !important`,
        },
      },
      '& label, .MuiInputBase-root': {
        color: `${theme.palette.secondary.light} !important`,
      },
    },
  }))();

  return (
    <Dialog open={open} onClose={closeForm} maxWidth={'sm'} fullWidth>
      <DialogTitle className={classess.background}>{formTitle}</DialogTitle>
      <form className={'force-secondary-color'} onSubmit={formik.handleSubmit}>
        <DialogContent className={classess.background}>
          <DialogContentText className={classess.text}>
            Upišite željeno ime vaše učionice. U budućnosti će biti omogućeno i
            dodavanje vlastite pozadinske slike.
          </DialogContentText>
          <TextField
            id="classroom"
            name="classroom"
            margin="dense"
            value={formik.values.classroom}
            onChange={formik.handleChange}
            autoComplete="classroom"
            autoFocus
            fullWidth
            required
            variant={'outlined'}
            className={classess.input}
          />
        </DialogContent>
        <DialogActions className={classess.background}>
          <Button type="submit" variant="text" className={classess.text}>
            {submitTitle}
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  );
};

export default ClassroomForm;
