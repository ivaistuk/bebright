import { useAppFormStyles } from '../../../hooks/use-styles';
import {
  Box,
  Dialog,
  DialogContent,
  DialogContentText,
  DialogTitle,
  TextField,
  Typography,
} from '@material-ui/core';
import React, { useCallback, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { selectFoundStudents } from '../../../store/selectors';
import Table from '../../table/Table';
import { userColumns } from '../../../utils/columns-template';
import { makeStyles } from '@material-ui/core/styles';
import Loading from '../../loading/Loading';
import { searchStudent } from '../../../store/actions/auth-actions';
import clsx from 'clsx';

const useStyles = makeStyles(theme => ({
  modal: {
    '& .MuiDialog-paper': {
      height: ({ value }) => (value !== '' ? '80vh' : 210),
      paddingBottom: 20,
      backgroundColor: ({ color }) => color,
      transition: theme.transitions.create('height', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    '& ::-webkit-scrollbar-thumb': {
      backgroundColor: ({ color }) => color,
    },
  },
  body: { display: 'flex', flexDirection: 'column' },
}));

const FindUserForm = ({
  open = false,
  closeForm,
  onSelect,
  title = '',
  description = '',
  color,
  filter = [],
  selector = selectFoundStudents,
  action = searchStudent,
}) => {
  const dispatch = useDispatch();
  const { data, loading } = useSelector(selector);
  const [loaded, setLoaded] = useState([]);
  const [value, setValue] = useState('');

  const filterLoaded = useCallback(
    array => {
      const filtered = [];
      array.forEach(e => {
        let remove = false;
        filter.forEach(f => {
          if (f.id === e.id) remove = true;
        });
        if (!remove) filtered.push(e);
      });
      return filtered;
    },
    [filter],
  );

  useEffect(() => {
    if (value === '') setLoaded([]);
    else if (!loading) {
      let changed = false;
      if (data.length === loaded.length)
        data.forEach(e => {
          let found = false;
          loaded.forEach(o => {
            if (o.id === e.id) found = true;
          });
          if (!found) changed = true;
        });
      else changed = true;
      if (changed) setLoaded(filterLoaded(data));
    }
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, [action, data, loading, value]);

  const handleChange = e => {
    if (e.target.value !== '') dispatch(action(e.target.value));
    setValue(e.target.value);
  };

  const handleSelect = id => {
    onSelect(id);
    closeForm();
  };

  const { modal, body } = useStyles({ color, value });
  const { background, input, text } = useAppFormStyles({ color });
  return (
    <Dialog
      open={open}
      onClose={closeForm}
      className={modal}
      maxWidth={'md'}
      fullWidth
    >
      <DialogTitle className={background}>{title}</DialogTitle>
      <DialogContent className={clsx(background, body)}>
        <DialogContentText className={text}>{description}</DialogContentText>
        <TextField
          id="keyword"
          name="keyword"
          margin="normal"
          defaultValue={''}
          onChange={handleChange}
          variant={'outlined'}
          className={input}
          autoFocus
          fullWidth
        />
        {loaded.length > 0 && (
          <Table
            rows={loaded}
            columns={userColumns}
            color={color}
            style={{ margin: 0, marginTop: 10, overflowY: 'auto' }}
            handleRowClick={handleSelect}
          />
        )}
        {value !== '' && loaded.length === 0 && (
          <Box
            display={'flex'}
            flexDirection={'column'}
            justifyContent={'center'}
            flexGrow={1}
          >
            <Typography component={'i'} variant={'body1'} align={'center'}>
              ( pretraživanje nije dalo rezultata )
            </Typography>
            <Loading color={'#fff'} minHeight={140} size={loading ? 70 : 0} />
          </Box>
        )}
      </DialogContent>
    </Dialog>
  );
};

export default FindUserForm;
