import { useRef } from 'react';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { useAppFormStyles } from '../../../hooks/use-styles';
import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  TextField,
} from '@material-ui/core';
import StyledButton from '../../styled-components/Button';
import { FileCopy } from '@material-ui/icons';
import clsx from 'clsx';

const FileForm = ({
  open = false,
  exists = false,
  closeForm,
  onSubmit,
  formTitle = '',
  submitTitle = '',
  color,
  values = {
    folder: '',
    fileName: '',
    file: null,
  },
}) => {
  const fileInput = useRef(null);
  const formik = useFormik({
    initialValues: { ...values },
    validateOnChange: false,
    validateOnMount: false,
    validateOnBlur: false,
    validationSchema: Yup.object({
      folder: Yup.string().required(),
      fileName: Yup.string().required(),
    }),
    onSubmit: values => {
      if (formik.values.file != null) {
        onSubmit(values);
        closeForm();
      }
    },
  });

  const { background, input, text, disabledInput } = useAppFormStyles({
    color,
  });
  const handleFileChange = e => {
    const reader = new FileReader();
    reader.onload = e => {
      const array = new Int8Array(e.target.result);
      const bytes = [];
      for (let i = 0; i < array.length; i++) bytes.push(array[i]);
      formik.setFieldValue('file', bytes.length > 0 ? bytes : '');
    };
    const file = e.target.files[0];
    formik.setFieldValue('fileName', file ? file.name : '');
    reader.readAsArrayBuffer(file);
  };

  return (
    <Dialog open={open} onClose={closeForm} maxWidth={'sm'} fullWidth>
      <DialogTitle className={background}>{formTitle}</DialogTitle>
      <form className={'force-secondary-color'} onSubmit={formik.handleSubmit}>
        <DialogContent className={background}>
          <DialogContentText className={text}>
            Dodajte datoteku u učionicu čime će ona biti dostupna svim
            sudionicima.
          </DialogContentText>
          <TextField
            id="folder"
            name="folder"
            margin="normal"
            value={formik.values.folder}
            onChange={formik.handleChange}
            label="Ime direktorija"
            autoFocus
            fullWidth
            variant={'outlined'}
            required
            disabled={exists}
            className={clsx(input, exists && disabledInput)}
            InputLabelProps={{
              shrink: true,
            }}
          />
          <Box marginTop={3} display={'flex'} justifyContent={'space-between'}>
            <StyledButton
              onClick={() => {
                if (fileInput) fileInput.current.click();
              }}
              color={color}
              icon={<FileCopy />}
            />
            <TextField
              id="fileName"
              name="fileName"
              margin="none"
              value={formik.values.fileName}
              onChange={formik.handleChange}
              label="Naziv Datoteke"
              variant={'outlined'}
              InputLabelProps={{
                shrink: true,
              }}
              className={input}
              style={{ width: '87%' }}
              placeholder={'Nema datoteke'}
            />
          </Box>
          <input
            id="fileInput"
            name="fileInput"
            onChange={handleFileChange}
            type="file"
            style={{ display: 'none' }}
            ref={fileInput}
          />
        </DialogContent>
        <DialogActions className={background}>
          <Button type="submit" variant="text" className={text}>
            {submitTitle}
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  );
};

export default FileForm;
