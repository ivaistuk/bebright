import Table from '../../table/Table';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  selectClassroomOrganizers,
  selectFoundOrganizers,
  selectUser,
} from '../../../store/selectors';
import {
  addOrganizerToClassroom,
  getClassroomOrganizers,
  removeOrganizerFromClassroom,
} from '../../../store/actions/classroom-actions';
import { getUser, searchOrgnizer } from '../../../store/actions/auth-actions';
import FindUserForm from '../forms/FindUserForm';
import { userColumns } from '../../../utils/columns-template';
import ConfirmDialog from '../../notifications/ConfirmDialog';
import Loading from '../../loading/Loading';

const OrganizersTable = ({ classId, color }) => {
  const dispatch = useDispatch();
  const { data: organizers, loading } = useSelector(selectClassroomOrganizers);
  const { data: selectedUser, loading: fetchingUser } = useSelector(selectUser);

  useEffect(() => {
    dispatch(getClassroomOrganizers(classId));
  }, [classId, dispatch]);

  const [openForm, setOpenForm] = useState(false);
  const handleOpenForm = () => {
    setOpenForm(true);
  };
  const handleCloseForm = () => {
    setOpenForm(false);
  };

  const handleAddUser = id => {
    if (id) dispatch(addOrganizerToClassroom(id, classId));
  };

  const [confirmDelete, setConfirmDelete] = useState(false);
  const handleOpenDeleteDialog = () => {
    setConfirmDelete(true);
  };
  const handleCloseDeleteDialog = () => {
    setConfirmDelete(false);
  };

  const handleRemoveUser = () => {
    dispatch(removeOrganizerFromClassroom(selectedUser.id, classId));
    handleCloseDeleteDialog();
  };
  return (
    <>
      <Table
        rows={organizers}
        columns={userColumns}
        color={color}
        handleAddRow={handleOpenForm}
        handleDeleteRow={id => {
          dispatch(getUser(id));
          handleOpenDeleteDialog();
        }}
      />
      {loading && <Loading color={'#fff'} minHeight={160} size={70} />}
      {openForm && !fetchingUser && (
        <FindUserForm
          open={openForm && !fetchingUser}
          closeForm={handleCloseForm}
          onSelect={handleAddUser}
          selector={selectFoundOrganizers}
          action={searchOrgnizer}
          filter={organizers}
          description={
            'Pretražite postojeće predavače i' +
            ' dodajte ih u trenutnu učionicu.'
          }
          title={'Dodaj organizatora'}
          color={color}
        />
      )}

      <ConfirmDialog
        open={confirmDelete && !fetchingUser}
        color={color}
        handleClose={handleCloseDeleteDialog}
        onConfirm={handleRemoveUser}
        title={'Ukloniti predavača?'}
        text={'Nakon ove akcije korisnik više neće moći pristupiti učionici!'}
      />
    </>
  );
};

export default OrganizersTable;
