import Table from '../../table/Table';
import {
  createAssignment,
  deleteAssignment,
  editAssignment,
  getAssignment,
  getClassroomAssignments,
} from '../../../store/actions/assignment-actions';
import React, { useEffect, useMemo, useState } from 'react';
import AssignmentForm from '../../assignements/forms/AssignmentForm';
import { useDispatch, useSelector } from 'react-redux';
import {
  selectAssignment,
  selectAssignmentCreatedStatus,
  selectAssignmentDeletedStatus,
  selectAssignmentEditedStatus,
  selectClassroomAssignments,
} from '../../../store/selectors';
import ConfirmDialog from '../../notifications/ConfirmDialog';
import { adjustDateForNativeInput } from '../../../utils/date-formatter';
import { taskColumns } from '../../../utils/columns-template';
import {
  RESET_ASSIGNEMENT_CREATED_STATUS,
  RESET_ASSIGNEMENT_DELETE_STATUS,
  RESET_ASSIGNEMENT_EDIT_STATUS,
} from '../../../store/types/assignment-action-types';
import Loading from '../../loading/Loading';

const resetAssignmentCrud = dispatch => {
  dispatch({ type: RESET_ASSIGNEMENT_DELETE_STATUS });
  dispatch({ type: RESET_ASSIGNEMENT_CREATED_STATUS });
  dispatch({ type: RESET_ASSIGNEMENT_EDIT_STATUS });
};

const TasksTable = ({ classId, color }) => {
  const dispatch = useDispatch();
  const { data: assignments, loading } = useSelector(
    selectClassroomAssignments,
  );
  const { data: selectedAssignment, loading: fetchingAssignment } =
    useSelector(selectAssignment);
  const { status: created } = useSelector(selectAssignmentCreatedStatus);
  const { status: edited } = useSelector(selectAssignmentEditedStatus);
  const { status: deleted } = useSelector(selectAssignmentDeletedStatus);

  useEffect(() => {
    dispatch(getClassroomAssignments(classId));
  }, [classId, dispatch]);

  useEffect(() => {
    if (created || deleted || edited) {
      dispatch(getClassroomAssignments(classId));
      resetAssignmentCrud(dispatch);
    }
  }, [classId, created, deleted, dispatch, edited]);

  const [openForm, setOpenForm] = useState(false);
  const [edit, setEdit] = useState(false);
  const handleOpenForm = () => {
    setOpenForm(true);
  };
  const handleCloseForm = () => {
    setOpenForm(false);
    setEdit(false);
  };
  const handleCreate = values => {
    const assignment = {
      classroomId: classId,
      title: values.title,
      description: values.description,
      points: values.points === '' ? null : Number(values.points),
      activeUntil: values.activeUntil !== '' ? values.activeUntil : null,
      fileName: values.file !== '' ? values.fileName : null,
    };
    dispatch(
      createAssignment(assignment, values.file !== '' ? values.file : null),
    );
  };
  const handleEdit = values => {
    const assignment = {
      id: selectedAssignment.id,
      title: values.title,
      description: values.description,
      points: values.points === '' ? null : Number(values.points),
      activeUntil: values.activeUntil,
      fileName: values.fileName,
    };

    dispatch(
      editAssignment(assignment, values.file !== '' ? values.file : null),
    );
  };
  const submit = edit ? handleEdit : handleCreate;
  const title = edit ? 'Uredi Zadatak' : 'Stvori Zadatak';
  const values = useMemo(() => {
    return edit && selectedAssignment
      ? {
          title: selectedAssignment.title,
          description: selectedAssignment.description,
          points: selectedAssignment.points
            ? Number(selectedAssignment.points)
            : '',
          activeUntil: adjustDateForNativeInput(selectedAssignment.activeUntil),
          fileName: selectedAssignment.fileName
            ? selectedAssignment.fileName
            : '',
        }
      : undefined;
  }, [edit, selectedAssignment]);

  const [confirmDelete, setConfirmDelete] = useState(false);
  const handleOpenDeleteDialog = () => {
    setConfirmDelete(true);
  };
  const handleCloseDeleteDialog = () => {
    setConfirmDelete(false);
  };
  const handleDelete = () => {
    dispatch(deleteAssignment(selectedAssignment.id));
    handleCloseDeleteDialog();
  };

  return (
    <>
      <Table
        rows={assignments}
        columns={taskColumns}
        color={color}
        handleAddRow={handleOpenForm}
        handleEditRow={id => {
          dispatch(getAssignment(id));
          setEdit(true);
          handleOpenForm();
        }}
        handleDeleteRow={id => {
          dispatch(getAssignment(id));
          handleOpenDeleteDialog();
        }}
      />
      {loading && <Loading color={'#fff'} minHeight={160} size={70} />}
      {openForm && !fetchingAssignment && (
        <AssignmentForm
          open={true}
          edit={edit}
          closeForm={handleCloseForm}
          onSubmit={submit}
          formTitle={title}
          submitTitle={title}
          color={color}
          values={values}
        />
      )}

      <ConfirmDialog
        open={confirmDelete && !fetchingAssignment}
        color={color}
        handleClose={handleCloseDeleteDialog}
        onConfirm={handleDelete}
        title={'Ukloniti zadatak?'}
        text={'Nakon ove akcije nije više moguće vratiti zadatak!'}
      />
    </>
  );
};

export default TasksTable;
