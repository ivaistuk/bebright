import Table from '../../table/Table';
import React, { useEffect, useState } from 'react';
import { userColumns } from '../../../utils/columns-template';
import { useDispatch, useSelector } from 'react-redux';
import {
  selectClassroomStudents,
  selectFoundStudents,
  selectUser,
} from '../../../store/selectors';
import {
  addStudentToClassroom,
  getClassroomStudents,
  removeStudentFromClassroom,
} from '../../../store/actions/classroom-actions';
import { getUser, searchStudent } from '../../../store/actions/auth-actions';
import FindUserForm from '../forms/FindUserForm';
import ConfirmDialog from '../../notifications/ConfirmDialog';
import Loading from '../../loading/Loading';

const StudentsTable = ({ classId, color }) => {
  const dispatch = useDispatch();
  const { data: students, loading } = useSelector(selectClassroomStudents);
  const { data: selectedUser, loading: fetchingUser } = useSelector(selectUser);

  useEffect(() => {
    dispatch(getClassroomStudents(classId));
  }, [classId, dispatch]);

  const [openForm, setOpenForm] = useState(false);
  const handleOpenForm = () => {
    setOpenForm(true);
  };
  const handleCloseForm = () => {
    setOpenForm(false);
  };

  const handleAddUser = id => {
    if (id) dispatch(addStudentToClassroom(id, classId));
  };

  const [confirmDelete, setConfirmDelete] = useState(false);
  const handleOpenDeleteDialog = () => {
    setConfirmDelete(true);
  };
  const handleCloseDeleteDialog = () => {
    setConfirmDelete(false);
  };

  const handleRemoveUser = () => {
    dispatch(removeStudentFromClassroom(selectedUser.id, classId));
    handleCloseDeleteDialog();
  };

  return (
    <>
      <Table
        rows={students}
        columns={userColumns}
        color={color}
        handleAddRow={handleOpenForm}
        handleDeleteRow={id => {
          dispatch(getUser(id));
          handleOpenDeleteDialog();
        }}
      />
      {loading && <Loading color={'#fff'} minHeight={160} size={70} />}
      {openForm && !fetchingUser && (
        <FindUserForm
          open={openForm && !fetchingUser}
          closeForm={handleCloseForm}
          onSelect={handleAddUser}
          selector={selectFoundStudents}
          action={searchStudent}
          filter={students}
          description={
            'Pretražite postojeće učenike i dodajte ih u trenutnu učionicu.'
          }
          title={'Dodaj učenika'}
          color={color}
        />
      )}

      <ConfirmDialog
        open={confirmDelete && !fetchingUser}
        color={color}
        handleClose={handleCloseDeleteDialog}
        onConfirm={handleRemoveUser}
        title={'Ukloniti učenika?'}
        text={'Nakon ove akcije korisnik više neće moći pristupiti učionici!'}
      />
    </>
  );
};

export default StudentsTable;
