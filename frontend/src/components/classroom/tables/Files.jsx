import { useDispatch, useSelector } from 'react-redux';
import {
  Box,
  Collapse,
  IconButton,
  Link,
  TableCell,
  TableRow,
  Table as MUITable,
  Fade,
  Paper,
  Typography,
} from '@material-ui/core';
import React, { useMemo, useState } from 'react';
import { Add, KeyboardArrowDown, KeyboardArrowUp } from '@material-ui/icons';
import Table from '../../table/Table';
import { getClassroom } from '../../../store/actions/classroom-actions';
import ConfirmDialog from '../../notifications/ConfirmDialog';
import FileForm from '../forms/FileForm';
import { makeStyles } from '@material-ui/core/styles';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableBody from '@material-ui/core/TableBody';
import Button from '../../styled-components/Button';
import axios from 'axios';
import {
  BASIC_HEADER_OPTIONS,
  PRODUCTION_URL,
} from '../../../utils/configure-axios';
import { selectClassroom, selectCurrentUser } from '../../../store/selectors';
import { EDUCATOR } from '../../../utils/role-names';
import Loading from '../../loading/Loading';

const useStyles = makeStyles(theme => ({
  paper: {
    margin: '20px',
  },
  container: ({ color }) => ({
    maxHeight: '100%',
    '& .MuiTableCell-root': {
      borderBottom: 'none',
    },
    '&  .MuiTableCell-head': {
      fontSize: '1rem',
      color: theme.palette.secondary.light,
      backgroundColor: color,
    },
  }),
  collapse: ({ color }) => ({
    backgroundColor: color,
  }),
}));

const Row = ({
  folder,
  rows,
  columns,
  handleAddRow,
  handleDeleteRow,
  color,
}) => {
  const { role } = useSelector(selectCurrentUser);
  const { collapse } = useStyles({ color });
  const [open, setOpen] = useState(false);

  const [openForm, setOpenForm] = useState(false);
  const handleOpenForm = () => {
    setOpenForm(true);
  };
  const handleCloseForm = () => {
    setOpenForm(false);
  };

  return (
    <>
      <TableRow className={collapse}>
        <TableCell style={{ width: 20 }}>
          <IconButton size="small" onClick={() => setOpen(!open)}>
            {open ? (
              <KeyboardArrowUp style={{ color: '#fff' }} />
            ) : (
              <KeyboardArrowDown style={{ color: '#fff' }} />
            )}
          </IconButton>
        </TableCell>
        <TableCell>
          <Typography variant={'body1'} style={{ color: '#fff' }}>
            {folder}
          </Typography>
        </TableCell>
        {role.name === EDUCATOR && (
          <TableCell align={'right'} style={{ width: 120, height: 73 }}>
            <Button
              color={color}
              size={'small'}
              icon={<Add />}
              onClick={handleOpenForm}
            />
          </TableCell>
        )}
      </TableRow>
      <TableRow>
        <TableCell
          style={{ padding: 0 }}
          colSpan={role.name === EDUCATOR ? 3 : 2}
        >
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Box margin={1} marginRight={0} marginLeft={0}>
              <Table
                rows={rows}
                columns={columns}
                color={color}
                handleDeleteRow={handleDeleteRow}
                nohead={true}
              />
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
      {openForm && (
        <FileForm
          open={openForm}
          closeForm={handleCloseForm}
          onSubmit={handleAddRow}
          color={color}
          formTitle={'Dodaj datoteku'}
          submitTitle={'Dodaj'}
          exists={true}
          values={{ folder: folder, fileName: '', file: null }}
        />
      )}
    </>
  );
};

const removeFile = async id => {
  try {
    await axios.delete(`/classroom/remove/file/${id}`, BASIC_HEADER_OPTIONS());
  } catch (e) {
    return Promise.reject(e);
  }
};

const addFile = async (data, file, classId) => {
  try {
    const id = (
      await axios.post(
        `/classroom/${classId}/add/file`,
        data,
        BASIC_HEADER_OPTIONS(),
      )
    ).data;
    await axios.post(
      `/classroom/${classId}/file/upload/${id}`,
      file,
      BASIC_HEADER_OPTIONS(),
    );
  } catch (e) {
    return Promise.reject(e);
  }
};

const Files = ({ classId, files, color }) => {
  const dispatch = useDispatch();
  const { paper, container } = useStyles({ color });
  const rows = useMemo(() => {
    const folders = [];
    files.forEach(file => {
      let updated = false;
      folders.forEach(f => {
        if (f.folder === file.folder) {
          f.rows.push(file);
          updated = true;
        }
      });
      if (!updated) folders.push({ folder: file.folder, rows: [file] });
    });
    return folders;
  }, [files]);

  const columns = useMemo(
    () => [
      {
        id: 'fileName',
        label: 'DATOTEKA',
        minWidth: 200,
        format: ({ row: { id, fileName }, color }) => (
          <Link
            href={`${PRODUCTION_URL}/classroom/${classId}/file/download/${id}`}
            download
            style={{ color: color }}
          >
            {fileName}
          </Link>
        ),
      },
    ],
    [classId],
  );

  const { role } = useSelector(selectCurrentUser);
  const { loading } = useSelector(selectClassroom);

  const [openForm, setOpenForm] = useState(false);
  const handleOpenForm = () => {
    setOpenForm(true);
  };
  const handleCloseForm = () => {
    setOpenForm(false);
  };
  const handleAddFile = values => {
    if (values.file) {
      addFile(
        { folder: values.folder, fileName: values.fileName },
        values.file,
        classId,
      ).then(res => dispatch(getClassroom(classId)));
    }
  };

  const [fileToRemove, setFileToRemove] = useState(false);
  const handleOpenDeleteDialog = id => {
    setFileToRemove(id);
  };
  const handleCloseDeleteDialog = () => {
    setFileToRemove(false);
  };
  const handleRemoveFile = () => {
    removeFile(fileToRemove).then(res => dispatch(getClassroom(classId)));
    handleCloseDeleteDialog();
  };

  return (
    <>
      {(role.name === EDUCATOR || rows.length > 0) && (
        <>
          <Fade in={true} timeout={500}>
            <Paper className={paper} elevation={10}>
              <TableContainer className={container}>
                <MUITable>
                  {role.name === EDUCATOR && (
                    <TableHead>
                      <TableRow>
                        <TableCell align={'left'} style={{ height: 73 }}>
                          <Button
                            color={color}
                            size={'small'}
                            icon={<Add />}
                            onClick={handleOpenForm}
                          />
                        </TableCell>
                        <TableCell />
                        <TableCell />
                      </TableRow>
                    </TableHead>
                  )}
                  <TableBody>
                    {rows.map((row, idx) => (
                      <Row
                        key={`${row.folder}${idx}`}
                        folder={row.folder}
                        rows={row.rows}
                        color={color}
                        handleAddRow={handleAddFile}
                        handleDeleteRow={handleOpenDeleteDialog}
                        columns={columns}
                      />
                    ))}
                  </TableBody>
                </MUITable>
              </TableContainer>
            </Paper>
          </Fade>
          {loading && <Loading color={'#fff'} minHeight={160} size={70} />}
        </>
      )}
      {openForm && (
        <FileForm
          open={openForm}
          closeForm={handleCloseForm}
          onSubmit={handleAddFile}
          color={color}
          formTitle={'Dodaj datoteku'}
          submitTitle={'Dodaj'}
        />
      )}
      <ConfirmDialog
        open={!!fileToRemove}
        color={color}
        handleClose={handleCloseDeleteDialog}
        onConfirm={handleRemoveFile}
        title={'Ukloniti datoteku?'}
        text={'Nakon ove akcije nije neće biti moguće vratiti datoteku!'}
      />
    </>
  );
};

export default Files;
