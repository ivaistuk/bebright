import { makeStyles } from '@material-ui/core/styles';
import { calculateColour } from '../../../utils/classroom-theme-colour';
import { Box, Paper, Typography } from '@material-ui/core';
import { Link } from 'react-router-dom';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    justifyContent: 'center',
    padding: theme.spacing(2.7),
  },
  paper: {
    minHeight: ({ size }) => size,
    minWidth: ({ size }) => size,
    backgroundColor: ({ title }) =>
      calculateColour(title.charCodeAt(0) + title.charCodeAt(1)),
  },
  name: {
    fontSize: 'xx-large',
    color: theme.palette.secondary.light,
    paddingTop: ({ size }) => `calc((${size}px - 48px) / 2)`,
  },
}));

const ClassroomPreview = ({ title, link, size = 250 }) => {
  const { container, paper, name } = useStyles({ title, size });

  return (
    <Box className={container}>
      <Link to={link}>
        <Paper className={paper} elevation={12}>
          <Typography align="center" className={name}>
            {title}
          </Typography>
        </Paper>
      </Link>
    </Box>
  );
};

export default ClassroomPreview;
