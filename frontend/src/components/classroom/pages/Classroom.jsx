import { Box, Container, Fade, Paper, Typography } from '@material-ui/core';
import { useHistory, useParams } from 'react-router';
import { useDispatch, useSelector } from 'react-redux';
import {
  selectAssignmentDeletedStatus,
  selectClassroom,
  selectCurrentUser,
} from '../../../store/selectors';
import React, { useEffect, useMemo, useState } from 'react';
import {
  deleteClassroom,
  editClassroom,
  getClassroom,
} from '../../../store/actions/classroom-actions';
import Alert from '../../notifications/Alert';
import { calculateColour } from '../../../utils/classroom-theme-colour';
import TabLayout from '../../tabs/TabLayout';
import { Delete, Edit, PeopleAlt } from '@material-ui/icons';
import ClassroomForm from '../forms/ClassroomForm';
import { EDUCATOR } from '../../../utils/role-names';
import Button from '../../styled-components/Button';
import TasksTable from '../tables/TasksTable';
import StudentsTable from '../tables/StudentsTable';
import OrganizersTable from '../tables/OrganizersTable';
import ConfirmDialog from '../../notifications/ConfirmDialog';
import { useItemPageStyles } from '../../../hooks/use-styles';
import { classroomAlerts } from '../../../utils/notifications/alerts';
import AlertGroup from '../../notifications/AlertGroup';
import Files from '../tables/Files';

const Classroom = () => {
  const dispatch = useDispatch();
  const { role } = useSelector(selectCurrentUser);
  const { data: classroom, error: getClassroomError } =
    useSelector(selectClassroom);
  const { status: assignmentDeletedStatus, error: assignmentDeletedError } =
    useSelector(selectAssignmentDeletedStatus);

  const alerts = useMemo(
    () => classroomAlerts(!!assignmentDeletedError, assignmentDeletedStatus),
    [assignmentDeletedError, assignmentDeletedStatus],
  );
  const { id: classId } = useParams();
  const history = useHistory();

  const classroomColor = useMemo(
    () =>
      calculateColour(
        classroom &&
          classroom.name.charCodeAt(0) + classroom.name.charCodeAt(1),
      ),
    [classroom],
  );

  const { title, container, paper, body } = useItemPageStyles({
    color: classroomColor,
  });

  useEffect(() => {
    dispatch(getClassroom(classId));
  }, [dispatch, classId]);

  const [openClassroomForm, setOpenClassroomForm] = useState(false);

  const handleOpenClassroomForm = () => {
    setOpenClassroomForm(true);
  };
  const handleCloseClassroomForm = () => {
    setOpenClassroomForm(false);
  };

  const handleClassroomSubmit = values => {
    const classInfo = {
      id: classId,
      name: values.classroom,
      picture: values.picture,
    };

    dispatch(editClassroom(classInfo));
  };

  const [confirmDelete, setConfirmDelete] = useState(false);
  const openDeleteDialog = () => {
    setConfirmDelete(true);
  };
  const closeDeleteDialog = () => {
    setConfirmDelete(false);
  };
  const handleDeleteClassroom = () => {
    dispatch(deleteClassroom(classId));
    closeDeleteDialog();
    history.push('/home');
  };

  return (
    <Container maxWidth="xl" className={container}>
      <Alert
        message={'Problem tijekom učitavanja učionice'}
        severity={'error'}
        shouldOpen={!!getClassroomError}
      />
      <Fade in={true} timeout={500}>
        <Paper className={paper} elevation={8}>
          <Box className={title}>
            <Button color={classroomColor} icon={<PeopleAlt />} />
            <Typography variant={'h3'}>
              {classroom && classroom.name}
            </Typography>
            {role.name === EDUCATOR && (
              <>
                <Button
                  onClick={handleOpenClassroomForm}
                  color={classroomColor}
                  size={'medium'}
                  icon={<Edit />}
                />
                <Button
                  onClick={openDeleteDialog}
                  color={'#c34d4d'}
                  size={'medium'}
                  icon={<Delete />}
                />
              </>
            )}
          </Box>

          <Box className={body}>
            <TabLayout
              data={[
                {
                  title: 'Zadatci',
                  component: (
                    <TasksTable classId={classId} color={classroomColor} />
                  ),
                },
                {
                  title: 'Materijali',
                  component: (
                    <Files
                      classId={classId}
                      files={classroom ? classroom.files : []}
                      color={classroomColor}
                    />
                  ),
                },
                {
                  title: 'Učenici',
                  component: (
                    <StudentsTable classId={classId} color={classroomColor} />
                  ),
                },
                {
                  title: 'Organizatori',
                  component: (
                    <OrganizersTable classId={classId} color={classroomColor} />
                  ),
                },
              ]}
            />
          </Box>
        </Paper>
      </Fade>
      {openClassroomForm && (
        <ClassroomForm
          open={true}
          closeForm={handleCloseClassroomForm}
          onSubmit={handleClassroomSubmit}
          formTitle={'Uredi učionicu'}
          submitTitle={'Uredi'}
          color={classroomColor}
          values={{ classroom: classroom && classroom.name, picture: null }}
        />
      )}

      <ConfirmDialog
        open={confirmDelete}
        title={'Želite obrisati učionicu?'}
        text={
          'Ukoliko to napravite obrisat će se i svi zadatci koji su zadani u sklopu ove učionice.'
        }
        onConfirm={handleDeleteClassroom}
        color={classroomColor}
        handleClose={closeDeleteDialog}
      />
      <AlertGroup data={alerts} />
    </Container>
  );
};

export default Classroom;
