import { useDispatch, useSelector } from 'react-redux';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { editUser } from '../../../store/actions/auth-actions';
import {
  Box,
  DialogContent,
  FormLabel,
  Paper,
  TextField,
  Tooltip,
  Typography,
} from '@material-ui/core';
import { useAppFormStyles } from '../../../hooks/use-styles';
import {
  selectCurrentUser,
  selectCurrentUserEditStatus,
} from '../../../store/selectors';
import Alert from '../../notifications/Alert';
import { RESET_USER_EDIT_STATUS } from '../../../store/types/auth-action-types';
import { adjustDateForNativeInput } from '../../../utils/date-formatter';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import React from 'react';
import Button from '../../styled-components/Button';
import { Face, Lock, Save } from '@material-ui/icons';

const useStyles = makeStyles(theme => ({
  field: {
    marginTop: 4,
    marginBottom: 10,
    '& input': { zIndex: 1, color: `${theme.palette.text.primary} !important` },
    '& fieldset': {
      backgroundColor: `${theme.palette.secondary.light} !important`,
    },
    '& .Mui-focused': {
      '& fieldset': {
        border: `none !important`,
        color: `${theme.palette.text.primary} !important`,
      },
    },
    '& label, .MuiInputBase-root': {
      color: `${theme.palette.secondary.light} !important`,
    },
  },
  disabled: {
    '& fieldset': {
      backgroundColor: `rgba(218, 216, 216) !important`,
    },
    '& input': { color: `rgb(128 125 125 / 71%) !important` },
  },
  label: {
    fontSize: '0.8rem',
    marginLeft: 15,
    '& :first-child': { marginLeft: 15 },
  },
  content: {
    padding: `15px !important`,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    height: '100%',
  },
}));

const UserForm = () => {
  const dispatch = useDispatch();
  const { id, name, familyName, email, birthDate, username } =
    useSelector(selectCurrentUser);
  const { status: edited, error } = useSelector(selectCurrentUserEditStatus);

  const formik = useFormik({
    initialValues: {
      id: id,
      name: name,
      familyName: familyName,
      username: username,
      password: '',
      passwordRepeated: '',
      email: email,
      birthDate: birthDate ? adjustDateForNativeInput(birthDate) : '',
    },
    validateOnChange: true,
    validateOnMount: false,
    validateOnBlur: false,
    validationSchema: Yup.object({
      name: Yup.string().required('Ime je obavezno.'),
      familyName: Yup.string().required('Prezime je obavezno.'),
      username: Yup.string().required('Korisničko ime je obavezno.'),
      passwordRepeated: Yup.string().oneOf(
        [Yup.ref('password'), ''],
        'Lozinke moraju biti iste!',
      ),
      email: Yup.string()
        .required('e-mail obavezan.')
        .email('Neispravan e-mail!'),
    }),
    onSubmit: values => {
      const data = {
        id: values.id,
        username: values.username,
        password: values.password !== '' ? values.password : null,
        email: values.email,
        name: values.name,
        familyName: values.familyName,
        birthDate: values.birthDate,
      };

      dispatch(editUser(data));
    },
  });

  const { background, errorText, input } = useAppFormStyles();
  const { label, field, disabled, content } = useStyles();
  return (
    <Box
      display={'flex'}
      justifyContent={'center'}
      padding={3}
      paddingTop={3}
      minHeight={'585px'}
    >
      <Alert
        message={'Neuspješno uređivanje korisnika!'}
        severity={'error'}
        shouldOpen={!!error}
      />
      <Alert
        message={'Promjene spremljene!'}
        severity={'success'}
        shouldOpen={edited}
        dispatchActionOnClose={{ type: RESET_USER_EDIT_STATUS }}
      />
      <Paper elevation={20} style={{ minWidth: '60%' }} className={background}>
        <form onSubmit={formik.handleSubmit} style={{ height: '100%' }}>
          <DialogContent className={content}>
            <Box>
              <Box
                display={'flex'}
                alignItems={'center'}
                justifyContent={'space-between'}
                marginBottom={'5px'}
              >
                <Box display={'flex'} alignItems={'center'}>
                  <Button size={'small'} icon={<Face />} />
                  <Typography variant={'h6'} style={{ marginLeft: 7 }}>
                    PODATCI
                  </Typography>
                </Box>
                <Tooltip title={'Spremi promjene'}>
                  <div>
                    <Button
                      size={'medium'}
                      type={'submit'}
                      icon={<Save />}
                      color={'#c34d4d'}
                    />
                  </div>
                </Tooltip>
              </Box>
              <Box display={'flex'} justifyContent={'space-between'}>
                <Box display={'flex'} flexDirection={'column'} minWidth={'48%'}>
                  <FormLabel className={label}>
                    Ime
                    <Typography component="span" className={errorText}>
                      {formik.errors.name ? formik.errors.name : null}
                    </Typography>
                  </FormLabel>

                  <TextField
                    variant="outlined"
                    margin="normal"
                    fullWidth
                    id="name"
                    name="name"
                    value={formik.values.name}
                    autoFocus
                    onChange={formik.handleChange}
                    className={clsx(input, field)}
                  />
                  <FormLabel className={label}>
                    Prezime
                    <Typography component="span" className={errorText}>
                      {formik.errors.familyName
                        ? formik.errors.familyName
                        : null}
                    </Typography>
                  </FormLabel>
                  <TextField
                    variant="outlined"
                    margin="normal"
                    fullWidth
                    name="familyName"
                    value={formik.values.familyName}
                    id="familyName"
                    onChange={formik.handleChange}
                    className={clsx(input, field)}
                  />
                </Box>
                <Box display={'flex'} flexDirection={'column'} minWidth={'48%'}>
                  <FormLabel className={label}>
                    E-mail
                    <Typography component="span" className={errorText}>
                      {formik.errors.email ? formik.errors.email : null}
                    </Typography>
                  </FormLabel>
                  <TextField
                    variant="outlined"
                    margin="normal"
                    fullWidth
                    id="email"
                    name="email"
                    value={formik.values.email}
                    autoFocus
                    onChange={formik.handleChange}
                    className={clsx(input, field)}
                  />

                  <FormLabel className={label}>
                    Rođendan
                    <Typography component="span" className={errorText} />
                  </FormLabel>
                  <TextField
                    id="birthDate"
                    name="birthDate"
                    margin="normal"
                    value={formik.values.birthDate}
                    onChange={formik.handleChange}
                    type="date"
                    fullWidth
                    variant={'outlined'}
                    className={clsx(input, field)}
                  />
                </Box>
              </Box>
            </Box>
            <Box>
              <Box display={'flex'} alignItems={'center'} marginBottom={'10px'}>
                <Button size={'small'} icon={<Lock />} />
                <Typography variant={'h6'} style={{ marginLeft: 7 }}>
                  RAČUN
                </Typography>
              </Box>
              <Box display={'flex'} justifyContent={'space-between'}>
                <Box display={'flex'} flexDirection={'column'} minWidth={'48%'}>
                  <FormLabel className={label}>
                    Lozinka{' '}
                    <Typography component="span" className={errorText} />
                  </FormLabel>
                  <TextField
                    variant="outlined"
                    margin="normal"
                    name="password"
                    value={formik.values.password}
                    placeholder={'(nepromijenjena)'}
                    type="password"
                    id="password"
                    onChange={formik.handleChange}
                    className={clsx(input, field)}
                  />
                  <FormLabel className={label}>
                    Ponovljena lozinka
                    <Typography component="span" className={errorText}>
                      {formik.errors.passwordRepeated
                        ? formik.errors.passwordRepeated
                        : null}
                    </Typography>
                  </FormLabel>
                  <TextField
                    variant="outlined"
                    margin="normal"
                    name="passwordRepeated"
                    value={formik.values.passwordRepeated}
                    placeholder={'(nepromijenjena)'}
                    type="password"
                    id="passwordRepeated"
                    onChange={formik.handleChange}
                    className={clsx(input, field)}
                  />
                </Box>
                <Box display={'flex'} flexDirection={'column'} minWidth={'48%'}>
                  <FormLabel className={label}>
                    Korisničko ime
                    <Typography component="span" className={errorText} />
                  </FormLabel>
                  <TextField
                    variant="outlined"
                    margin="normal"
                    name="username"
                    value={formik.values.username}
                    id="username"
                    disabled
                    className={clsx(input, field, disabled)}
                  />
                </Box>
              </Box>
            </Box>
          </DialogContent>
        </form>
      </Paper>
    </Box>
  );
};

export default UserForm;
