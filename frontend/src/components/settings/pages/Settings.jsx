import { useItemPageStyles } from '../../../hooks/use-styles';
import { Box, Container, Fade, Paper, Typography } from '@material-ui/core';
import { Settings as SettingsIcon } from '@material-ui/icons';
import UserForm from '../forms/UserForm';
import TabLayout from '../../tabs/TabLayout';
import Button from '../../styled-components/Button';

const Settings = () => {
  const { title, paper, body, container } = useItemPageStyles();
  return (
    <Container maxWidth={'xl'} className={container}>
      <Fade in={true} timeout={500}>
        <Paper className={paper} elevation={8}>
          <Box className={title}>
            <Button icon={<SettingsIcon />} />
            <Typography variant={'h3'}>Postavke</Typography>
          </Box>
          <Box className={body}>
            <TabLayout
              data={[
                {
                  title: 'Uredi profil',
                  component: <UserForm />,
                },
              ]}
            />
          </Box>
        </Paper>
      </Fade>
    </Container>
  );
};

export default Settings;
