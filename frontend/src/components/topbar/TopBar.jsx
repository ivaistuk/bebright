import { AppBar, IconButton, Toolbar, Typography } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import clsx from 'clsx';

import { useTopBarStyles } from '../../hooks/use-styles';

function TopBar({ handleOpen, isOpen, title }) {
  const {
    appBar,
    appBarShift,
    toolbar,
    title: titleClass,
    menuButtonHidden,
    menuButton,
  } = useTopBarStyles();

  return (
    <AppBar
      position="absolute"
      className={clsx(appBar, isOpen && appBarShift, 'zIndexed')}
    >
      <Toolbar className={toolbar}>
        <IconButton
          edge="start"
          color="inherit"
          aria-label="open drawer"
          onClick={handleOpen}
          className={clsx(menuButton, isOpen && menuButtonHidden)}
        >
          <MenuIcon />
        </IconButton>
        <Typography
          component="h1"
          variant="h6"
          color="inherit"
          noWrap
          className={titleClass}
        >
          {title}
        </Typography>
      </Toolbar>
    </AppBar>
  );
}

export default TopBar;
