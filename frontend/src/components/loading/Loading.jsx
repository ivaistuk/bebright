import { Box, CircularProgress, Container } from '@material-ui/core';

function Loading({ color, minHeight, size = 100, display = 'flex' }) {
  return (
    <Container maxWidth="lg">
      <Box
        display={display}
        flexDirection="column"
        justifyContent="center"
        minHeight={minHeight ? minHeight : '100vh'}
      >
        <Box alignSelf="center">
          <CircularProgress size={size} style={{ color: color }} />
        </Box>
      </Box>
    </Container>
  );
}

export default Loading;
