import { useCallback, useMemo } from 'react';
import { makeStyles } from '@material-ui/styles';
import {
  ListItem,
  ListItemIcon,
  ListItemText,
  Paper,
  Typography,
} from '@material-ui/core';
import PersonIcon from '@material-ui/icons/Person';
import SettingsIcon from '@material-ui/icons/Settings';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import DashboardIcon from '@material-ui/icons/Dashboard';
import EventIcon from '@material-ui/icons/Event';
import AssignmentIcon from '@material-ui/icons/Assignment';
import BarChartIcon from '@material-ui/icons/BarChart';
import { Link as RouterLink } from 'react-router-dom';
import { FormatListBulleted } from '@material-ui/icons';

const SidebarItem = ({ title = '', icon = 'dashboard', onClick, link }) => {
  const getIcon = useCallback(() => {
    switch (icon) {
      case 'chart':
        return <BarChartIcon />;
      case 'dashboard':
        return <DashboardIcon />;
      case 'assignment':
        return <AssignmentIcon />;
      case 'calendar':
        return <EventIcon />;
      case 'person':
        return <PersonIcon />;
      case 'settings':
        return <SettingsIcon />;
      case 'logout':
        return <ExitToAppIcon />;
      case 'list':
        return <FormatListBulleted />;
      default:
        return <PersonIcon />;
    }
  }, [icon]);

  const classes = makeStyles(theme => ({
    paper: {
      backgroundColor: theme.palette.secondary.light,
      color: theme.palette.primary.main,
    },
    icon: {
      color: theme.palette.primary.main,
      marginLeft: 7,
    },
  }))();

  const content = useMemo(
    () => (
      <Paper className={classes.paper} elevation={0}>
        <ListItem button onClick={onClick}>
          <ListItemIcon className={classes.icon}>{getIcon()}</ListItemIcon>
          <ListItemText
            primary={
              <Typography align={'center'} variant={'body2'} component={'span'}>
                {title}
              </Typography>
            }
          />
        </ListItem>
      </Paper>
    ),
    [classes.icon, classes.paper, getIcon, onClick, title],
  );

  if (link) return <RouterLink to={link}>{content}</RouterLink>;
  return <>{content}</>;
};

export default SidebarItem;
