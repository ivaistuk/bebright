import { Divider, List } from '@material-ui/core';
import SidebarItem from '../SideBarItem';

function Elements() {
  return (
    <>
      <List>
        <SidebarItem title={'Radna Ploča'} icon={'dashboard'} link={'/home'} />
        <SidebarItem
          title={'Postignuća'}
          icon={'chart'}
          link={'/achievements'}
        />
      </List>
      <Divider />
      <List>
        <SidebarItem title={'Kalendar'} icon={'calendar'} link={'/calendar'} />
        <SidebarItem
          title={'Zadatci'}
          icon={'assignment'}
          link={'/assignments'}
        />
      </List>
    </>
  );
}

export default Elements;
