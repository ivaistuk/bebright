import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import { useDispatch } from 'react-redux';
import clsx from 'clsx';
import { logout } from '../../store/actions/auth-actions';
import SidebarItem from './SideBarItem';
import { useSideBarStyles } from '../../hooks/use-styles';
import { Drawer, IconButton, Divider, List } from '@material-ui/core';

function SideBar({ handleClose, isOpen, elements }) {
  const { drawerPaper, drawerPaperClose, toolbarIcon, chevron } =
    useSideBarStyles();
  const dispatch = useDispatch();

  const handleLogout = () => {
    localStorage.removeItem('bebright-current-user-token');
    dispatch(logout());
  };

  return (
    <Drawer
      variant="permanent"
      classes={{
        paper: clsx(drawerPaper, !isOpen && drawerPaperClose),
      }}
      open={isOpen}
    >
      <div className={toolbarIcon}>
        <IconButton onClick={handleClose}>
          <ChevronLeftIcon className={chevron} />
        </IconButton>
      </div>
      <Divider />
      {elements}
      <Divider />
      <List>
        <SidebarItem title={'Profil'} icon={'person'} link={'/profile'} />
        <SidebarItem title={'Postavke'} icon={'settings'} link={'/settings'} />
        <SidebarItem title={'Odjava'} icon={'logout'} onClick={handleLogout} />
      </List>
    </Drawer>
  );
}

export default SideBar;
