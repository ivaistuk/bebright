import { useEffect, useRef, useState } from 'react';
import { useFormik } from 'formik';
import { useAppFormStyles } from '../../../hooks/use-styles';
import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  TextField,
} from '@material-ui/core';
import StyledButton from '../../styled-components/Button';
import { FileCopy } from '@material-ui/icons';
import { useDispatch, useSelector } from 'react-redux';
import {
  selectAssignment,
  selectBadgeAddStatus,
} from '../../../store/selectors';
import { getAssignment } from '../../../store/actions/assignment-actions';
import axios from 'axios';
import { BASIC_HEADER_OPTIONS } from '../../../utils/configure-axios';
import Badge from '../../profile/components/Badge';
import Alert from '../../notifications/Alert';
import { addBadgeToUser } from '../../../store/actions/achievement-actions';
import { RESET_ADD_BADGE_STATUS } from '../../../store/types/achievment-action-types';

const fetchAllBadges = async () => {
  try {
    return (await axios.get('/achievement/badge/all', BASIC_HEADER_OPTIONS()))
      .data;
  } catch (e) {
    return Promise.reject(e);
  }
};

const SubmissionForm = ({
  open = false,
  assignmentId,
  closeForm,
  onSubmit,
  formTitle = '',
  submitTitle = '',
  edit = false,
  color,
  values = {
    uploadedText: '',
    fileName: '',
    file: null,
    points: '',
    userId: undefined,
  },
}) => {
  const { data: assignment } = useSelector(selectAssignment);
  const dispatch = useDispatch();
  useEffect(() => {
    if (assignmentId) dispatch(getAssignment(assignmentId));
  }, [assignmentId, dispatch]);

  const [badges, setbadges] = useState([]);
  const { status, error } = useSelector(selectBadgeAddStatus);
  useEffect(() => {
    fetchAllBadges().then(res => setbadges(res));
  }, [setbadges]);

  const handleAddBadge = badgeId => {
    dispatch(addBadgeToUser(badgeId, values.userId));
  };

  const fileInput = useRef(null);
  const formik = useFormik({
    initialValues: { ...values },
    validateOnChange: false,
    validateOnMount: false,
    validateOnBlur: false,
    onSubmit: values => {
      onSubmit(values);
      closeForm();
    },
  });

  const { background, input, text, modal } = useAppFormStyles({ color });

  const handleNumberChange = e => {
    if (isNaN(e.target.value)) return;
    const value = e.target.value;
    if (assignment && assignment.points)
      if (value <= assignment.points) formik.setFieldValue('points', value);
      else formik.setFieldValue('points', assignment.points);
    else formik.setFieldValue('points', value);
  };

  const handleFileChange = e => {
    const reader = new FileReader();
    reader.onload = e => {
      const array = new Int8Array(e.target.result);
      const bytes = [];
      for (let i = 0; i < array.length; i++) bytes.push(array[i]);
      formik.setFieldValue('file', bytes.length > 0 ? bytes : '');
    };
    const file = e.target.files[0];
    formik.setFieldValue('fileName', file ? file.name : '');
    reader.readAsArrayBuffer(file);
  };

  return (
    <Dialog
      open={open}
      onClose={closeForm}
      maxWidth={'sm'}
      fullWidth
      className={modal}
    >
      <Alert
        message={'Značka nije dodana korisniku!'}
        severity={'error'}
        shouldOpen={!!error}
      />
      <Alert
        message={'Značka je dodana!'}
        severity={'success'}
        shouldOpen={status}
        dispatchActionOnClose={{ type: RESET_ADD_BADGE_STATUS }}
      />
      <DialogTitle className={background}>{formTitle}</DialogTitle>
      <form className={'force-secondary-color'} onSubmit={formik.handleSubmit}>
        <DialogContent className={background}>
          <DialogContentText className={text}>
            {edit
              ? `Ocijenite predani odgovor dodijeljivanjem broja bodova. Ukoliko nije navedeno ograničenje bodova možete dati proizvoljnu količinu istih.`
              : 'Predajte zadatak tako da napišete odgovor direktno u predloženo polje ili priložite vlastitu datoteku. Moguće je napraviti više predaja dok će ocjenjivač biti u stanju vidjeti samo zadnje predanu'}
          </DialogContentText>
          {!edit ? (
            <>
              <TextField
                id="uploadedText"
                name="uploadedText"
                margin="normal"
                value={formik.values.uploadedText}
                onChange={formik.handleChange}
                label={'Priloženi text'}
                multiline
                fullWidth
                rows={13}
                variant={'outlined'}
                InputLabelProps={{
                  shrink: true,
                }}
                className={input}
              />
              <Box
                marginTop={2}
                display={'flex'}
                justifyContent={'space-between'}
              >
                <StyledButton
                  onClick={() => {
                    if (fileInput) fileInput.current.click();
                  }}
                  color={color}
                  icon={<FileCopy />}
                />
                <TextField
                  id="fileName"
                  name="fileName"
                  margin="none"
                  value={formik.values.fileName}
                  onChange={formik.handleChange}
                  label="Naziv Datoteke"
                  variant={'outlined'}
                  InputLabelProps={{
                    shrink: true,
                  }}
                  className={input}
                  style={{ width: '87%' }}
                  placeholder={'Nema datoteke'}
                />
              </Box>
              <input
                id="fileInput"
                name="fileInput"
                onChange={handleFileChange}
                type="file"
                style={{ display: 'none' }}
                ref={fileInput}
              />
            </>
          ) : (
            <>
              <TextField
                id="points"
                name="points"
                margin="normal"
                value={formik.values.points}
                onChange={handleNumberChange}
                label="Bodovi"
                fullWidth
                variant={'outlined'}
                InputLabelProps={{
                  shrink: true,
                }}
                className={input}
              />
              <DialogContentText
                className={text}
                style={{ marginTop: 35, marginBottom: 20 }}
              >
                Ukoliko želite možete dodati neku od dolje ponuđenih znački
                korisniku. Dodati ju možete tako što kliknete na nju.
              </DialogContentText>
              <Box
                display={'flex'}
                justifyContent={'center'}
                flexWrap={'wrap'}
                maxHeight={'150px'}
                maxWidth={'100%'}
                overflow={'auto'}
              >
                {badges.map(badge => (
                  <Box padding={2} key={badge.id}>
                    <Badge
                      name={badge.name}
                      id={badge.id}
                      onClick={() => handleAddBadge(badge.id)}
                    />
                  </Box>
                ))}
              </Box>
            </>
          )}
        </DialogContent>
        <DialogActions className={background}>
          <Button type="submit" variant="text" className={text}>
            {submitTitle}
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  );
};

export default SubmissionForm;
