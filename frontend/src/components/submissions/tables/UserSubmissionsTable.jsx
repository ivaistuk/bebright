import { useDispatch, useSelector } from 'react-redux';
import {
  selectCurrentSubmission,
  selectStudentSubmissions,
  selectSubmissionCreatedStatus,
  selectSubmissionDeleteStatus,
} from '../../../store/selectors';
import React, { useEffect, useState } from 'react';
import Table from '../../table/Table';
import { userSubmissionsColumns } from '../../../utils/columns-template';
import ConfirmDialog from '../../notifications/ConfirmDialog';
import {
  createSubmision,
  deleteSubmission,
  getStudentSubmissions,
  getSubmission,
} from '../../../store/actions/submission-actions';
import {
  RESET_SUBMISSION_CREATED_STATUS,
  RESET_SUBMISSION_DELETED_STATUS,
} from '../../../store/types/submission-action-types';
import SubmissionForm from '../forms/SubmissionForm';

const resetSubmissionCrud = dispatch => {
  dispatch({ type: RESET_SUBMISSION_CREATED_STATUS });
  dispatch({ type: RESET_SUBMISSION_DELETED_STATUS });
};

const UserSubmissionsTable = ({ color, assignmentId }) => {
  const { data: submissions } = useSelector(selectStudentSubmissions);
  const { data: selectedSubmission, loading: fetchingSubmission } = useSelector(
    selectCurrentSubmission,
  );
  const { status: created } = useSelector(selectSubmissionCreatedStatus);
  const { status: deleted } = useSelector(selectSubmissionDeleteStatus);

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getStudentSubmissions(assignmentId));
  }, [assignmentId, dispatch]);

  useEffect(() => {
    if (created || deleted) {
      dispatch(getStudentSubmissions(assignmentId));
      resetSubmissionCrud(dispatch);
    }
  }, [assignmentId, created, deleted, dispatch]);

  const [openForm, setOpenForm] = useState(false);
  const handleOpenForm = () => {
    setOpenForm(true);
  };
  const handleCloseForm = () => {
    setOpenForm(false);
  };
  const handleCreate = values => {
    const submission = {
      assignmentId: assignmentId,
      uploadedText: values.uploadedText,
      fileName: values.file !== '' ? values.fileName : null,
    };
    dispatch(
      createSubmision(submission, values.file !== '' ? values.file : null),
    );
  };

  const [confirmDelete, setConfirmDelete] = useState(false);
  const handleOpenDeleteDialog = () => {
    setConfirmDelete(true);
  };
  const handleCloseDeleteDialog = () => {
    setConfirmDelete(false);
  };
  const handleDelete = () => {
    dispatch(deleteSubmission(selectedSubmission.id));
    handleCloseDeleteDialog();
  };

  return (
    <>
      <Table
        rows={submissions}
        columns={userSubmissionsColumns}
        color={color}
        handleAddRow={handleOpenForm}
        handleDeleteRow={id => {
          dispatch(getSubmission(id));
          handleOpenDeleteDialog();
        }}
        reverse
      />
      {openForm && !fetchingSubmission && (
        <SubmissionForm
          open={true}
          edit={false}
          closeForm={handleCloseForm}
          onSubmit={handleCreate}
          formTitle={'Predaj zadatak'}
          submitTitle={'Predaj'}
          color={color}
          assignmentId={assignmentId}
        />
      )}

      <ConfirmDialog
        open={confirmDelete && !fetchingSubmission}
        color={color}
        handleClose={handleCloseDeleteDialog}
        onConfirm={handleDelete}
        title={'Ukloniti zadatak?'}
        text={'Nakon ove akcije nije više moguće vratiti zadatak!'}
      />
    </>
  );
};

export default UserSubmissionsTable;
