import { useDispatch, useSelector } from 'react-redux';
import {
  selectAssignmentSubmissions,
  selectCurrentSubmission,
  selectGradeSubmissionStatus,
} from '../../../store/selectors';
import React, { useEffect, useState } from 'react';
import Table from '../../table/Table';
import { taskSubmissionsColumns } from '../../../utils/columns-template';
import {
  getAssignmentSubmissions,
  getSubmission,
  gradeSubmission,
} from '../../../store/actions/submission-actions';
import { RESET_SUBMISSION_GRADE_STATUS } from '../../../store/types/submission-action-types';
import SubmissionForm from '../forms/SubmissionForm';
import Loading from '../../loading/Loading';

const resetSubmissionCrud = dispatch => {
  dispatch({ type: RESET_SUBMISSION_GRADE_STATUS });
};

const TaskSubmissionsTable = ({ color, assignmentId }) => {
  const dispatch = useDispatch();
  const { data: submissions } = useSelector(selectAssignmentSubmissions);
  const { data: selectedSubmission, loading: fetchingSubmission } = useSelector(
    selectCurrentSubmission,
  );
  const { status: edited, loading } = useSelector(selectGradeSubmissionStatus);

  useEffect(() => {
    dispatch(getAssignmentSubmissions(assignmentId));
  }, [assignmentId, dispatch]);

  useEffect(() => {
    if (edited) {
      dispatch(getAssignmentSubmissions(assignmentId));
      resetSubmissionCrud(dispatch);
    }
  }, [assignmentId, dispatch, edited]);

  const [openForm, setOpenForm] = useState(false);
  const handleOpenForm = () => {
    setOpenForm(true);
  };
  const handleCloseForm = () => {
    setOpenForm(false);
  };
  const handleEdit = values => {
    const submission = {
      id: selectedSubmission.id,
      points: values.points,
    };
    dispatch(gradeSubmission(submission));
  };

  if (loading) return <Loading color={color} />;
  return (
    <>
      <Table
        rows={submissions}
        columns={taskSubmissionsColumns}
        color={color}
        handleEditRow={id => {
          dispatch(getSubmission(id));
          handleOpenForm();
        }}
      />
      {openForm && !fetchingSubmission && (
        <SubmissionForm
          open={true}
          edit={true}
          closeForm={handleCloseForm}
          onSubmit={handleEdit}
          formTitle={'Ocijeni zadatak'}
          submitTitle={'Ocijeni'}
          color={color}
          values={{
            userId: selectedSubmission.student.id,
            points: selectedSubmission.points,
          }}
          assignmentId={assignmentId}
        />
      )}
    </>
  );
};

export default TaskSubmissionsTable;
