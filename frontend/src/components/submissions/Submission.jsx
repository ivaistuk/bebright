import { useDispatch, useSelector } from 'react-redux';
import {
  selectCurrentSubmission,
  selectCurrentUser,
  selectGradeSubmissionStatus,
} from '../../store/selectors';
import { useHistory, useParams } from 'react-router';
import React, { useEffect, useState } from 'react';
import {
  deleteSubmission,
  getSubmission,
  gradeSubmission,
} from '../../store/actions/submission-actions';
import { RESET_SUBMISSION_GRADE_STATUS } from '../../store/types/submission-action-types';
import { useItemPageStyles } from '../../hooks/use-styles';
import {
  Box,
  Container,
  Fade,
  Paper,
  Tooltip,
  Typography,
} from '@material-ui/core';
import Alert from '../notifications/Alert';
import { EDUCATOR, STUDENT } from '../../utils/role-names';
import Button from '../styled-components/Button';
import { Delete, Edit, FormatListBulleted } from '@material-ui/icons';
import ConfirmDialog from '../notifications/ConfirmDialog';
import SubmissionInfo from './components/SubmissionInfo';
import SubmissionForm from './forms/SubmissionForm';

const resetSubmissionCrud = dispatch => {
  dispatch({ type: RESET_SUBMISSION_GRADE_STATUS });
};

const Submission = () => {
  const dispatch = useDispatch();
  const { role } = useSelector(selectCurrentUser);
  const { data: submission, error: fetchSubmissionError } = useSelector(
    selectCurrentSubmission,
  );
  const { id: submissionId } = useParams();
  const history = useHistory();

  useEffect(() => {
    dispatch(getSubmission(submissionId));
  }, [dispatch, submissionId]);

  const { status: edited } = useSelector(selectGradeSubmissionStatus);
  useEffect(() => {
    if (edited) {
      dispatch(getSubmission(submissionId));
      resetSubmissionCrud(dispatch);
    }
  }, [dispatch, edited, submissionId]);

  const [openForm, setOpenForm] = useState(false);
  const handleOpenForm = () => {
    setOpenForm(true);
  };
  const handleCloseForm = () => {
    setOpenForm(false);
  };

  const handleEdit = values => {
    const data = {
      id: submissionId,
      points: values.points,
    };
    dispatch(gradeSubmission(data));
  };

  const [confirmDelete, setConfirmDelete] = useState(false);
  const handleOpenDeleteDialog = () => {
    setConfirmDelete(true);
  };
  const handleCloseDeleteDialog = () => {
    setConfirmDelete(false);
  };
  const handleDelete = () => {
    dispatch(deleteSubmission(submissionId));
    history.goBack();
  };

  const { container, paper, body, title } = useItemPageStyles();

  return (
    <Container maxWidth="xl" className={container}>
      <Alert
        message={'Problem tijekom učitavanja predaje'}
        severity={'error'}
        shouldOpen={!!fetchSubmissionError}
      />
      <Fade in={true} timeout={500}>
        <Paper className={paper} elevation={8}>
          <Box className={title}>
            <Button icon={<FormatListBulleted />} />
            <Typography variant={'h3'}>
              Predaja
              <span style={{ color: 'rgba(196,196,196,0.85)' }}>
                #{submissionId}
              </span>
            </Typography>
            {role.name === EDUCATOR && (
              <Tooltip title={'Ocijeni'}>
                <div>
                  <Button
                    onClick={handleOpenForm}
                    size={'medium'}
                    icon={<Edit />}
                  />
                </div>
              </Tooltip>
            )}
            {role.name === STUDENT && (
              <Button
                size={'medium'}
                onClick={handleOpenDeleteDialog}
                icon={<Delete />}
              />
            )}
          </Box>
          <Box className={body} justifyContent={'flex-end'}>
            <SubmissionInfo
              submission={submission}
              assignment={submission && submission.assignment}
            />
          </Box>
        </Paper>
      </Fade>

      {openForm && (
        <SubmissionForm
          open={true}
          edit={true}
          closeForm={handleCloseForm}
          onSubmit={handleEdit}
          formTitle={'Ocijeni zadatak'}
          submitTitle={'Ocijeni'}
          values={
            submission && {
              userId: submission.student.id,
              points: submission.points,
              uploadedText: '',
              fileName: '',
            }
          }
          assignmentId={submission && submission.assignment.id}
        />
      )}

      {role.name === STUDENT && (
        <ConfirmDialog
          open={confirmDelete}
          handleClose={handleCloseDeleteDialog}
          onConfirm={handleDelete}
          title={'Ukloniti predaju?'}
          text={'Nakon ove akcije nije moguće vratiti predaju!'}
        />
      )}
    </Container>
  );
};
export default Submission;
