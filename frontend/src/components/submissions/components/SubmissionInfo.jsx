import { useItemInfoStyles } from '../../../hooks/use-styles';
import { Box, Fade, Link, Paper, Tooltip, Typography } from '@material-ui/core';
import Button from '../../styled-components/Button';
import {
  AccessTime,
  Description,
  ExitToApp,
  InsertEmoticon,
  List,
  VerticalAlignTop,
} from '@material-ui/icons';
import { trimDate } from '../../../utils/date-formatter';
import React from 'react';
import { useHistory } from 'react-router';
import { PRODUCTION_URL } from '../../../utils/configure-axios';

const SubmissionInfo = ({ submission, assignment }) => {
  const { panel, paragraph, heading, field, title } = useItemInfoStyles();
  const history = useHistory();
  return (
    <Box display={'flex'} justifyContent={'center'} padding={3} paddingTop={3}>
      <Fade in={true} timeout={500}>
        <Paper className={panel} elevation={20}>
          <Box display={'flex'} padding={'15px'} marginBottom={'5px'}>
            <Tooltip title={'Vidi zadatak'} placement={'top'}>
              <div>
                <Button
                  size={'medium'}
                  icon={<ExitToApp />}
                  onClick={() =>
                    history.push(`/assignment/${assignment && assignment.id}`)
                  }
                />
              </div>
            </Tooltip>

            <Typography variant={'h5'} className={title}>
              {`PREDAJA ZADATKA ${
                assignment && assignment.title.toUpperCase()
              }`}
            </Typography>
          </Box>
          <Box className={field}>
            <Box display={'flex'}>
              <Button size={'small'} icon={<List />} />
              <Typography variant={'h6'} className={heading}>
                PREDANI TEKST
              </Typography>
            </Box>
            <Typography
              variant={'body2'}
              className={paragraph}
              style={{ minHeight: 150 }}
            >
              {submission ? (
                submission.uploadedText !== '' ? (
                  submission.uploadedText
                ) : (
                  <i style={{ color: 'rgba(152,151,151,0.77)' }}>
                    ( Text nije dodan )
                  </i>
                )
              ) : (
                <i style={{ color: 'rgba(152,151,151,0.77)' }}>
                  ( Text nije dodan )
                </i>
              )}
            </Typography>
          </Box>
          <Box display={'flex'}>
            <Box className={field} maxWidth={'50%'}>
              <Box display={'flex'}>
                <Button size={'small'} icon={<AccessTime />} />
                <Typography variant={'h6'} className={heading}>
                  PREDANO
                </Typography>
              </Box>
              <Typography variant={'body2'} className={paragraph}>
                {submission &&
                  submission.created &&
                  trimDate(submission.created)}
              </Typography>
            </Box>
            <Box className={field} maxWidth={'50%'}>
              <Box display={'flex'}>
                <Button size={'small'} icon={<Description />} />
                <Typography variant={'h6'} className={heading}>
                  DATOTEKA
                </Typography>
              </Box>
              <Typography variant={'body2'} className={paragraph}>
                {submission && submission.fileName ? (
                  <Link
                    href={`${PRODUCTION_URL}/api/assignment/${assignment.id}/file/download`}
                    download
                  >
                    {submission.fileName}
                  </Link>
                ) : (
                  <i style={{ color: 'rgba(152,151,151,0.77)' }}>
                    ( Nema priloženih datoteka )
                  </i>
                )}
              </Typography>
            </Box>
          </Box>
          <Box display={'flex'}>
            <Box className={field} maxWidth={'50%'}>
              <Box display={'flex'}>
                <Button size={'small'} icon={<InsertEmoticon />} />
                <Typography variant={'h6'} className={heading}>
                  OSTVARENI BODOVI
                </Typography>
              </Box>
              <Typography variant={'body2'} className={paragraph}>
                {submission && submission.points ? (
                  submission.points
                ) : (
                  <i style={{ color: 'rgba(152,151,151,0.77)' }}>
                    ( Nije ocijenjeno )
                  </i>
                )}
              </Typography>
            </Box>
            <Box className={field} maxWidth={'50%'}>
              <Box display={'flex'}>
                <Button size={'small'} icon={<VerticalAlignTop />} />
                <Typography variant={'h6'} className={heading}>
                  MAX. BODOVI
                </Typography>
              </Box>
              <Typography variant={'body2'} className={paragraph}>
                {assignment && assignment.points ? (
                  assignment.points
                ) : (
                  <i style={{ color: 'rgba(152,151,151,0.77)' }}>
                    ( Neograničeno )
                  </i>
                )}
              </Typography>
            </Box>
          </Box>
        </Paper>
      </Fade>
    </Box>
  );
};
export default SubmissionInfo;
