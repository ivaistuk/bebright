import { useDispatch, useSelector } from 'react-redux';
import {
  selectCurrentSubmission,
  selectGradedSubmissions,
  selectGradeSubmissionStatus,
  selectUngradedSubmissions,
} from '../../store/selectors';
import React, { useEffect, useState } from 'react';
import {
  getGradedSubmissions,
  getSubmission,
  getUngradedSubmissions,
  gradeSubmission,
} from '../../store/actions/submission-actions';
import { RESET_SUBMISSION_GRADE_STATUS } from '../../store/types/submission-action-types';
import Alert from '../notifications/Alert';
import { Box, Container, Fade, Paper, Typography } from '@material-ui/core';
import Button from '../styled-components/Button';
import { FormatListBulleted } from '@material-ui/icons';
import Table from '../table/Table';
import {
  gradedSubmissionsColumns,
  ungradedSubmissionsColumns,
} from '../../utils/columns-template';
import SubmissionForm from './forms/SubmissionForm';
import { useItemPageStyles } from '../../hooks/use-styles';
import Loading from '../loading/Loading';
import TabLayout from '../tabs/TabLayout';

const resetSubmissionCrud = dispatch => {
  dispatch({ type: RESET_SUBMISSION_GRADE_STATUS });
};

const Grading = () => {
  const dispatch = useDispatch();
  const {
    data: ungradedSubmissions,
    error: ungradedSubmissionError,
    loading: fetchingUngradedSubmissions,
  } = useSelector(selectUngradedSubmissions);
  const {
    data: gradedSubmissions,
    error: gradedSubmissionsError,
    loading: fetchingGradedSubmissions,
  } = useSelector(selectGradedSubmissions);

  const { data: selectedSubmission, loading: fetchingSubmission } = useSelector(
    selectCurrentSubmission,
  );
  const { status: edited } = useSelector(selectGradeSubmissionStatus);

  useEffect(() => {
    dispatch(getUngradedSubmissions());
    dispatch(getGradedSubmissions());
  }, [dispatch]);

  useEffect(() => {
    if (edited) {
      dispatch(getGradedSubmissions());
      dispatch(getUngradedSubmissions());
      resetSubmissionCrud(dispatch);
    }
  }, [dispatch, edited]);

  const [openForm, setOpenForm] = useState(false);
  const handleOpenForm = () => {
    setOpenForm(true);
  };
  const handleCloseForm = () => {
    setOpenForm(false);
  };
  const handleEdit = values => {
    const submission = {
      id: selectedSubmission.id,
      points: values.points,
    };
    dispatch(gradeSubmission(submission));
  };
  const { title, paper, body, container } = useItemPageStyles();

  return (
    <Container maxWidth={'xl'} className={container}>
      <Alert
        message={'Problem tijekom učitavanja predaja'}
        severity={'error'}
        shouldOpen={!!ungradedSubmissionError || !!gradedSubmissionsError}
      />
      <Fade in={true} timeout={500}>
        <Paper className={paper} elevation={8}>
          <Box className={title}>
            <Button size={'large'} icon={<FormatListBulleted />} />
            <Typography variant={'h3'}>Predaje</Typography>
          </Box>
          <Box className={body}>
            <TabLayout
              data={[
                {
                  title: 'Neocijenjeno',
                  component: (
                    <Table
                      rows={ungradedSubmissions}
                      columns={ungradedSubmissionsColumns}
                      handleEditRow={id => {
                        dispatch(getSubmission(id));
                        handleOpenForm();
                      }}
                    />
                  ),
                },
                {
                  title: 'Ocijenjeno',
                  component: (
                    <Table
                      rows={gradedSubmissions}
                      columns={gradedSubmissionsColumns}
                      handleEditRow={id => {
                        dispatch(getSubmission(id));
                        handleOpenForm();
                      }}
                    />
                  ),
                },
              ]}
            />
            {(fetchingUngradedSubmissions || fetchingGradedSubmissions) && (
              <Loading color={'#fff'} minHeight={160} size={70} />
            )}
          </Box>
        </Paper>
      </Fade>
      {openForm && !fetchingSubmission && (
        <SubmissionForm
          open={true}
          edit={true}
          closeForm={handleCloseForm}
          onSubmit={handleEdit}
          formTitle={'Ocijeni zadatak'}
          submitTitle={'Ocijeni'}
          values={{
            userId: selectedSubmission.student.id,
            points: selectedSubmission.points,
          }}
          assignmentId={selectedSubmission && selectedSubmission.assignment.id}
        />
      )}
    </Container>
  );
};

export default Grading;
