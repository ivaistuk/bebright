import { useDispatch, useSelector } from 'react-redux';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import {
  Box,
  Button,
  Container,
  Fade,
  Paper,
  TextField,
  Typography,
} from '@material-ui/core';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import '../../styles/components/auth.css';
import logo from '../../assets/logo.png';
import { useFormStyles } from '../../hooks/use-styles';
import clsx from 'clsx';
import { login } from '../../store/actions/auth-actions';
import { Link } from 'react-router-dom';
import Alert from '../notifications/Alert';
import { LOGIN_RESET } from '../../store/types/auth-action-types';
import { selectCurrentAuthStatus } from '../../store/selectors';

function Login() {
  const { error } = useSelector(selectCurrentAuthStatus);
  const dispach = useDispatch();
  const classes = useFormStyles();

  const formik = useFormik({
    initialValues: {
      username: '',
      password: '',
    },
    validateOnChange: false,
    validateOnMount: false,
    validateOnBlur: false,
    validationSchema: Yup.object({
      username: Yup.string().required(),
      password: Yup.string().required(),
    }),
    onSubmit: values => {
      const loginData = {
        username: values.username,
        password: values.password,
      };

      dispach(login(loginData));
    },
  });

  return (
    <Fade in={true} timeout={500}>
      <Box className="login-background">
        <Alert
          message={'Neispravno korisničko ime ili lozinka!'}
          severity={'error'}
          shouldOpen={!!error}
          dispatchActionOnClose={{ type: LOGIN_RESET }}
        />
        <Container maxWidth="sm">
          <Fade in={true} timeout={2000}>
            <Paper elevation={8} className={classes.formPaper}>
              <Box component="span" p={5} marginBottom={2}>
                <img alt="logo" src={logo} className={classes.logo} />
                <Typography component="span" variant="h3">
                  Be
                  <Typography
                    component="span"
                    variant="inherit"
                    color="primary"
                  >
                    Bright
                  </Typography>
                </Typography>
              </Box>
              <Box component="span">
                <LockOutlinedIcon />
                <Typography component="span" variant="h5">
                  Prijava
                </Typography>
              </Box>
              <form
                className={clsx(classes.form, 'force-secondary-color')}
                onSubmit={formik.handleSubmit}
              >
                <TextField
                  variant="outlined"
                  margin="normal"
                  fullWidth
                  id="username"
                  label="Korisničko ime"
                  name="username"
                  value={formik.values.username}
                  autoComplete="username"
                  autoFocus
                  onChange={formik.handleChange}
                  required
                />
                <TextField
                  variant="outlined"
                  margin="normal"
                  fullWidth
                  name="password"
                  value={formik.values.password}
                  label="Lozinka"
                  type="password"
                  id="password"
                  autoComplete="current-password"
                  onChange={formik.handleChange}
                  required
                />
                <Box className={classes.submit}>
                  <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                  >
                    Prijavi se
                  </Button>
                </Box>
                <Box className={classes.linkToOtherForm}>
                  <Typography variant="body2">
                    {'Nemate napravljen račun? '}
                    <Typography
                      variant="body2"
                      component="span"
                      color="primary"
                    >
                      <Link to="/register">Registriraj se!</Link>
                    </Typography>
                  </Typography>
                </Box>
              </form>
            </Paper>
          </Fade>
        </Container>
      </Box>
    </Fade>
  );
}

export default Login;
