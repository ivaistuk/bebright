import * as ROLES from '../../utils/role-names';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { useDispatch, useSelector } from 'react-redux';
import {
  Box,
  Button,
  Container,
  Fade,
  FormControlLabel,
  Paper,
  Radio,
  RadioGroup,
  TextField,
  Typography,
} from '@material-ui/core';
import { LockOpenRounded } from '@material-ui/icons';

import '../../styles/components/auth.css';
import { useFormStyles } from '../../hooks/use-styles';
import clsx from 'clsx';
import { selectRegisteredStatus } from '../../store/selectors';
import { register } from '../../store/actions/auth-actions';
import Alert from '../notifications/Alert';
import { Link } from 'react-router-dom';

function Register() {
  const dispatch = useDispatch();
  const classes = useFormStyles();
  const { error } = useSelector(selectRegisteredStatus);

  const formik = useFormik({
    initialValues: {
      name: '',
      familyName: '',
      username: '',
      password: '',
      passwordRepeated: '',
      email: '',
      roleName: ROLES.STUDENT,
      birthDate: null,
    },
    validateOnChange: true,
    validateOnMount: false,
    validateOnBlur: false,
    validationSchema: Yup.object({
      name: Yup.string().required('Ime je obavezno.'),
      familyName: Yup.string().required('Prezime je obavezno.'),
      username: Yup.string().required('Korisničko ime je obavezno.'),
      password: Yup.string().required('Lozinka je obavezna.'),
      passwordRepeated: Yup.string()
        .required('Ponovljena lozinka je obavezna')
        .oneOf([Yup.ref('password'), ''], 'Lozinke moraju biti iste!'),
      email: Yup.string()
        .required('e-mail obavezan.')
        .email('Neispravan e-mail!'),
    }),
    onSubmit: values => {
      const registerData = {
        username: values.username,
        password: values.password,
        email: values.email,
        name: values.name,
        familyName: values.familyName,
        roleName: values.roleName,
        birthDate: values.birthDate,
      };

      const loginData = {
        username: values.username,
        password: values.password,
      };

      dispatch(register(registerData, loginData));
    },
  });

  return (
    <Fade in={true} timeout={500}>
      <Box className="registration-background">
        <Alert
          message={'Korisničko ime ili email već postoji!'}
          severity={'error'}
          shouldOpen={!!error}
        />
        <Container maxWidth="sm">
          <Fade in={true} timeout={2000}>
            <Paper elevation={8} className={classes.formPaper}>
              <Box component="span">
                <LockOpenRounded />
                <Typography component="span" variant="h5">
                  Registracija
                </Typography>
              </Box>
              <form
                className={clsx(classes.form, 'force-secondary-color')}
                onSubmit={formik.handleSubmit}
              >
                <TextField
                  variant="outlined"
                  margin="normal"
                  fullWidth
                  id=""
                  label="Ime"
                  name="name"
                  value={formik.values.name}
                  autoFocus
                  onChange={formik.handleChange}
                />
                <Typography component="p" className="error-text">
                  {formik.errors.name ? formik.errors.name : null}
                </Typography>
                <TextField
                  variant="outlined"
                  margin="normal"
                  fullWidth
                  name="familyName"
                  value={formik.values.familyName}
                  label="Prezime"
                  id="familyName"
                  onChange={formik.handleChange}
                />
                <Typography component="p" className="error-text">
                  {formik.errors.familyName ? formik.errors.familyName : null}
                </Typography>
                <TextField
                  variant="outlined"
                  margin="normal"
                  fullWidth
                  id="username"
                  label="Korisničko ime"
                  name="username"
                  value={formik.values.username}
                  autoFocus
                  onChange={formik.handleChange}
                />
                <Typography component="p" className="error-text">
                  {formik.errors.username ? formik.errors.username : null}
                </Typography>
                <TextField
                  variant="outlined"
                  margin="normal"
                  fullWidth
                  name="password"
                  value={formik.values.password}
                  label="Lozinka"
                  type="password"
                  id="password"
                  onChange={formik.handleChange}
                />
                <Typography component="p" className="error-text">
                  {formik.errors.password ? formik.errors.password : null}
                </Typography>
                <TextField
                  variant="outlined"
                  margin="normal"
                  fullWidth
                  name="passwordRepeated"
                  value={formik.values.passwordRepeated}
                  label="Ponovljena lozinka"
                  type="password"
                  id="passwordRepeated"
                  onChange={formik.handleChange}
                />
                <Typography component="p" className="error-text">
                  {formik.errors.passwordRepeated
                    ? formik.errors.passwordRepeated
                    : null}
                </Typography>
                <TextField
                  variant="outlined"
                  margin="normal"
                  fullWidth
                  id="email"
                  label="E-mail"
                  name="email"
                  value={formik.values.email}
                  autoFocus
                  onChange={formik.handleChange}
                />
                <Typography component="p" className="error-text">
                  {formik.errors.email ? formik.errors.email : null}
                </Typography>
                <RadioGroup
                  row
                  name="role"
                  defaultValue={ROLES.STUDENT}
                  value={formik.values.roleName}
                  onChange={formik.handleChange}
                  className="radio-group"
                >
                  <FormControlLabel
                    control={
                      <Radio
                        color="primary"
                        name="roleName"
                        value={ROLES.STUDENT}
                        className="force-primary-color"
                      />
                    }
                    label="Učenik"
                  />
                  <FormControlLabel
                    control={
                      <Radio
                        color="primary"
                        name="roleName"
                        value={ROLES.EDUCATOR}
                        className="force-primary-color"
                      />
                    }
                    label="Predavač"
                  />
                </RadioGroup>
                {/* Date picker */}
                <Box className={classes.submit}>
                  <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                  >
                    Registriraj se
                  </Button>
                </Box>
                <Box className={classes.linkToOtherForm}>
                  <Typography variant="body2">
                    {'Imate već račun? '}
                    <Typography
                      component="span"
                      variant="body2"
                      color="primary"
                    >
                      <Link to={'/login'}>Prijavi se!</Link>
                    </Typography>
                  </Typography>
                </Box>
              </form>
            </Paper>
          </Fade>
        </Container>
      </Box>
    </Fade>
  );
}

export default Register;
