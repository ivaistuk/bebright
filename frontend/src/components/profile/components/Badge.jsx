import { Box, Tooltip } from '@material-ui/core';
import Button from '../../styled-components/Button';
import { useMemo } from 'react';
import { calculateColour } from '../../../utils/classroom-theme-colour';
import { badgeIcons } from '../../../utils/badge-icons';

const Badge = ({ id, name, onClick }) => {
  const color = useMemo(
    () => calculateColour(name.charCodeAt(0) + name.charCodeAt(1)),
    [name],
  );

  const { Icon } = useMemo(() => badgeIcons[id - 1], [id]);

  return (
    <Box>
      <Tooltip title={name}>
        <div>
          <Button
            color={color}
            icon={<Icon />}
            onClick={onClick ? onClick : undefined}
          />
        </div>
      </Tooltip>
    </Box>
  );
};

export default Badge;
