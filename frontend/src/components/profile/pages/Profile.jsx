import {
  Box,
  Container,
  Fade,
  IconButton,
  LinearProgress,
  Paper,
  Tooltip,
  Typography,
} from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import clsx from 'clsx';
import ChevronLeft from '@material-ui/icons/ChevronLeftOutlined';
import ChevronRight from '@material-ui/icons/ChevronRightOutlined';
import { FixedButton } from '../../styled-components/FixedButton';
import { useEffect, useRef, useState } from 'react';
import { makeStyles, useTheme } from '@material-ui/core/styles';

import '../../../styles/components/profile.css';
import Badge from '../components/Badge';
import { Add, Face, Mail, VerifiedUser } from '@material-ui/icons';
import {
  selectBadges,
  selectCurrentUser,
  selectUserData,
} from '../../../store/selectors';
import Loading from '../../loading/Loading';
import Alert from '../../notifications/Alert';
import { getUserBadges } from '../../../store/actions/achievement-actions';
import { useParams } from 'react-router';
import { getCurrentUser, getUser } from '../../../store/actions/auth-actions';
import { STUDENT } from '../../../utils/role-names';
import Button from '../../styled-components/Button';

const useStyles = makeStyles(theme => ({
  profileContainer: {
    flexGrow: 1,
    paddingLeft: theme.spacing(12),
    paddingRight: theme.spacing(12),
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
  },
  profileBox: {
    display: 'flex',
    flexWrap: 'wrap',
    padding: theme.spacing(4),
    paddingBottom: theme.spacing(1),
    paddingTop: theme.spacing(1),
    borderRadius: 10,
    backgroundColor: theme.palette.primary.main,
  },
  leftProfileBox: {
    flexGrow: 1,
    margin: theme.spacing(2),
  },
  rightProfileBox: {
    width: 550,
    height: '100%',
    margin: 'auto',
    paddingTop: theme.spacing(2),
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  rightProfileBoxClosed: {
    width: 0,
    height: 0,
    overflow: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  profilePicture: {
    height: `calc(60vh - 2*${theme.spacing(14)}px)`,
    width: `calc(60vh - 2*${theme.spacing(14)}px)`,
    minHeight: 200,
    minWidth: 200,
    borderRadius: 7,
    border: `1px solid ${theme.palette.background.default}`,
    margin: 0,
    backgroundSize: '50%',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    position: 'relative',
  },
  badgeContainer: {
    display: 'flex',
    flexWrap: 'wrap',
    maxHeight: 690,
    paddingLeft: 16,
    overflowY: 'auto',
    overflowX: 'hidden',
  },
  openBadgesIcon: {
    alignSelf: 'flex-end',
    width: 'min-content',
  },
  closedBadgesIcon: {
    width: 0,
    overflow: 'hidden',
  },
  visibleClosedBadgesIcon: {
    alignSelf: 'flex-start',
    width: 'min-content',
  },
  addPictureHiddenInput: {
    display: 'none',
  },
  chevron: {
    color: theme.palette.secondary.light,
  },
  progress: {
    backgroundColor: theme.palette.secondary.light,
  },
}));

function Profile() {
  const { id } = useParams();
  const user = useSelector(!id ? selectCurrentUser : selectUserData);
  const dispatch = useDispatch();
  const { loading, data, error } = useSelector(selectBadges);
  const [open, setOpen] = useState(false);
  const pictureInput = useRef(null);

  useEffect(() => {
    if (id) dispatch(getUser(id));
    if (user && user.role.name === STUDENT)
      dispatch(getUserBadges(id ? id : user.id));
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dispatch, id]);

  useEffect(() => dispatch(getCurrentUser()), [dispatch]);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleAddButtonClick = () => {
    pictureInput.current.click();
  };

  const theme = useTheme();
  const {
    addPictureHiddenInput,
    badgeContainer,
    chevron,
    closedBadgesIcon,
    leftProfileBox,
    profileBox,
    openBadgesIcon,
    profileContainer,
    profilePicture,
    rightProfileBox,
    rightProfileBoxClosed,
    visibleClosedBadgesIcon,
    progress,
  } = useStyles();

  if (loading) return <Loading />;
  return (
    <Container maxWidth="lg" className={profileContainer}>
      <Alert
        message={'Nije moguće dohavtiti značke!'}
        severity={'error'}
        shouldOpen={!!error}
      />
      <Fade in={true} timeout={500}>
        <Paper className={profileBox} elevation={8}>
          <Box
            display="flex"
            flexDirection="column"
            justifyContent="start"
            className={leftProfileBox}
          >
            {user && user.role.name === STUDENT ? (
              <Tooltip title={'Prikaži značke'}>
                <div
                  className={clsx(
                    !open && openBadgesIcon,
                    open && closedBadgesIcon,
                  )}
                >
                  <IconButton onClick={handleOpen} title={'Prikaži značke'}>
                    <ChevronLeft className={chevron} />
                  </IconButton>
                </div>
              </Tooltip>
            ) : (
              <Box height={theme.spacing(5)} />
            )}
            <Box margin="auto">
              <Paper
                className={clsx(profilePicture, 'profile-picture')}
                elevation={10}
              >
                {!id && (
                  <FixedButton
                    top="89%"
                    right={-23}
                    onClick={handleAddButtonClick}
                    buttonContent={<Add />}
                    title="Dodaj sliku"
                  />
                )}
              </Paper>
            </Box>
            <Box padding={3}>
              <Typography
                component="h1"
                variant="h4"
                color="textSecondary"
                align="center"
              >
                {user && user.name} {user && user.familyName}
              </Typography>
              {user && user.role.name === STUDENT && (
                <Box marginTop={1} marginBottom={2} display="flex">
                  <Box flexGrow={1} padding={2} paddingLeft={0}>
                    <LinearProgress
                      variant="determinate"
                      value={
                        user &&
                        user.role.name === STUDENT &&
                        ((user.experience - user.previous) /
                          (user.required - user.previous)) *
                          100
                      }
                      color="secondary"
                      className={progress}
                    />
                  </Box>
                  <Typography component="h1" variant="h6" color="secondary">
                    LEVEL
                    <Typography
                      component="span"
                      variant="h6"
                      color="textSecondary"
                    >{` ${user && user.level}`}</Typography>
                  </Typography>
                </Box>
              )}
              <Box display={'flex'} flexDirection={'column'} marginTop={5}>
                {user && user.role.name === STUDENT && (
                  <Box
                    marginRight={5}
                    display={'flex'}
                    alignItems={'center'}
                    marginBottom={2}
                  >
                    <Button
                      size={'small'}
                      style={{ marginRight: 7 }}
                      icon={<VerifiedUser />}
                    />
                    <Typography component="h1" variant="h6" color="secondary">
                      RANK
                    </Typography>
                    <Typography
                      component="h1"
                      variant="h6"
                      color="textSecondary"
                    >
                      #{user && user.rank.title}
                    </Typography>
                  </Box>
                )}
                <Box
                  display={'flex'}
                  alignItems={'center'}
                  marginRight={5}
                  marginBottom={2}
                >
                  <Button
                    size={'small'}
                    style={{ marginRight: 7 }}
                    icon={<Face />}
                  />
                  <Typography component="h1" variant="h6" color="secondary">
                    USERNAME
                  </Typography>
                  <Typography component="h1" variant="h6" color="textSecondary">
                    #{user && user.username}
                  </Typography>
                </Box>
                <Box display={'flex'} alignItems={'center'} marginBottom={2}>
                  <Button
                    size={'small'}
                    style={{ marginRight: 7 }}
                    icon={<Mail />}
                  />
                  <Typography component="h1" variant="h6" color="secondary">
                    EMAIL
                  </Typography>
                  <Typography component="h1" variant="h6" color="textSecondary">
                    #{user && user.email}
                  </Typography>
                </Box>
              </Box>
            </Box>
          </Box>
          {user && user.role.name === STUDENT && (
            <Box
              display="flex"
              flexDirection="column"
              className={clsx(
                open && rightProfileBox,
                !open && rightProfileBoxClosed,
              )}
            >
              <div className={visibleClosedBadgesIcon}>
                <Tooltip title={'Sakrij značke'}>
                  <div>
                    <IconButton onClick={handleClose}>
                      <ChevronRight className={chevron} />
                    </IconButton>
                  </div>
                </Tooltip>
              </div>
              <Box className={badgeContainer}>
                {data.map((badge, idx) => (
                  <Box padding={2} key={idx} height={'fit-content'}>
                    <Badge id={badge.id} name={badge.name} />
                  </Box>
                ))}
              </Box>
            </Box>
          )}
        </Paper>
      </Fade>
      <input type="file" className={addPictureHiddenInput} ref={pictureInput} />
    </Container>
  );
}

export default Profile;
