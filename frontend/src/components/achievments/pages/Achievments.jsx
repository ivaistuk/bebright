import { useDispatch, useSelector } from 'react-redux';
import { selectRankedStudents } from '../../../store/selectors';
import React, { useEffect } from 'react';
import { getRankedStudents } from '../../../store/actions/auth-actions';
import { useItemPageStyles } from '../../../hooks/use-styles';
import { Box, Container, Fade, Paper, Typography } from '@material-ui/core';
import Alert from '../../notifications/Alert';
import Button from '../../styled-components/Button';
import { Equalizer } from '@material-ui/icons';
import Table from '../../table/Table';
import { rankedUsersColumns } from '../../../utils/columns-template';
import Loading from '../../loading/Loading';

const Achievments = () => {
  const dispatch = useDispatch();
  const { data: students, loading, error } = useSelector(selectRankedStudents);
  useEffect(() => {
    dispatch(getRankedStudents());
  }, [dispatch]);

  const { title, paper, body, container } = useItemPageStyles();

  return (
    <Container maxWidth={'xl'} className={container}>
      <Alert
        message={'Problem tijekom učitavanja rang ljestvice'}
        severity={'error'}
        shouldOpen={!!error}
      />
      <Fade in={true} timeout={500}>
        <Paper className={paper} elevation={8}>
          <Box className={title}>
            <Button icon={<Equalizer />} />
            <Typography variant={'h3'}>Rang ljestvica</Typography>
          </Box>
          <Box className={body}>
            <Table rows={students} columns={rankedUsersColumns} mark reverse />
            {loading && <Loading color={'#fff'} minHeight={160} size={70} />}
          </Box>
        </Paper>
      </Fade>
    </Container>
  );
};

export default Achievments;
