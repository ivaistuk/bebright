import { Portal, Snackbar } from '@material-ui/core';
import { Alert as MUIAlert } from '@material-ui/lab';
import { useEffect, useState } from 'react';

const Info = ({ message = '', shouldOpen = false }) => {
  const [open, setOpen] = useState(true);

  useEffect(() => {
    setOpen(shouldOpen);
  }, [shouldOpen]);

  const handleClose = (event, reason) => {
    if (reason === 'clickaway' || reason === 'timeout') return;
    setOpen(false);
  };

  return (
    <Portal>
      <Snackbar
        open={open}
        onClose={handleClose}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        style={{ top: 85 }}
      >
        <MUIAlert onClose={handleClose} severity={'info'} variant="filled">
          {message}
        </MUIAlert>
      </Snackbar>
    </Portal>
  );
};

export default Info;
