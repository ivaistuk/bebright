import { Portal, Snackbar } from '@material-ui/core';
import { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { Alert as MUIAlert } from '@material-ui/lab';

const Alert = ({ severity, message, shouldOpen, dispatchActionOnClose }) => {
  const [open, setOpen] = useState(false);
  const dispatch = useDispatch();

  useEffect(() => {
    if (shouldOpen !== undefined) setOpen(shouldOpen);
  }, [shouldOpen]);

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') return;
    setOpen(false);
    if (dispatchActionOnClose) dispatch(dispatchActionOnClose);
  };

  return (
    <Portal>
      <Snackbar
        open={open}
        autoHideDuration={2000}
        onClose={handleClose}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        style={{ zIndex: 2000 }}
      >
        <MUIAlert onClose={handleClose} severity={severity} variant="filled">
          {message}
        </MUIAlert>
      </Snackbar>
    </Portal>
  );
};

export default Alert;
