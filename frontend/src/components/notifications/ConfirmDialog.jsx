import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from '@material-ui/core';
import { useAppFormStyles } from '../../hooks/use-styles';

const ConfirmDialog = ({
  open = false,
  handleClose,
  onConfirm,
  color,
  title = '',
  text = '',
}) => {
  const { background, text: textStyle, warning } = useAppFormStyles({ color });
  return (
    <Dialog open={open}>
      <DialogTitle className={background}>{title}</DialogTitle>
      <DialogContent className={background}>
        <DialogContentText>{text}</DialogContentText>
      </DialogContent>
      <DialogActions className={background}>
        <Button
          variant={'outlined'}
          onClick={handleClose}
          className={textStyle}
        >
          Odustani
        </Button>
        <Button variant={'outlined'} onClick={onConfirm} className={warning}>
          Potvrdi
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default ConfirmDialog;
