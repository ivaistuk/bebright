import Alert from './Alert';

const AlertGroup = ({ data = [] }) => {
  return (
    <>
      {data.map(d => (
        <Alert
          message={d.message}
          severity={d.severity}
          shouldOpen={d.shouldOpen}
          dispatchActionOnClose={d.dispatchActionOnClose}
          key={d.message}
        />
      ))}
    </>
  );
};

export default AlertGroup;
