import axios from 'axios';

export const BASIC_HEADER_OPTIONS = () => ({
  headers: {
    authorization:
      'Bearer ' + localStorage.getItem('bebright-current-user-token'),
    accept: 'application/json',
    'Content-Type': 'application/json',
  },
});

const configure = baseURL => {
  axios.defaults.baseURL = baseURL;
};

// eslint-disable-next-line
const LOCALHOST = 'http://localhost:8080/api';
// eslint-disable-next-line
const DEPLOYED = 'https://bebright-server.herokuapp.com/api';
export const PRODUCTION_URL = LOCALHOST;

export default configure;
