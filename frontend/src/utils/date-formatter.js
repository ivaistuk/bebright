export const adjustDateForNativeInput = dateString =>
  dateString ? dateString.substr(0, 19) : '';

export const trimDate = dateString =>
  new Date(dateString).toUTCString().substr(5, 17);
