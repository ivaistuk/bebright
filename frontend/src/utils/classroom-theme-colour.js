const colors = [
  '#529992',
  '#1f8e6d',
  '#b7a94c',
  '#90b147',
  '#589552',
  '#2f6d80',
];

export const calculateColour = character => {
  return colors[character % colors.length];
};
