import {
  RESET_CLASSROOM_CREATED_STATUS,
  RESET_CLASSROOM_DELETE_STATUS,
} from '../../store/types/classroom-action-types';
import { RESET_ASSIGNEMENT_DELETE_STATUS } from '../../store/types/assignment-action-types';

export const dashboardAlerts = (so1, so2, so3, so4) => [
  {
    message: 'Neuspjelo dohvaćanje učionica!',
    severity: 'error',
    shouldOpen: so1,
    dispatchActionOnClose: undefined,
  },
  {
    message: 'Neuspjelo stvaranje učionice!',
    severity: 'error',
    shouldOpen: so2,
    dispatchActionOnClose: { type: RESET_CLASSROOM_CREATED_STATUS },
  },

  {
    message: 'Učionica nije obrisana!',
    severity: 'error',
    shouldOpen: so3,
    dispatchActionOnClose: { type: RESET_CLASSROOM_DELETE_STATUS },
  },
  {
    message: 'Uspješno brisanje učionice!',
    severity: 'success',
    shouldOpen: so4,
    dispatchActionOnClose: { type: RESET_CLASSROOM_DELETE_STATUS },
  },
];

export const classroomAlerts = (so1, so2) => [
  {
    message: 'Zadatak nije obrisan!',
    severity: 'error',
    shouldOpen: so1,
    dispatchActionOnClose: { type: RESET_ASSIGNEMENT_DELETE_STATUS },
  },
  {
    message: 'Uspješno brisanje zadatka!',
    severity: 'success',
    shouldOpen: so2,
    dispatchActionOnClose: { type: RESET_ASSIGNEMENT_DELETE_STATUS },
  },
];
