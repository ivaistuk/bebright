import { Link as RouterLink } from 'react-router-dom';
import { trimDate } from './date-formatter';
import React from 'react';
import { Link, Tooltip } from '@material-ui/core';
import { MoreHoriz } from '@material-ui/icons';
import { STUDENT } from './role-names';
import { PRODUCTION_URL } from './configure-axios';

export const userColumns = [
  {
    id: 'id',
    label: '',
    format: ({ row: { id }, color }) => (
      <RouterLink to={`/profile/${id}`}>
        <Tooltip title={'Pojedinosti'} placement={'top'}>
          <MoreHoriz
            style={{
              cursor: 'pointer',
              color: color,
              display: 'flex',
              margin: 'auto',
            }}
          />
        </Tooltip>
      </RouterLink>
    ),
  },
  {
    id: 'name',
    label: 'IME',
    minWidth: 200,
    format: ({ row: { name, id }, color }) => (
      <RouterLink
        to={`/profile/${id}`}
        style={{ color: color }}
        className={'MuiLink-underlineHover'}
      >
        {name}
      </RouterLink>
    ),
  },
  { id: 'familyName', label: 'PREZIME', minWidth: 200 },
  {
    id: 'email',
    label: 'EMAIL',
    minWidth: 200,
  },
];

export const rankedUsersColumns = [
  {
    id: 'id',
    label: '',
    format: ({ row: { id }, color }) => (
      <RouterLink to={`/profile/${id}`}>
        <Tooltip title={'Pojedinosti'} placement={'top'}>
          <MoreHoriz
            style={{
              cursor: 'pointer',
              color: color,
              display: 'flex',
              margin: 'auto',
            }}
          />
        </Tooltip>
      </RouterLink>
    ),
  },
  {
    id: 'number',
    label: '',
    format: ({ index, color }) => (
      <span style={{ fontWeight: 'bold', color: color }}>{`${index}.`}</span>
    ),
  },
  {
    id: 'name',
    label: 'IME',
    minWidth: 150,
    format: ({ row: { name, id }, color }) => (
      <RouterLink
        to={`/profile/${id}`}
        style={{ color: color }}
        className={'MuiLink-underlineHover'}
      >
        {name}
      </RouterLink>
    ),
  },
  { id: 'familyName', label: 'PREZIME', minWidth: 150 },
  {
    id: 'email',
    label: 'EMAIL',
    minWidth: 150,
  },
  {
    id: 'level',
    label: 'LEVEL',
    minWidth: 150,
  },
  { id: 'experience', label: 'UKUPNI BODOVI', minWidth: 150 },
  { id: 'required', label: 'BODOVI ZA NOVI LEVEL', minWidth: 150 },
  {
    id: 'rank',
    label: 'RANK',
    minWidth: 100,
    format: ({
      row: {
        rank: { title },
      },
      color,
    }) => <span style={{ fontWeight: 'bold', color: color }}>{title}</span>,
  },
];

export const taskColumns = [
  {
    id: 'id',
    label: '',
    format: ({ row: { id }, color }) => (
      <RouterLink to={`/assignment/${id}`}>
        <Tooltip title={'Pojedinosti'} placement={'top'}>
          <MoreHoriz
            style={{
              cursor: 'pointer',
              color: color,
              display: 'flex',
              margin: 'auto',
            }}
          />
        </Tooltip>
      </RouterLink>
    ),
  },
  {
    id: 'title',
    label: 'NAZIV',
    minWidth: 170,
    format: ({ row: { id, title }, color }) => (
      <RouterLink
        to={`/assignment/${id}`}
        style={{ color: color }}
        className={'MuiLink-underlineHover'}
      >
        {title}
      </RouterLink>
    ),
  },
  { id: 'description', label: 'OPIS', minWidth: 250 },
  {
    id: 'activeUntil',
    label: 'ROK ZA PREDAJU',
    minWidth: 170,
    format: ({ row: { activeUntil } }) =>
      activeUntil ? trimDate(activeUntil) : '',
  },
  {
    id: 'maxPoints',
    label: 'MAX. BODOVI',
    minWidth: 170,
    format: ({ row: { points } }) => (points ? points : 'Nije definirano'),
  },
  {
    id: 'fileName',
    label: 'DATOTEKA',
    minWidth: 170,
    format: ({ row: { fileName, id }, color }) => (
      <Link
        href={`${PRODUCTION_URL}/assignment/${id}/file/download`}
        download
        style={{ color: color }}
      >
        {fileName}
      </Link>
    ),
  },
  {
    id: 'points',
    show: ({ role: { name } }) => name === STUDENT,
    minWidth: 170,
    label: 'OSTVARENI BODOVI',
    format: ({ row: { earned } }) => (earned ? earned : 'Nije ocijenjeno'),
  },
];

export const userSubmissionsColumns = [
  {
    id: 'id',
    label: '',
    format: ({ row: { id }, color }) => (
      <RouterLink to={`/submission/${id}`}>
        <Tooltip title={'Pojedinosti'} placement={'top'}>
          <MoreHoriz
            style={{
              cursor: 'pointer',
              color: color,
              display: 'flex',
              margin: 'auto',
            }}
          />
        </Tooltip>
      </RouterLink>
    ),
  },
  {
    id: 'uploadedText',
    label: 'TEKST',
    minWidth: 400,
  },
  {
    id: 'fileName',
    label: 'DATOTEKA',
    minWidth: 200,
    format: ({ row: { fileName, id }, color }) => (
      <Link
        href={`${PRODUCTION_URL}/submission/${id}/file/download`}
        download
        style={{ color: color }}
      >
        {fileName}
      </Link>
    ),
  },
  {
    id: 'points',
    label: 'BODOVI',
    minWidth: 170,
    format: ({ row: { points } }) =>
      points !== null ? points : 'Nije ocijenjeno',
  },
  {
    id: 'maxPoints',
    label: 'MAX. BODOVI',
    minWidth: 170,
    format: ({
      row: {
        assignment: { points },
      },
    }) => (points ? points : 'Nije definirano'),
  },
];

export const taskSubmissionsColumns = [
  {
    id: 'id',
    label: '',
    format: ({ row: { id }, color }) => (
      <RouterLink to={`/submission/${id}`}>
        <Tooltip title={'Pojedinosti'} placement={'top'}>
          <MoreHoriz
            style={{
              cursor: 'pointer',
              color: color,
              display: 'flex',
              margin: 'auto',
            }}
          />
        </Tooltip>
      </RouterLink>
    ),
  },
  {
    id: 'student',
    label: 'UČENIK',
    minWidth: 200,
    format: ({
      row: {
        student: { id, name },
      },
      color,
    }) => (
      <RouterLink
        to={`/profile/${id}`}
        style={{ color: color }}
        className={'MuiLink-underlineHover'}
      >
        {name}
      </RouterLink>
    ),
  },
  {
    id: 'uploadedText',
    label: 'TEKST',
    minWidth: 400,
  },
  {
    id: 'fileName',
    label: 'DATOTEKA',
    minWidth: 200,
    format: ({ row: { fileName, id } }, color) => (
      <Link
        href={`${PRODUCTION_URL}/submission/${id}/file/download`}
        download
        style={{ color: color }}
      >
        {fileName}
      </Link>
    ),
  },
  {
    id: 'points',
    label: 'DODIJELJENI BODOVI',
    minWidth: 170,
    format: ({ row: { points } }) =>
      points !== null ? points : 'Nije ocijenjeno',
  },
  {
    id: 'maxPoints',
    label: 'MAX. BODOVI',
    minWidth: 170,
    format: ({
      row: {
        assignment: { points },
      },
    }) => (points ? points : 'Nije definirano'),
  },
];

export const ungradedSubmissionsColumns = [
  {
    id: 'id',
    label: '',
    format: ({ row: { id }, color }) => (
      <RouterLink to={`/submission/${id}`}>
        <Tooltip title={'Pojedinosti'} placement={'top'}>
          <MoreHoriz
            style={{
              cursor: 'pointer',
              color: color,
              display: 'flex',
              margin: 'auto',
            }}
          />
        </Tooltip>
      </RouterLink>
    ),
  },
  {
    id: 'student',
    label: 'UČENIK',
    minWidth: 170,
    format: ({
      row: {
        student: { id, name },
      },
      color,
    }) => (
      <RouterLink
        to={`/profile/${id}`}
        style={{ color: color }}
        className={'MuiLink-underlineHover'}
      >
        {name}
      </RouterLink>
    ),
  },
  {
    id: 'assignment',
    label: 'ZADATAK',
    minWidth: 170,
    format: ({
      row: {
        assignment: { id, title },
      },
      color,
    }) => (
      <RouterLink
        to={`/assignment/${id}`}
        style={{ color: color }}
        className={'MuiLink-underlineHover'}
      >
        {title}
      </RouterLink>
    ),
  },
  {
    id: 'uploadedText',
    label: 'TEKST',
    minWidth: 400,
  },
  {
    id: 'fileName',
    label: 'DATOTEKA',
    minWidth: 170,
    format: ({ row: { fileName, id }, color }) => (
      <Link
        href={`${PRODUCTION_URL}/submission/${id}/file/download`}
        download
        style={{ color: color }}
      >
        {fileName}
      </Link>
    ),
  },
  {
    id: 'points',
    label: 'MAX. BODOVI',
    minWidth: 170,
    format: ({
      row: {
        assignment: { points },
      },
    }) => (points ? points : 'Nije definirano'),
  },
];

export const gradedSubmissionsColumns = [
  {
    id: 'id',
    label: '',
    format: ({ row: { id }, color }) => (
      <RouterLink to={`/submission/${id}`}>
        <Tooltip title={'Pojedinosti'} placement={'top'}>
          <MoreHoriz
            style={{
              cursor: 'pointer',
              color: color,
              display: 'flex',
              margin: 'auto',
            }}
          />
        </Tooltip>
      </RouterLink>
    ),
  },
  {
    id: 'student',
    label: 'UČENIK',
    minWidth: 170,
    format: ({
      row: {
        student: { id, name },
      },
      color,
    }) => (
      <RouterLink
        to={`/profile/${id}`}
        style={{ color: color }}
        className={'MuiLink-underlineHover'}
      >
        {name}
      </RouterLink>
    ),
  },
  {
    id: 'assignment',
    label: 'ZADATAK',
    minWidth: 170,
    format: ({
      row: {
        assignment: { id, title },
      },
      color,
    }) => (
      <RouterLink
        to={`/assignment/${id}`}
        style={{ color: color }}
        className={'MuiLink-underlineHover'}
      >
        {title}
      </RouterLink>
    ),
  },
  {
    id: 'uploadedText',
    label: 'TEKST',
    minWidth: 400,
  },
  {
    id: 'fileName',
    label: 'DATOTEKA',
    minWidth: 170,
    format: ({ row: { fileName, id }, color }) => (
      <Link
        href={`${PRODUCTION_URL}/submission/${id}/file/download`}
        download
        style={{ color: color }}
      >
        {fileName}
      </Link>
    ),
  },
  {
    id: 'points',
    label: 'DODIJELJENI BODOVI',
    minWidth: 170,
    format: ({ row: { points } }) =>
      points !== null ? points : 'Nije ocijenjeno',
  },
  {
    id: 'maxPoints',
    label: 'MAX. BODOVI',
    minWidth: 170,
    format: ({
      row: {
        assignment: { points },
      },
    }) => (points ? points : 'Nije definirano'),
  },
];
