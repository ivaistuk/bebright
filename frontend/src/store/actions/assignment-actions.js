import axios from 'axios';
import { BASIC_HEADER_OPTIONS } from '../../utils/configure-axios';
import {
  CREATE_ASSIGNEMENT,
  CREATE_ASSIGNEMENT_FAILURE,
  CREATE_ASSIGNEMENT_SUCCESS,
  DELETE_ASSIGNEMENT,
  DELETE_ASSIGNEMENT_FAILURE,
  DELETE_ASSIGNEMENT_SUCCESS,
  EDIT_ASSIGNEMENT,
  EDIT_ASSIGNEMENT_FAILURE,
  EDIT_ASSIGNEMENT_SUCCESS,
  GET_ALL_CLSSROOM_ASSIGNEMENTS,
  GET_ALL_CLSSROOM_ASSIGNEMENTS_FAILURE,
  GET_ALL_CLSSROOM_ASSIGNEMENTS_SUCESS,
  GET_ASSIGNMENT,
  GET_ASSIGNMENT_FAILURE,
  GET_ASSIGNMENT_SUCCESS,
  GET_USER_ASSIGNMENTS,
  GET_USER_ASSIGNMENTS_FAILURE,
  GET_USER_ASSIGNMENTS_SUCCESS,
} from '../types/assignment-action-types';

const fetchClassroomAssingmnets = async id => {
  try {
    const { data } = await axios.get(
      `/classroom/${id}/assignments`,
      BASIC_HEADER_OPTIONS(),
    );

    return data.sort((data1, data2) => {
      if (data1.submitted) return 1;
      if (data2.submitted) return -1;
      const t1 = new Date(data1.created).getTime();
      const t2 = new Date(data2.created).getTime();
      if (t1 > t2) return -1;
      if (t1 < t2) return 1;
      return 0;
    });
  } catch (e) {
    return Promise.reject(e);
  }
};

const fetchAssingment = async id => {
  try {
    return (await axios.get(`/assignment/${id}`, BASIC_HEADER_OPTIONS())).data;
  } catch (e) {
    return Promise.reject(e);
  }
};

const saveAssingment = async (assignment, file) => {
  try {
    const id = (
      await axios.post('/assignment/create', assignment, BASIC_HEADER_OPTIONS())
    ).data;
    await axios.post(
      `/assignment/${id}/file/upload`,
      { file: file },
      BASIC_HEADER_OPTIONS(),
    );
  } catch (e) {
    return Promise.reject(e);
  }
};

const updateAssignment = async (assignment, file) => {
  try {
    await axios.put('/assignment/update', assignment, BASIC_HEADER_OPTIONS());
    await axios.post(
      `/assignment/${assignment.id}/file/upload`,
      { file: file },
      BASIC_HEADER_OPTIONS(),
    );
  } catch (e) {
    return Promise.reject(e);
  }
};

const deleteAssingmentAtServer = async id => {
  try {
    await axios.delete(`/assignment/delete/${id}`, BASIC_HEADER_OPTIONS());
  } catch (e) {
    return Promise.reject(e);
  }
};

const getUserAssignmentsAtServer = async () => {
  try {
    const { data } = await axios.get(
      '/user/assignments',
      BASIC_HEADER_OPTIONS(),
    );
    return data.sort((data1, data2) => {
      if (data1.earned) return 1;
      if (data2.earned) return -1;
      if (data1.submitted) return 1;
      if (data2.submitted) return -1;
      const t1 = new Date(data1.created).getTime();
      const t2 = new Date(data2.created).getTime();
      if (t1 > t2) return -1;
      if (t1 < t2) return 1;
      return 0;
    });
  } catch (e) {
    return Promise.reject(e);
  }
};

export const getUserAssignments = () => dispatch => {
  dispatch({ type: GET_USER_ASSIGNMENTS });
  getUserAssignmentsAtServer()
    .then(res => dispatch(getUserAssignmentSuccess(res)))
    .catch(err => dispatch(getUserAssignmentsFailure(err)));
};

export const getUserAssignmentSuccess = assignments => ({
  type: GET_USER_ASSIGNMENTS_SUCCESS,
  payload: { assignments: assignments },
});

export const getUserAssignmentsFailure = err => ({
  type: GET_USER_ASSIGNMENTS_FAILURE,
  payload: { error: err },
});

export const getClassroomAssignments = classId => dispatch => {
  dispatch({ type: GET_ALL_CLSSROOM_ASSIGNEMENTS });
  fetchClassroomAssingmnets(classId)
    .then(res => dispatch(getClassroomAssignmentsSuccess(res)))
    .catch(err => dispatch(getClassroomAssignmentsFailure(err)));
};
export const getAssignment = id => dispatch => {
  dispatch({ type: GET_ASSIGNMENT });
  fetchAssingment(id)
    .then(res => dispatch(getAssignemntSuccess(res)))
    .catch(err => dispatch(getAssignemntFailure(err)));
};
export const createAssignment = (data, file) => dispatch => {
  dispatch({ type: CREATE_ASSIGNEMENT });
  saveAssingment(data, file)
    .then(res => {
      dispatch(getClassroomAssignments(data.classroomId));
      dispatch(createAssignmentSuccess(res));
    })
    .catch(err => dispatch(createAssignmentFailure(err)));
};
export const editAssignment = (data, file) => dispatch => {
  dispatch({ type: EDIT_ASSIGNEMENT });
  updateAssignment(data, file)
    .then(res => dispatch(editAssignementSuccess(res)))
    .catch(err => dispatch(editAssignmentFailure(err)));
};
export const deleteAssignment = id => dispatch => {
  dispatch({ type: DELETE_ASSIGNEMENT });
  deleteAssingmentAtServer(id)
    .then(res => dispatch(deleteAssignmentSuccess(res)))
    .catch(err => dispatch(deleteAssignemntFailure(err)));
};

export const getClassroomAssignmentsSuccess = assignments => ({
  type: GET_ALL_CLSSROOM_ASSIGNEMENTS_SUCESS,
  payload: { assignments: assignments },
});
export const getClassroomAssignmentsFailure = error => ({
  type: GET_ALL_CLSSROOM_ASSIGNEMENTS_FAILURE,
  payload: { error: error },
});

export const getAssignemntSuccess = assignemnt => ({
  type: GET_ASSIGNMENT_SUCCESS,
  payload: { assignment: assignemnt },
});

export const getAssignemntFailure = err => ({
  type: GET_ASSIGNMENT_FAILURE,
  payload: { error: err },
});

export const createAssignmentSuccess = () => ({
  type: CREATE_ASSIGNEMENT_SUCCESS,
});

export const createAssignmentFailure = err => ({
  type: CREATE_ASSIGNEMENT_FAILURE,
  payload: { error: err },
});

export const editAssignementSuccess = () => ({
  type: EDIT_ASSIGNEMENT_SUCCESS,
});

export const editAssignmentFailure = err => ({
  type: EDIT_ASSIGNEMENT_FAILURE,
  payload: { error: err },
});

export const deleteAssignmentSuccess = () => ({
  type: DELETE_ASSIGNEMENT_SUCCESS,
});

export const deleteAssignemntFailure = err => ({
  type: DELETE_ASSIGNEMENT_FAILURE,
  payload: { error: err },
});
