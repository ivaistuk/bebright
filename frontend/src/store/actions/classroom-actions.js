import axios from 'axios';
import { BASIC_HEADER_OPTIONS } from '../../utils/configure-axios';
import {
  ADD_ORGANIZER_TO_CLASSROOM,
  ADD_STUDENT_TO_CLASSROOM,
  CREATE_CLASSROOM,
  CREATE_CLASSROOM_FAILURE,
  CREATE_CLASSROOM_SUCCESS,
  DELETE_CLASSROOM,
  DELETE_CLASSROOM_FAILURE,
  DELETE_CLASSROOM_SUCCESS,
  EDIT_CLASSROOM,
  EDIT_CLASSROOM_FAILURE,
  EDIT_CLASSROOM_SUCCESS,
  GET_CLASSROOM,
  GET_CLASSROOM_FAILURE,
  GET_CLASSROOM_ORGANIZERS,
  GET_CLASSROOM_ORGANIZERS_FAILURE,
  GET_CLASSROOM_ORGANIZERS_SUCCESS,
  GET_CLASSROOM_STUDENTS,
  GET_CLASSROOM_STUDENTS_FAILURE,
  GET_CLASSROOM_STUDENTS_SUCCESS,
  GET_CLASSROOM_SUCCESS,
  GET_USER_CLASSROOMS,
  GET_USER_CLASSROOMS_FAILURE,
  GET_USER_CLASSROOMS_SUCCESS,
  REMOVE_ORGANIZER_FROM_CLASSROOM,
  REMOVE_STUDENT_FROM_CLASSROOM,
} from '../types/classroom-action-types';

const fetchUserClassrooms = async () => {
  try {
    return (await axios.get('/user/classrooms', BASIC_HEADER_OPTIONS())).data;
  } catch (e) {
    return Promise.reject(e);
  }
};

const fetchClassroom = async id => {
  try {
    return (await axios.get(`/classroom/${id}`, BASIC_HEADER_OPTIONS())).data;
  } catch (e) {
    return Promise.reject(e);
  }
};

const saveClassroom = async classroom => {
  try {
    await axios.post('/classroom/create', classroom, BASIC_HEADER_OPTIONS());
  } catch (e) {
    return Promise.reject(e);
  }
};

const updateClassroom = async classroom => {
  try {
    await axios.put('/classroom/update', classroom, BASIC_HEADER_OPTIONS());
  } catch (e) {
    return Promise.reject(e);
  }
};

const deleteClassroomAtServer = async id => {
  try {
    await axios.delete(`/classroom/delete/${id}`, BASIC_HEADER_OPTIONS());
  } catch (e) {
    return Promise.reject(e);
  }
};

export const createClassroom = classroomData => dispatch => {
  dispatch({ type: CREATE_CLASSROOM });
  saveClassroom(classroomData)
    .then(res => dispatch(createClassroomSuccess(res)))
    .catch(err => dispatch(createClassroomFailure(err)));
};

export const editClassroom = classroomData => dispatch => {
  dispatch({ type: EDIT_CLASSROOM });
  updateClassroom(classroomData)
    .then(res => {
      dispatch(editClassroomSuccess(res));
      dispatch(getClassroom(classroomData.id));
    })
    .catch(err => dispatch(editClasssroomFailure(err)));
};

export const deleteClassroom = id => dispatch => {
  dispatch({ type: DELETE_CLASSROOM });
  deleteClassroomAtServer(id)
    .then(res => {
      dispatch(getUserClassrooms());
      dispatch(deleteClassroomSuccess(res));
    })
    .catch(err => dispatch(deleteClassroomFailure(err)));
};

export const getClassroom = id => dispatch => {
  dispatch({ type: GET_CLASSROOM });
  fetchClassroom(id)
    .then(res => dispatch(getClassroomSuccess(res)))
    .catch(err => dispatch(getClassroomFailure(err)));
};

export const getUserClassrooms = () => dispatch => {
  dispatch({ type: GET_USER_CLASSROOMS });
  fetchUserClassrooms()
    .then(res => dispatch(getUserClassroomsSuccess(res)))
    .catch(err => dispatch(getUserClassroomsFailure(err)));
};

const fetchClassroomStudents = async id => {
  try {
    return (await axios.get(`classroom/${id}/students`, BASIC_HEADER_OPTIONS()))
      .data;
  } catch (e) {
    return Promise.reject(e);
  }
};

export const getClassroomStudents = classId => dispatch => {
  dispatch({ type: GET_CLASSROOM_STUDENTS });
  fetchClassroomStudents(classId)
    .then(res =>
      dispatch({
        type: GET_CLASSROOM_STUDENTS_SUCCESS,
        payload: { students: res },
      }),
    )
    .catch(err =>
      dispatch({
        type: GET_CLASSROOM_STUDENTS_FAILURE,
        payload: { error: err },
      }),
    );
};

const fetchClassroomOrganizers = async id => {
  try {
    return (
      await axios.get(`classroom/${id}/organizers`, BASIC_HEADER_OPTIONS())
    ).data;
  } catch (e) {
    return Promise.reject(e);
  }
};

export const getClassroomOrganizers = classId => dispatch => {
  dispatch({ type: GET_CLASSROOM_ORGANIZERS });
  fetchClassroomOrganizers(classId)
    .then(res =>
      dispatch({
        type: GET_CLASSROOM_ORGANIZERS_SUCCESS,
        payload: { organizers: res },
      }),
    )
    .catch(err =>
      dispatch({
        type: GET_CLASSROOM_ORGANIZERS_FAILURE,
        payload: { error: err },
      }),
    );
};

const addStudentToClassroomAtServer = async (studentId, classId) => {
  try {
    await axios.post(
      `classroom/${classId}/add/student`,
      JSON.stringify(studentId),
      BASIC_HEADER_OPTIONS(),
    );
  } catch (e) {
    return Promise.reject(e);
  }
};

export const addStudentToClassroom = (studentId, classId) => dispatch => {
  dispatch({ type: ADD_STUDENT_TO_CLASSROOM });
  //TODO add status managemnt
  addStudentToClassroomAtServer(studentId, classId)
    .then(res => dispatch(getClassroomStudents(classId)))
    .catch();
};

const removeStudentfromClassroomAtServer = async (studentId, classId) => {
  try {
    await axios.post(
      `classroom/${classId}/remove/student`,
      JSON.stringify(studentId),
      BASIC_HEADER_OPTIONS(),
    );
  } catch (e) {
    return Promise.reject(e);
  }
};

export const removeStudentFromClassroom = (studentId, classId) => dispatch => {
  dispatch({ type: REMOVE_STUDENT_FROM_CLASSROOM });
  //TODO add status managemnt
  removeStudentfromClassroomAtServer(studentId, classId)
    .then(res => dispatch(getClassroomStudents(classId)))
    .catch();
};

const addOrganizerToClassroomAtServer = async (organizerId, classId) => {
  try {
    await axios.post(
      `classroom/${classId}/add/organizer`,
      JSON.stringify(organizerId),
      BASIC_HEADER_OPTIONS(),
    );
  } catch (e) {
    return Promise.reject(e);
  }
};

export const addOrganizerToClassroom = (organizerId, classId) => dispatch => {
  dispatch({ type: ADD_ORGANIZER_TO_CLASSROOM });
  //TODO add status managemnt
  addOrganizerToClassroomAtServer(organizerId, classId)
    .then(res => dispatch(getClassroomOrganizers(classId)))
    .catch();
};

const removeOrganizerfromClassroomAtServer = async (organizerId, classId) => {
  try {
    await axios.post(
      `classroom/${classId}/remove/organizer`,
      JSON.stringify(organizerId),
      BASIC_HEADER_OPTIONS(),
    );
  } catch (e) {
    return Promise.reject(e);
  }
};

export const removeOrganizerFromClassroom =
  (organizerId, classId) => dispatch => {
    dispatch({ type: REMOVE_ORGANIZER_FROM_CLASSROOM });
    //TODO add status managemnt
    removeOrganizerfromClassroomAtServer(organizerId, classId)
      .then(res => dispatch(getClassroomOrganizers(classId)))
      .catch();
  };

export const getUserClassroomsSuccess = classrooms => ({
  type: GET_USER_CLASSROOMS_SUCCESS,
  payload: { classrooms: classrooms },
});
export const getUserClassroomsFailure = error => ({
  type: GET_USER_CLASSROOMS_FAILURE,
  payload: { error: error },
});

export const getClassroomSuccess = classroom => ({
  type: GET_CLASSROOM_SUCCESS,
  payload: { classroom: classroom },
});

export const getClassroomFailure = err => ({
  type: GET_CLASSROOM_FAILURE,
  payload: { error: err },
});

export const createClassroomSuccess = () => ({
  type: CREATE_CLASSROOM_SUCCESS,
});

export const createClassroomFailure = err => ({
  type: CREATE_CLASSROOM_FAILURE,
  payload: { error: err },
});

export const editClassroomSuccess = () => ({
  type: EDIT_CLASSROOM_SUCCESS,
});

export const editClasssroomFailure = err => ({
  type: EDIT_CLASSROOM_FAILURE,
  payload: { error: err },
});

export const deleteClassroomSuccess = () => ({
  type: DELETE_CLASSROOM_SUCCESS,
});

export const deleteClassroomFailure = err => ({
  type: DELETE_CLASSROOM_FAILURE,
  payload: { error: err },
});
