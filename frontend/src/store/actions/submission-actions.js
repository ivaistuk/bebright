import axios from 'axios';
import { BASIC_HEADER_OPTIONS } from '../../utils/configure-axios';
import {
  CREATE_SUBMISSION,
  CREATE_SUBMISSION_FAILURE,
  CREATE_SUBMISSION_SUCCESS,
  DELETE_SUBMISSION,
  DELETE_SUBMISSION_FAILURE,
  DELETE_SUBMISSION_SUCCESS,
  GET_ASSIGNMENT_SUBMISSIONS,
  GET_ASSIGNMENT_SUBMISSIONS_FAILURE,
  GET_ASSIGNMENT_SUBMISSIONS_SUCCESSS,
  GET_GRADED_SUBMISSIONS,
  GET_GRADED_SUBMISSIONS_FAILURE,
  GET_GRADED_SUBMISSIONS_SUCCESS,
  GET_STUDENT_SUBMISSIONS,
  GET_STUDENT_SUBMISSIONS_FAILURE,
  GET_STUDENT_SUBMISSIONS_SUCCESS,
  GET_SUBMISSION,
  GET_SUBMISSION_FAILURE,
  GET_SUBMISSION_SUCCESS,
  GET_UNGRADED_SUBMISSIONS,
  GET_UNGRADED_SUBMISSIONS_FAILURE,
  GET_UNGRADED_SUBMISSIONS_SUCCESS,
  GRADE_SUBMISSION,
  GRADE_SUBMISSION_FAILURE,
  GRADE_SUBMISSION_SUCCESS,
} from '../types/submission-action-types';

const saveSubmission = async (data, file) => {
  const id = (
    await axios.post('/submission/create', data, BASIC_HEADER_OPTIONS())
  ).data;
  await axios.post(
    `/submission/${id}/file/upload`,
    { file: file },
    BASIC_HEADER_OPTIONS(),
  );
};
export const createSubmision = (data, file) => dispatch => {
  dispatch({ type: CREATE_SUBMISSION });
  saveSubmission(data, file)
    .then(res =>
      dispatch({ type: CREATE_SUBMISSION_SUCCESS, payload: { response: res } }),
    )
    .catch(err =>
      dispatch({ type: CREATE_SUBMISSION_FAILURE, payload: { error: err } }),
    );
};

const deleteSubmissionAtServer = async id => {
  try {
    await axios.delete(`/submission/delete/${id}`, BASIC_HEADER_OPTIONS());
  } catch (e) {
    return Promise.reject(e);
  }
};

export const deleteSubmission = id => dispatch => {
  dispatch({ type: DELETE_SUBMISSION });
  deleteSubmissionAtServer(id)
    .then(res =>
      dispatch({ type: DELETE_SUBMISSION_SUCCESS, payload: { response: res } }),
    )
    .catch(err =>
      dispatch({ type: DELETE_SUBMISSION_FAILURE, payload: { error: err } }),
    );
};

const updateSubmission = async data => {
  try {
    await axios.put(`/submission/update`, data, BASIC_HEADER_OPTIONS());
  } catch (e) {
    return Promise.reject(e);
  }
};

export const gradeSubmission = data => dispatch => {
  dispatch({ type: GRADE_SUBMISSION });
  updateSubmission(data)
    .then(res =>
      dispatch({ type: GRADE_SUBMISSION_SUCCESS, payload: { response: res } }),
    )
    .catch(err =>
      dispatch({ type: GRADE_SUBMISSION_FAILURE, payload: { error: err } }),
    );
};

const fetchUserSubmissions = async id => {
  try {
    const data = (
      await axios.get(
        `/submission/assignment/${id}/user`,
        BASIC_HEADER_OPTIONS(),
      )
    ).data;
    return data.sort((data1, data2) => {
      const t1 = new Date(data1.created).getTime();
      const t2 = new Date(data2.created).getTime();
      if (t1 > t2) return -1;
      if (t1 < t2) return 1;
      return 0;
    });
  } catch (e) {
    return Promise.reject(e);
  }
};

export const getStudentSubmissions = id => dispatch => {
  dispatch({ type: GET_STUDENT_SUBMISSIONS });
  fetchUserSubmissions(id)
    .then(res =>
      dispatch({
        type: GET_STUDENT_SUBMISSIONS_SUCCESS,
        payload: { submissions: res },
      }),
    )
    .catch(err =>
      dispatch({
        type: GET_STUDENT_SUBMISSIONS_FAILURE,
        payload: { error: err },
      }),
    );
};

const fetchAssignmentSubmissions = async id => {
  try {
    return (
      await axios.get(`/submission/assignment/${id}`, BASIC_HEADER_OPTIONS())
    ).data;
  } catch (e) {
    return Promise.reject(e);
  }
};

export const getAssignmentSubmissions = id => dispatch => {
  dispatch({ type: GET_ASSIGNMENT_SUBMISSIONS });
  fetchAssignmentSubmissions(id)
    .then(res =>
      dispatch({
        type: GET_ASSIGNMENT_SUBMISSIONS_SUCCESSS,
        payload: { submissions: res },
      }),
    )
    .catch(err =>
      dispatch({
        type: GET_ASSIGNMENT_SUBMISSIONS_FAILURE,
        payload: { error: err },
      }),
    );
};

const fetchUngradedSubmissionsOfEducator = async () => {
  try {
    return (await axios.get(`/submission/ungraded`, BASIC_HEADER_OPTIONS()))
      .data;
  } catch (e) {
    return Promise.reject(e);
  }
};

export const getUngradedSubmissions = () => dispatch => {
  dispatch({ type: GET_UNGRADED_SUBMISSIONS });
  fetchUngradedSubmissionsOfEducator()
    .then(res =>
      dispatch({
        type: GET_UNGRADED_SUBMISSIONS_SUCCESS,
        payload: { submissions: res },
      }),
    )
    .catch(err =>
      dispatch({
        type: GET_UNGRADED_SUBMISSIONS_FAILURE,
        payload: { error: err },
      }),
    );
};

const fetchGradedSubmissionsOfEducator = async () => {
  try {
    return (await axios.get(`/submission/graded`, BASIC_HEADER_OPTIONS())).data;
  } catch (e) {
    return Promise.reject(e);
  }
};

export const getGradedSubmissions = () => dispatch => {
  dispatch({ type: GET_GRADED_SUBMISSIONS });
  fetchGradedSubmissionsOfEducator()
    .then(res =>
      dispatch({
        type: GET_GRADED_SUBMISSIONS_SUCCESS,
        payload: { submissions: res },
      }),
    )
    .catch(err =>
      dispatch({
        type: GET_GRADED_SUBMISSIONS_FAILURE,
        payload: { error: err },
      }),
    );
};

const fetchSubmission = async id => {
  try {
    return (await axios.get(`/submission/${id}`, BASIC_HEADER_OPTIONS())).data;
  } catch (e) {
    return Promise.reject(e);
  }
};

export const getSubmission = id => dispatch => {
  dispatch({ type: GET_SUBMISSION });
  fetchSubmission(id)
    .then(res =>
      dispatch({
        type: GET_SUBMISSION_SUCCESS,
        payload: { submission: res },
      }),
    )
    .catch(err =>
      dispatch({
        type: GET_SUBMISSION_FAILURE,
        payload: { error: err },
      }),
    );
};
