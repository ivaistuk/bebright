import axios from 'axios';
import { BASIC_HEADER_OPTIONS } from '../../utils/configure-axios';
import {
  ADD_BADGE_TO_USER,
  ADD_BADGE_TO_USER_FAILURE,
  ADD_BADGE_TO_USER_SUCCESS,
  GET_USER_BADGES,
  GET_USER_BADGES_FAILURE,
  GET_USER_BADGES_SUCCESS,
} from '../types/achievment-action-types';

const fetchUserBadges = async id => {
  try {
    return (await axios.get(`/user/${id}/badges`, BASIC_HEADER_OPTIONS())).data;
  } catch (e) {
    return Promise.reject(e);
  }
};

export const getUserBadges = id => dispatch => {
  dispatch({ type: GET_USER_BADGES });
  fetchUserBadges(id)
    .then(res => dispatch(getUserBadgesSuccess(res)))
    .catch(err => dispatch(getUserBadgesFailure(err)));
};

export const getUserBadgesSuccess = badges => ({
  type: GET_USER_BADGES_SUCCESS,
  payload: { badges: badges },
});

export const getUserBadgesFailure = err => ({
  type: GET_USER_BADGES_FAILURE,
  payload: { error: err },
});

const savebadgeToUser = async (badgeId, userId) => {
  try {
    await axios.post(
      `/user/${userId}/add/badge`,
      badgeId,
      BASIC_HEADER_OPTIONS(),
    );
  } catch (e) {
    return Promise.reject(e);
  }
};

export const addBadgeToUser = (badgeId, userId) => dispatch => {
  dispatch({ type: ADD_BADGE_TO_USER });
  savebadgeToUser(badgeId, userId)
    .then(res =>
      dispatch({ type: ADD_BADGE_TO_USER_SUCCESS, payload: { res: res } }),
    )
    .catch(err =>
      dispatch({ type: ADD_BADGE_TO_USER_FAILURE, payload: { error: err } }),
    );
};
