import axios from 'axios';
import { BASIC_HEADER_OPTIONS } from '../../utils/configure-axios';
import {
  DELETE_USER,
  DELETE_USER_FAILURE,
  DELETE_USER_SUCCESS,
  EDIT_USER,
  EDIT_USER_FAILURE,
  EDIT_USER_SUCCESS,
  FIND_ORGANIZER,
  FIND_ORGANIZER_SUCCESS,
  FIND_STUDENT,
  FIND_STUDENT_SUCCESS,
  GET_RANKED_STUDENTS,
  GET_RANKED_STUDENTS_FAILURE,
  GET_RANKED_STUDENTS_SUCCESS,
  GET_USER,
  GET_USER_FAILURE,
  GET_USER_SUCCESS,
  LOGIN,
  LOGIN_FAILURE,
  LOGIN_SUCCESS,
  LOGOUT,
  REGISTER,
  REGISTER_FAILURE,
  REGISTER_SUCCESS,
} from '../types/auth-action-types';
import { STUDENT } from '../../utils/role-names';

const searchStudentsAtServer = async keyword => {
  try {
    return (
      await axios.post('/user/search/student', keyword, BASIC_HEADER_OPTIONS())
    ).data;
  } catch (e) {
    return Promise.reject(e);
  }
};

export const searchStudent = keyword => dispatch => {
  dispatch({ type: FIND_STUDENT });
  searchStudentsAtServer(keyword)
    .then(res =>
      dispatch({ type: FIND_STUDENT_SUCCESS, payload: { students: res } }),
    )
    .catch();
};

const searchOrganizersAtServer = async keyword => {
  try {
    return (
      await axios.post(
        '/user/search/organizer',
        keyword,
        BASIC_HEADER_OPTIONS(),
      )
    ).data;
  } catch (e) {
    return Promise.reject(e);
  }
};

export const searchOrgnizer = keyword => dispatch => {
  dispatch({ type: FIND_ORGANIZER });
  searchOrganizersAtServer(keyword)
    .then(res =>
      dispatch({ type: FIND_ORGANIZER_SUCCESS, payload: { organizers: res } }),
    )
    .catch();
};

const fetchCurrentUser = async () => {
  try {
    const { data } = await axios.get('/user/me', BASIC_HEADER_OPTIONS());
    if (data.role.name === STUDENT) {
      data.experience = (
        await axios.get(`/user/${data.id}/experience`, BASIC_HEADER_OPTIONS())
      ).data;
      data.required = (
        await axios.get(`/achievement/${data.level}`, BASIC_HEADER_OPTIONS())
      ).data;
      data.previous = (
        await axios.get(
          `/achievement/${data.level - 1}`,
          BASIC_HEADER_OPTIONS(),
        )
      ).data;
    }
    return data;
  } catch (e) {
    localStorage.removeItem('bebright-current-user-token');
    return Promise.reject(e);
  }
};

export const getCurrentUser = () => dispatch => {
  fetchCurrentUser()
    .then(res => dispatch(loginSuccess(res)))
    .catch(err => dispatch(loginFailure(err)));
};

const fetchUser = async id => {
  try {
    const { data } = await axios.get(`/user/${id}`, BASIC_HEADER_OPTIONS());
    if (data.role.name === STUDENT) {
      data.experience = (
        await axios.get(`/user/${data.id}/experience`, BASIC_HEADER_OPTIONS())
      ).data;
      data.required = (
        await axios.get(`/achievement/${data.level}`, BASIC_HEADER_OPTIONS())
      ).data;
      data.previous = (
        await axios.get(
          `/achievement/${data.level - 1}`,
          BASIC_HEADER_OPTIONS(),
        )
      ).data;
    }
    return data;
  } catch (e) {
    return Promise.reject(e);
  }
};

const loginUserToServer = async user => {
  try {
    return (await axios.post('/login', user)).data;
  } catch (e) {
    return Promise.reject(e);
  }
};

const registerUserToServer = async user => {
  try {
    await axios.post('/user/create', user);
  } catch (e) {
    return Promise.reject(e);
  }
};

const editUserAtServer = async user => {
  try {
    await axios.put('/user/update', user, BASIC_HEADER_OPTIONS());
  } catch (e) {
    return Promise.reject(e);
  }
};

const deleteUserAtServer = async id => {
  try {
    await axios.delete(`/user/delete/${id}`, BASIC_HEADER_OPTIONS());
  } catch (e) {
    return Promise.reject(e);
  }
};

export const register = (registerData, loginData) => dispatch => {
  dispatch({ type: REGISTER });
  registerUserToServer(registerData)
    .then(res => {
      dispatch(registerSuccess(res));
      dispatch(login(loginData));
    })
    .catch(err => dispatch(registerFailure(err)));
};

export const editUser = userData => dispatch => {
  dispatch({ type: EDIT_USER });
  editUserAtServer(userData)
    .then(res => dispatch(editSuccess(res)))
    .catch(err => dispatch(editFailure(err)));
};

export const deleteUser = id => dispatch => {
  dispatch({ type: DELETE_USER });
  deleteUserAtServer(id)
    .then(res => dispatch(deleteSuccess(res)))
    .catch(err => dispatch(deleteFailure(err)));
};

export const login = userData => dispatch => {
  dispatch({ type: LOGIN });
  loginUserToServer(userData)
    .then(res => {
      localStorage.setItem('bebright-current-user-token', res.split(' ')[1]);
      fetchCurrentUser()
        .then(res => dispatch(loginSuccess(res)))
        .catch(err => dispatch(loginFailure(err)));
    })
    .catch(err => dispatch(loginFailure(err)));
};

export const autoLoginUser = () => dispatch => {
  dispatch({ type: LOGIN });
  fetchCurrentUser()
    .then(res => dispatch(loginSuccess(res)))
    .catch(err => dispatch(loginFailure(err)));
};

export const getUser = id => dispatch => {
  dispatch({ type: GET_USER });
  fetchUser(id)
    .then(res => dispatch(getUserSuccess(res)))
    .catch(err => dispatch(getUserFailure(err)));
};

export const loginSuccess = user => {
  return {
    type: LOGIN_SUCCESS,
    payload: { user: user },
  };
};

export const loginFailure = error => {
  return {
    type: LOGIN_FAILURE,
    payload: { error: error },
  };
};

export const logout = () => {
  return {
    type: LOGOUT,
  };
};

export const registerSuccess = () => ({ type: REGISTER_SUCCESS });

export const registerFailure = err => ({
  type: REGISTER_FAILURE,
  payload: { error: err },
});

export const editSuccess = () => ({ type: EDIT_USER_SUCCESS });
export const editFailure = err => ({
  type: EDIT_USER_FAILURE,
  payload: { error: err },
});

export const deleteSuccess = () => ({ type: DELETE_USER_SUCCESS });
export const deleteFailure = err => ({
  type: DELETE_USER_FAILURE,
  payload: { error: err },
});
export const getUserSuccess = user => ({
  type: GET_USER_SUCCESS,
  payload: { user: user },
});

export const getUserFailure = err => ({
  type: GET_USER_FAILURE,
  payload: { error: err },
});

const fetchRankedStudents = async () => {
  try {
    return (await axios.get('/user/students/ranked', BASIC_HEADER_OPTIONS()))
      .data;
  } catch (e) {
    return Promise.reject(e);
  }
};

export const getRankedStudents = () => dispatch => {
  dispatch({ type: GET_RANKED_STUDENTS });
  fetchRankedStudents()
    .then(res =>
      dispatch({
        type: GET_RANKED_STUDENTS_SUCCESS,
        payload: { students: res },
      }),
    )
    .catch(err =>
      dispatch({ type: GET_RANKED_STUDENTS_FAILURE, payload: { error: err } }),
    );
};
