export const selectCurrentAuthStatus = state => state.auth.currentUser;
export const selectCurrentUser = state => state.auth.currentUser.data;
export const selectRegisteredStatus = state => state.auth.registered;
export const selectCurrentUserEditStatus = state => state.auth.edited;
export const selectCurtrentUserDeletedStatus = state => state.auth.deleted;
export const selectFoundStudents = state => state.auth.foundStudents;
export const selectFoundOrganizers = state => state.auth.foundOrganizers;
export const selectUser = state => state.auth.user;
export const selectUserData = state => state.auth.user.data;
export const selectRankedStudents = state => state.auth.ranked;

export const selectBadges = state => state.achievement.badges;
export const selectBadgeAddStatus = state => state.achievement.add;

export const selectClassroom = state => state.class.currentClassroom;
export const selectCurrentUserClassrooms = state => state.class.classrooms;
export const selectClassroomCreatedStatus = state => state.class.created;
export const selectClassroomEditedStatus = state => state.class.edited;
export const selectClassroomDeletedStatus = state => state.class.deleted;
export const selectClassroomStudents = state =>
  state.class.currentClassroomStudents;
export const selectClassroomOrganizers = state =>
  state.class.currentClassroomOrganizers;

export const selectClassroomAssignments = state =>
  state.assign.classAssignments;
export const selectAssignment = state => state.assign.currentAssignment;
export const selectAssignmentCreatedStatus = state => state.assign.created;
export const selectAssignmentEditedStatus = state => state.assign.edited;
export const selectAssignmentDeletedStatus = state => state.assign.deleted;
export const selectUserAssignments = state => state.assign.assignments;

export const selectSubmissionCreatedStatus = state => state.submission.created;
export const selectSubmissionDeleteStatus = state => state.submission.deleted;
export const selectCurrentSubmission = state =>
  state.submission.currentSubmission;
export const selectAssignmentSubmissions = state =>
  state.submission.assignmentSubmissions;
export const selectStudentSubmissions = state =>
  state.submission.studentSubmissions;
export const selectUngradedSubmissions = state =>
  state.submission.ungradedSubmissions;
export const selectGradedSubmissions = state =>
  state.submission.gradedSubmissions;
export const selectGradeSubmissionStatus = state =>
  state.submission.gradeStatus;
