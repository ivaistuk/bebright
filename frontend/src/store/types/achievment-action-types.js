export const GET_USER_BADGES = 'GET_USER_BADGES';
export const GET_USER_BADGES_SUCCESS = 'GET_BADGES_SUCCESS';
export const GET_USER_BADGES_FAILURE = 'GET_BADGES_FAILURE';

export const ADD_BADGE_TO_USER = 'ADD_BADGE_TO_USER';
export const ADD_BADGE_TO_USER_SUCCESS = 'ADD_BADGE_TO_USER_SUCCESS';
export const ADD_BADGE_TO_USER_FAILURE = 'ADD_BADGE_TO_USER_FAILURE';
export const RESET_ADD_BADGE_STATUS = 'RESET_ADD_BADGE_STATUS';
