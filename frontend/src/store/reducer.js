import { combineReducers } from 'redux';
import { authReducer } from './reducers/auth-reducer';
import { classroomReducer } from './reducers/classroom-reducer';
import { achievementReducer } from './reducers/achievement-reducer';
import { assignmentReducer } from './reducers/assignment-reducer';
import { submissionReducer } from './reducers/submission-reducer';

export const reducer = combineReducers({
  auth: authReducer,
  class: classroomReducer,
  achievement: achievementReducer,
  assign: assignmentReducer,
  submission: submissionReducer,
});
