import {
  CREATE_CLASSROOM,
  CREATE_CLASSROOM_FAILURE,
  CREATE_CLASSROOM_SUCCESS,
  DELETE_CLASSROOM,
  DELETE_CLASSROOM_FAILURE,
  DELETE_CLASSROOM_SUCCESS,
  EDIT_CLASSROOM,
  EDIT_CLASSROOM_FAILURE,
  EDIT_CLASSROOM_SUCCESS,
  GET_CLASSROOM,
  GET_CLASSROOM_FAILURE,
  GET_CLASSROOM_ORGANIZERS,
  GET_CLASSROOM_ORGANIZERS_FAILURE,
  GET_CLASSROOM_ORGANIZERS_SUCCESS,
  GET_CLASSROOM_STUDENTS,
  GET_CLASSROOM_STUDENTS_FAILURE,
  GET_CLASSROOM_STUDENTS_SUCCESS,
  GET_CLASSROOM_SUCCESS,
  GET_USER_CLASSROOMS,
  GET_USER_CLASSROOMS_FAILURE,
  GET_USER_CLASSROOMS_SUCCESS,
  RESET_CLASSROOM_CREATED_STATUS,
  RESET_CLASSROOM_DELETE_STATUS,
  RESET_CLASSROOM_EDIT_STATUS,
} from '../types/classroom-action-types';

const initialState = {
  classrooms: {
    loading: false,
    data: [],
    error: false,
  },
  currentClassroom: {
    loading: false,
    data: undefined,
    error: false,
  },
  created: {
    loading: false,
    status: false,
    error: false,
  },
  edited: {
    loading: false,
    status: false,
    error: false,
  },
  deleted: {
    loading: false,
    status: false,
    error: false,
  },
  currentClassroomStudents: {
    data: [],
    loading: false,
    error: false,
  },
  currentClassroomOrganizers: {
    data: [],
    loading: false,
    error: false,
  },
};

export const classroomReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_USER_CLASSROOMS:
      return { ...state, classrooms: { ...state.classrooms, loading: true } };
    case GET_USER_CLASSROOMS_SUCCESS:
      return {
        ...state,
        classrooms: {
          ...state.classrooms,
          loading: false,
          data: action.payload.classrooms,
        },
      };
    case GET_USER_CLASSROOMS_FAILURE:
      return {
        ...state,
        classrooms: {
          ...state.classrooms,
          loading: false,
          data: [],
          error: action.payload.error,
        },
      };
    case CREATE_CLASSROOM:
      return {
        ...state,
        created: {
          loading: true,
        },
      };
    case CREATE_CLASSROOM_SUCCESS:
      return {
        ...state,
        created: { loading: false, status: true, error: false },
      };
    case CREATE_CLASSROOM_FAILURE:
      return {
        ...state,
        created: { loading: false, status: false, error: action.payload.error },
      };
    case RESET_CLASSROOM_CREATED_STATUS:
      return {
        ...state,
        created: { loading: false, status: false, error: false },
      };
    case EDIT_CLASSROOM:
      return { ...state, edited: { loading: true } };
    case EDIT_CLASSROOM_SUCCESS:
      return {
        ...state,
        edited: { loading: false, status: true, error: false },
      };
    case EDIT_CLASSROOM_FAILURE:
      return {
        ...state,
        edited: { loading: false, status: false, error: action.payload.error },
      };
    case RESET_CLASSROOM_EDIT_STATUS:
      return {
        ...state,
        edited: { loading: false, status: false, error: false },
      };
    case DELETE_CLASSROOM:
      return { ...state, deleted: { loading: true } };
    case DELETE_CLASSROOM_SUCCESS:
      return {
        ...state,
        deleted: { loading: false, status: true, error: false },
      };
    case DELETE_CLASSROOM_FAILURE:
      return {
        ...state,
        deleted: { loading: false, status: false, error: action.payload.error },
      };
    case RESET_CLASSROOM_DELETE_STATUS:
      return {
        ...state,
        deleted: { loading: false, status: false, error: false },
      };
    case GET_CLASSROOM:
      return {
        ...state,
        currentClassroom: { ...state.currentClassroom, loading: true },
      };
    case GET_CLASSROOM_SUCCESS:
      return {
        ...state,
        currentClassroom: {
          ...state.currentClassroom,
          loading: false,
          data: action.payload.classroom,
        },
      };
    case GET_CLASSROOM_FAILURE:
      return {
        ...state,
        currentClassroom: {
          ...state.currentClassroom,
          loading: false,
          data: undefined,
          error: action.payload.error,
        },
      };
    case GET_CLASSROOM_STUDENTS:
      return {
        ...state,
        currentClassroomStudents: {
          loading: true,
          data: [],
          error: false,
        },
      };
    case GET_CLASSROOM_STUDENTS_SUCCESS:
      return {
        ...state,
        currentClassroomStudents: {
          loading: false,
          data: action.payload.students,
          error: false,
        },
      };
    case GET_CLASSROOM_STUDENTS_FAILURE:
      return {
        ...state,
        currentClassroomStudents: {
          loading: false,
          data: [],
          error: action.payload.error,
        },
      };
    case GET_CLASSROOM_ORGANIZERS:
      return {
        ...state,
        currentClassroomOrganizers: {
          loading: true,
          data: [],
          error: false,
        },
      };
    case GET_CLASSROOM_ORGANIZERS_SUCCESS:
      return {
        ...state,
        currentClassroomOrganizers: {
          loading: false,
          data: action.payload.organizers,
          error: false,
        },
      };
    case GET_CLASSROOM_ORGANIZERS_FAILURE:
      return {
        ...state,
        currentClassroomOrganizers: {
          loading: false,
          data: [],
          error: action.payload.error,
        },
      };

    default:
      return { ...state };
  }
};
