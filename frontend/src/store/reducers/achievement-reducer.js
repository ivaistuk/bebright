import {
  ADD_BADGE_TO_USER,
  ADD_BADGE_TO_USER_FAILURE,
  ADD_BADGE_TO_USER_SUCCESS,
  GET_USER_BADGES,
  GET_USER_BADGES_FAILURE,
  GET_USER_BADGES_SUCCESS,
  RESET_ADD_BADGE_STATUS,
} from '../types/achievment-action-types';

const initialState = {
  badges: {
    loading: false,
    data: [],
    error: false,
  },
  add: {
    loading: false,
    status: false,
    error: false,
  },
};

export const achievementReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_USER_BADGES:
      return {
        ...state,
        badges: { ...state.badges, loading: true },
      };
    case GET_USER_BADGES_SUCCESS:
      return {
        ...state,
        badges: {
          ...state.badges,
          loading: false,
          data: action.payload.badges,
        },
      };
    case GET_USER_BADGES_FAILURE:
      return {
        ...state,
        badges: {
          loading: false,
          data: [],
          error: action.payload.error,
        },
      };
    case ADD_BADGE_TO_USER:
      return { ...state, add: { loading: true, status: false, error: false } };
    case ADD_BADGE_TO_USER_SUCCESS:
      return { ...state, add: { loading: true, status: true, error: false } };
    case ADD_BADGE_TO_USER_FAILURE:
      return {
        ...state,
        add: { loading: false, status: false, error: action.payload.error },
      };
    case RESET_ADD_BADGE_STATUS:
      return { ...state, add: { loading: false, status: false, error: false } };
    default:
      return { ...state };
  }
};
