import {
  CREATE_ASSIGNEMENT,
  CREATE_ASSIGNEMENT_FAILURE,
  CREATE_ASSIGNEMENT_SUCCESS,
  DELETE_ASSIGNEMENT,
  DELETE_ASSIGNEMENT_FAILURE,
  DELETE_ASSIGNEMENT_SUCCESS,
  EDIT_ASSIGNEMENT,
  EDIT_ASSIGNEMENT_FAILURE,
  EDIT_ASSIGNEMENT_SUCCESS,
  GET_ALL_CLSSROOM_ASSIGNEMENTS,
  GET_ALL_CLSSROOM_ASSIGNEMENTS_FAILURE,
  GET_ALL_CLSSROOM_ASSIGNEMENTS_SUCESS,
  GET_ASSIGNMENT,
  GET_ASSIGNMENT_FAILURE,
  GET_ASSIGNMENT_SUCCESS,
  GET_USER_ASSIGNMENTS,
  GET_USER_ASSIGNMENTS_FAILURE,
  GET_USER_ASSIGNMENTS_SUCCESS,
  RESET_ASSIGNEMENT_CREATED_STATUS,
  RESET_ASSIGNEMENT_DELETE_STATUS,
  RESET_ASSIGNEMENT_EDIT_STATUS,
} from '../types/assignment-action-types';

const initialState = {
  classAssignments: {
    data: [],
    loading: false,
    error: false,
  },
  currentAssignment: { data: undefined, loading: false, error: false },
  created: {
    status: false,
    loading: false,
    error: false,
  },
  edited: { status: false, loading: false, error: false },
  deleted: { status: false, loading: false, error: false },
  assignments: { loading: false, data: [], error: false },
};

export const assignmentReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_ALL_CLSSROOM_ASSIGNEMENTS:
      return {
        ...state,
        classAssignments: { ...state.classAssignments, loading: true },
      };
    case GET_ALL_CLSSROOM_ASSIGNEMENTS_SUCESS:
      return {
        ...state,
        classAssignments: {
          ...state.classAssignments,
          loading: false,
          data: action.payload.assignments,
        },
      };
    case GET_ALL_CLSSROOM_ASSIGNEMENTS_FAILURE:
      return {
        ...state,
        classAssignments: {
          ...state.classAssignments,
          loading: false,
          data: [],
          error: action.payload.error,
        },
      };
    case GET_ASSIGNMENT:
      return {
        ...state,
        currentAssignment: { ...state.currentAssignment, loading: true },
      };
    case GET_ASSIGNMENT_SUCCESS:
      return {
        ...state,
        currentAssignment: {
          ...state.currentAssignment,
          loading: false,
          data: action.payload.assignment,
        },
      };
    case GET_ASSIGNMENT_FAILURE:
      return {
        ...state,
        currentAssignment: {
          ...state.currentAssignment,
          loading: false,
          data: undefined,
          error: action.payload.error,
        },
      };
    case CREATE_ASSIGNEMENT:
      return { ...state, created: { loading: true } };
    case CREATE_ASSIGNEMENT_SUCCESS:
      return {
        ...state,
        created: { loading: false, status: true, error: false },
      };
    case CREATE_ASSIGNEMENT_FAILURE:
      return {
        ...state,
        created: { loading: false, error: action.payload.error, status: false },
      };
    case RESET_ASSIGNEMENT_CREATED_STATUS:
      return {
        ...state,
        created: { loading: false, error: false, status: false },
      };
    case EDIT_ASSIGNEMENT:
      return { ...state, edited: { loading: true } };
    case EDIT_ASSIGNEMENT_SUCCESS:
      return {
        ...state,
        edited: { loading: false, status: true, error: false },
      };
    case EDIT_ASSIGNEMENT_FAILURE:
      return {
        ...state,
        edited: { loading: false, error: action.payload.error, status: false },
      };
    case RESET_ASSIGNEMENT_EDIT_STATUS:
      return {
        ...state,
        edited: { loading: false, error: false, status: false },
      };
    case DELETE_ASSIGNEMENT:
      return { ...state, deleted: { loading: true } };
    case DELETE_ASSIGNEMENT_SUCCESS:
      return {
        ...state,
        deleted: { loading: false, status: true, error: false },
      };
    case DELETE_ASSIGNEMENT_FAILURE:
      return {
        ...state,
        deleted: { loading: false, error: action.payload.error, status: false },
      };
    case RESET_ASSIGNEMENT_DELETE_STATUS:
      return {
        ...state,
        deleted: { loading: false, error: false, status: false },
      };
    case GET_USER_ASSIGNMENTS:
      return {
        ...state,
        assignments: { loading: true, error: false, data: [] },
      };
    case GET_USER_ASSIGNMENTS_SUCCESS:
      return {
        ...state,
        assignments: {
          loading: false,
          error: false,
          data: action.payload.assignments,
        },
      };
    case GET_USER_ASSIGNMENTS_FAILURE:
      return {
        ...state,
        assignments: { loading: false, error: action.payload.error, data: [] },
      };
    default:
      return { ...state };
  }
};
