import {
  CREATE_SUBMISSION,
  CREATE_SUBMISSION_FAILURE,
  CREATE_SUBMISSION_SUCCESS,
  DELETE_SUBMISSION,
  DELETE_SUBMISSION_FAILURE,
  DELETE_SUBMISSION_SUCCESS,
  GET_ASSIGNMENT_SUBMISSIONS,
  GET_ASSIGNMENT_SUBMISSIONS_FAILURE,
  GET_ASSIGNMENT_SUBMISSIONS_SUCCESSS,
  GET_GRADED_SUBMISSIONS,
  GET_GRADED_SUBMISSIONS_FAILURE,
  GET_GRADED_SUBMISSIONS_SUCCESS,
  GET_STUDENT_SUBMISSIONS,
  GET_STUDENT_SUBMISSIONS_FAILURE,
  GET_STUDENT_SUBMISSIONS_SUCCESS,
  GET_SUBMISSION,
  GET_SUBMISSION_FAILURE,
  GET_SUBMISSION_SUCCESS,
  GET_UNGRADED_SUBMISSIONS,
  GET_UNGRADED_SUBMISSIONS_FAILURE,
  GET_UNGRADED_SUBMISSIONS_SUCCESS,
  GRADE_SUBMISSION,
  GRADE_SUBMISSION_FAILURE,
  GRADE_SUBMISSION_SUCCESS,
  RESET_SUBMISSION_CREATED_STATUS,
  RESET_SUBMISSION_DELETED_STATUS,
  RESET_SUBMISSION_GRADE_STATUS,
} from '../types/submission-action-types';

const initialState = {
  assignmentSubmissions: {
    loading: false,
    data: [],
    error: false,
  },
  studentSubmissions: {
    loading: false,
    data: [],
    error: false,
  },
  created: {
    status: false,
    loading: false,
    error: false,
  },
  deleted: {
    status: false,
    loading: false,
    error: false,
  },
  currentSubmission: {
    data: undefined,
    error: false,
    loading: false,
  },
  ungradedSubmissions: {
    loading: false,
    data: [],
    error: false,
  },
  gradedSubmissions: {
    loading: false,
    data: [],
    error: false,
  },
  gradeStatus: {
    loading: false,
    status: false,
    error: false,
  },
};

export const submissionReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_SUBMISSION:
      return {
        ...state,
        currentSubmission: { data: undefined, error: false, loading: true },
      };
    case GET_SUBMISSION_SUCCESS:
      return {
        ...state,
        currentSubmission: {
          data: action.payload.submission,
          error: false,
          loading: false,
        },
      };
    case GET_SUBMISSION_FAILURE:
      return {
        ...state,
        currentSubmission: {
          error: action.payload.error,
          data: undefined,
          loading: false,
        },
      };
    case GET_STUDENT_SUBMISSIONS:
      return {
        ...state,
        studentSubmissions: { data: [], error: false, loading: true },
      };
    case GET_STUDENT_SUBMISSIONS_SUCCESS:
      return {
        ...state,
        studentSubmissions: {
          data: action.payload.submissions,
          loading: false,
          error: false,
        },
      };
    case GET_STUDENT_SUBMISSIONS_FAILURE:
      return {
        ...state,
        studentSubmissions: {
          error: action.payload.error,
          loading: false,
          data: [],
        },
      };
    case GET_ASSIGNMENT_SUBMISSIONS:
      return {
        ...state,
        assignmentSubmissions: { loading: true, data: [], error: false },
      };
    case GET_ASSIGNMENT_SUBMISSIONS_SUCCESSS:
      return {
        ...state,
        assignmentSubmissions: {
          loading: false,
          data: action.payload.submissions,
          error: false,
        },
      };
    case GET_ASSIGNMENT_SUBMISSIONS_FAILURE:
      return {
        ...state,
        assignmentSubmissions: {
          loading: false,
          data: [],
          error: action.payload.error,
        },
      };
    case GET_GRADED_SUBMISSIONS:
      return {
        ...state,
        gradedSubmissions: { data: [], error: false, loading: true },
      };
    case GET_GRADED_SUBMISSIONS_SUCCESS:
      return {
        ...state,
        gradedSubmissions: {
          data: action.payload.submissions,
          loading: false,
          error: false,
        },
      };
    case GET_GRADED_SUBMISSIONS_FAILURE:
      return {
        ...state,
        gradedSubmissions: {
          data: [],
          error: action.payload.error,
          loading: false,
        },
      };
    case GET_UNGRADED_SUBMISSIONS:
      return {
        ...state,
        ungradedSubmissions: { data: [], error: false, loading: true },
      };
    case GET_UNGRADED_SUBMISSIONS_SUCCESS:
      return {
        ...state,
        ungradedSubmissions: {
          data: action.payload.submissions,
          loading: false,
          error: false,
        },
      };
    case GET_UNGRADED_SUBMISSIONS_FAILURE:
      return {
        ...state,
        ungradedSubmissions: {
          data: [],
          error: action.payload.error,
          loading: false,
        },
      };
    case CREATE_SUBMISSION:
      return {
        ...state,
        created: { status: false, loading: true, error: false },
      };
    case CREATE_SUBMISSION_SUCCESS:
      return {
        ...state,
        created: { status: true, loading: false, error: false },
      };
    case CREATE_SUBMISSION_FAILURE:
      return {
        ...state,
        created: { status: false, loading: false, error: action.payload.error },
      };
    case RESET_SUBMISSION_CREATED_STATUS:
      return {
        ...state,
        created: { status: false, loading: false, error: false },
      };
    case DELETE_SUBMISSION:
      return {
        ...state,
        deleted: { status: false, loading: true, error: false },
      };
    case DELETE_SUBMISSION_SUCCESS:
      return {
        ...state,
        deleted: { status: true, loading: false, error: false },
      };
    case DELETE_SUBMISSION_FAILURE:
      return {
        ...state,
        deleted: { status: false, loading: false, error: action.payload.error },
      };
    case RESET_SUBMISSION_DELETED_STATUS:
      return {
        ...state,
        deleted: { status: false, loading: false, error: false },
      };
    case GRADE_SUBMISSION:
      return {
        ...state,
        gradeStatus: { status: false, loading: true, error: false },
      };
    case GRADE_SUBMISSION_SUCCESS:
      return {
        ...state,
        gradeStatus: { status: true, loading: false, error: false },
      };
    case GRADE_SUBMISSION_FAILURE:
      return {
        ...state,
        gradeStatus: {
          status: false,
          loading: false,
          error: action.payload.error,
        },
      };
    case RESET_SUBMISSION_GRADE_STATUS:
      return {
        ...state,
        gradeStatus: { status: false, loading: false, error: false },
      };
    default:
      return { ...state };
  }
};
