import {
  DELETE_USER,
  DELETE_USER_FAILURE,
  DELETE_USER_SUCCESS,
  EDIT_USER,
  EDIT_USER_FAILURE,
  EDIT_USER_SUCCESS,
  GET_USER,
  GET_USER_FAILURE,
  GET_USER_SUCCESS,
  LOGIN,
  LOGIN_FAILURE,
  LOGIN_RESET,
  LOGIN_SUCCESS,
  LOGOUT,
  REGISTER,
  REGISTER_FAILURE,
  REGISTER_SUCCESS,
  RESET_REGISTER_STATUS,
  RESET_USER_DELETE_STATUS,
  RESET_USER_EDIT_STATUS,
  FIND_ORGANIZER,
  FIND_ORGANIZER_SUCCESS,
  FIND_STUDENT,
  FIND_STUDENT_SUCCESS,
  GET_RANKED_STUDENTS_SUCCESS,
  GET_RANKED_STUDENTS,
  GET_RANKED_STUDENTS_FAILURE,
} from '../types/auth-action-types';

const defaultState = {
  currentUser: {
    loading: false,
    authenticated: false,
    data: undefined,
    error: false,
  },
  user: {
    loading: false,
    data: undefined,
    error: false,
  },
  registered: {
    loading: false,
    status: false,
    error: false,
  },
  edited: {
    loading: false,
    status: false,
    error: false,
  },
  deleted: {
    loading: false,
    status: false,
    error: false,
  },
  foundStudents: {
    data: [],
    loading: false,
  },
  foundOrganizers: {
    data: [],
    loading: false,
  },
  ranked: {
    data: [],
    loading: false,
    error: false,
  },
};

export const authReducer = (state = defaultState, action) => {
  switch (action.type) {
    case LOGIN:
      return { ...state, currentUser: { ...state.currentUser, loading: true } };
    case LOGIN_SUCCESS:
      return {
        ...state,
        currentUser: {
          ...state.currentUser,
          loading: false,
          authenticated: true,
          data: action.payload.user,
        },
      };
    case LOGIN_FAILURE:
      return {
        ...state,
        currentUser: {
          ...state.currentUser,
          loading: false,
          authenticated: false,
          data: undefined,
          error: action.payload.error,
        },
      };
    case LOGIN_RESET:
      return {
        ...state,
        currentUser: {
          loading: false,
          authenticated: false,
          data: undefined,
          error: false,
        },
      };
    case LOGOUT:
      return {
        ...state,
        currentUser: {
          error: false,
          loading: false,
          authenticated: false,
          data: undefined,
        },
      };
    case REGISTER:
      return {
        ...state,
        registered: { loading: true, status: undefined, error: false },
      };
    case REGISTER_SUCCESS:
      return {
        ...state,
        registered: { loading: false, status: true, error: false },
      };
    case REGISTER_FAILURE:
      return {
        ...state,
        registered: {
          loading: false,
          status: false,
          error: action.payload.error,
        },
      };
    case RESET_REGISTER_STATUS:
      return {
        ...state,
        registered: { loading: false, status: false, error: false },
      };
    case EDIT_USER:
      return { ...state, edited: { loading: true } };
    case EDIT_USER_SUCCESS:
      return {
        ...state,
        edited: { loading: false, status: true, error: false },
      };
    case EDIT_USER_FAILURE:
      return {
        ...state,
        edited: { loading: false, status: false, error: action.payload.error },
      };
    case RESET_USER_EDIT_STATUS:
      return {
        ...state,
        edited: { loading: false, status: false, error: false },
      };
    case DELETE_USER:
      return { ...state, deleted: { loading: true } };
    case DELETE_USER_SUCCESS:
      return {
        ...state,
        deleted: { loading: false, status: true, error: false },
      };
    case DELETE_USER_FAILURE:
      return {
        ...state,
        deleted: { loading: false, status: false, error: action.payload.error },
      };
    case RESET_USER_DELETE_STATUS:
      return {
        ...state,
        deleted: { loading: false, status: false, error: false },
      };
    case GET_USER:
      return {
        ...state,
        user: { loading: true, data: undefined, error: false },
      };
    case GET_USER_SUCCESS:
      return {
        ...state,
        user: { loading: false, data: action.payload.user, error: false },
      };
    case GET_USER_FAILURE:
      return {
        ...state,
        user: {
          loading: false,
          data: undefined,
          error: action.payload.error,
        },
      };
    case FIND_STUDENT:
      return {
        ...state,
        foundStudents: {
          loading: true,
          data: [],
        },
      };
    case FIND_STUDENT_SUCCESS:
      return {
        ...state,
        foundStudents: {
          loading: false,
          data: action.payload.students,
        },
      };
    case FIND_ORGANIZER:
      return {
        ...state,
        foundOrganizers: {
          loading: true,
          data: [],
        },
      };
    case FIND_ORGANIZER_SUCCESS:
      return {
        ...state,
        foundOrganizers: {
          loading: false,
          data: action.payload.organizers,
        },
      };
    case GET_RANKED_STUDENTS:
      return {
        ...state,
        ranked: {
          loading: true,
          data: [],
          error: false,
        },
      };
    case GET_RANKED_STUDENTS_SUCCESS:
      return {
        ...state,
        ranked: {
          loading: false,
          data: action.payload.students,
          error: false,
        },
      };
    case GET_RANKED_STUDENTS_FAILURE:
      return {
        ...state,
        ranked: {
          loading: false,
          data: [],
          error: action.payload.error,
        },
      };
    default:
      return state;
  }
};
