import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';

import { store } from '../store/store';
import Routes from './Routes';
import AuthMiddleware from './AuthMiddleware';

import { ThemeProvider, CssBaseline, createTheme } from '@material-ui/core';

const theme = createTheme({
  palette: {
    primary: {
      main: '#1f8e6d',
      dark: '#104c3a',
      light: '#38fdc1',
    },
    secondary: {
      main: '#093c2e',
      dark: '#05261c',
      light: '#ffffff',
    },
    error: {
      main: 'rgb(231,52,52)',
    },
    background: {
      default: 'rgba(224,226,222,0.68)',
    },
    text: {
      primary: '#154032',
      secondary: '#ffffff',
    },
  },
});

function App() {
  return (
    <Provider store={store}>
      <AuthMiddleware>
        <BrowserRouter>
          <ThemeProvider theme={theme}>
            <CssBaseline />
            <Routes />
          </ThemeProvider>
        </BrowserRouter>
      </AuthMiddleware>
    </Provider>
  );
}

export default App;
