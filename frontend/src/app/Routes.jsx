import { Redirect, Route, Switch, useLocation } from 'react-router';
import { useSelector } from 'react-redux';

import Login from '../components/auth/Login';
import Register from '../components/auth/Register';
import Home from '../components/home/Home';
import Dashboard from '../components/home/Dashboard';
import Loading from '../components/loading/Loading';
import Profile from '../components/profile/pages/Profile';
import Settings from '../components/settings/pages/Settings';
import Classroom from '../components/classroom/pages/Classroom';
import { selectCurrentAuthStatus } from '../store/selectors';
import Achievments from '../components/achievments/pages/Achievments';
import Tasks from '../components/assignements/pages/Tasks';
import Calendar from '../components/calendar/Calendar';
import Grading from '../components/submissions/Grading';
import Submission from '../components/submissions/Submission';
import Assignment from '../components/assignements/pages/Assignement';

function Routes() {
  const { loading, authenticated } = useSelector(selectCurrentAuthStatus);
  const { pathname } = useLocation();

  if (loading && pathname !== '/login') return <Loading />;
  return (
    <Switch>
      {!authenticated && pathname !== '/login' && pathname !== '/register' && (
        <Redirect to="/login" />
      )}
      {authenticated && (pathname === '/login' || pathname === '/register') && (
        <Redirect to="/home" />
      )}

      <Route path="/" exact>
        <Redirect to="/home" />
      </Route>

      <Route path="/login" component={Login} exact />
      <Route path="/register" component={Register} exact />
      <Route path="/home" exact>
        <Home component={<Dashboard />} title="Radna ploča" />
      </Route>
      <Route path="/profile" exact>
        <Home component={<Profile />} title="Profil" />
      </Route>
      <Route path="/profile/:id" exact>
        <Home component={<Profile />} title="Profil" />
      </Route>
      <Route path="/settings" exact>
        <Home component={<Settings />} title="Postavke" />
      </Route>
      <Route path="/classroom/:id" exact>
        <Home component={<Classroom />} title="Učionica" />
      </Route>
      <Route path="/achievements" exact>
        <Home component={<Achievments />} title="Postignuća" />
      </Route>
      <Route path="/assignments" exact>
        <Home component={<Tasks />} title="Zadatci" />
      </Route>
      <Route path="/assignment/:id" exact>
        <Home component={<Assignment />} title="Zadatak" />
      </Route>
      <Route path="/calendar" exact>
        <Home component={<Calendar />} title="Kalendar" />
      </Route>
      <Route path="/grading" exact>
        <Home component={<Grading />} title="Ocjenjivanje" />
      </Route>
      <Route path="/submission/:id" exact>
        <Home component={<Submission />} title="Predaja" />
      </Route>
    </Switch>
  );
}

export default Routes;
