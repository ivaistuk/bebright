import { useDispatch } from 'react-redux';
import { autoLoginUser } from '../store/actions/auth-actions';

const AuthMiddleWare = ({ children }) => {
  const dispatch = useDispatch();
  if (window.localStorage.getItem('bebright-current-user-token')) {
    dispatch(autoLoginUser());
  }
  return <>{children}</>;
};

export default AuthMiddleWare;
