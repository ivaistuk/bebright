import React from 'react';
import ReactDOM from 'react-dom';

import App from './app/App';
import restServerConfigure, { PRODUCTION_URL } from './utils/configure-axios';

import './styles/index.css';

restServerConfigure(PRODUCTION_URL);

ReactDOM.render(<App />, document.getElementById('root'));
