import { makeStyles } from '@material-ui/core/styles';

export const useFormStyles = makeStyles(theme => ({
  formPaper: {
    marginTop: theme.spacing(8),
    padding: theme.spacing(5),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    backgroundColor: '#ffffff',
    borderRadius: 7,
    position: 'relative',
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  linkToOtherForm: {
    marginTop: theme.spacing(1),
    textAlign: 'center',
  },
  logo: {
    width: 'auto',
    height: theme.spacing(8),
  },
  error: {
    borderColor: 'red',
  },
  overlayingFormContainer: {
    paddingTop: 'calc((100vh - 450px) / 2)',
    position: 'fixed',
    width: '100vw',
    height: '100vh',
    top: 0,
    left: 0,
    backgroundColor: 'rgba(161,161,159,0.62)',
    zIndex: 2020,
  },
  forcePrimaryColor: {
    color: theme.palette.primary.main,
  },
}));

const drawerWidth = 240;
export const useTopBarStyles = makeStyles(theme => ({
  appBar: {
    zIndex: `${theme.zIndex.drawer + 1} !important`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px) !important`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
  },
  title: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: 36,
  },
  menuButtonHidden: {
    display: 'none',
  },
}));

export const useSideBarStyles = makeStyles(theme => ({
  drawerPaper: {
    position: 'relative !important',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden !important',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9),
    },
  },
  toolbarIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  chevron: {
    color: theme.palette.primary.main,
  },
}));

export const useAppFormStyles = makeStyles(theme => ({
  modal: {
    '& ::-webkit-scrollbar-thumb': {
      backgroundColor: ({ color }) =>
        color ? color : theme.palette.primary.main,
    },
  },
  background: ({ color }) => ({
    backgroundColor: color ? color : theme.palette.primary.main,
    color: theme.palette.secondary.light,
  }),
  text: {
    color: theme.palette.secondary.light,
  },
  input: {
    '& .Mui-focused': {
      '& fieldset': {
        border: `1px solid ${theme.palette.secondary.light} !important`,
        color: `${theme.palette.secondary.light} !important`,
      },
    },
    '& label, .MuiInputBase-root': {
      color: `${theme.palette.secondary.light} !important`,
    },
  },
  disabledInput: {
    '& label': {
      color: `#c4c4c4c4 !important`,
    },
    '& .MuiInputBase-root': {
      color: `#c4c4c4c4 !important`,
    },
  },
  warning: {
    color: theme.palette.secondary.light,
    backgroundColor: '#c34d4d',
  },
  errorText: {
    color: '#eec596',
    fontSize: ' 0.8rem !important',
  },
}));

export const useItemPageStyles = makeStyles(theme => ({
  container: { flexGrow: 1, display: 'flex', flexDirection: 'column' },
  paper: {
    marginTop: theme.spacing(6.5),
    marginBottom: theme.spacing(6.5),
    backgroundColor: ({ color }) =>
      color ? color : theme.palette.primary.main,
    color: theme.palette.secondary.light,
    display: 'flex',
    flexDirection: 'column',
    position: 'relative',
    flexGrow: 1,
  },
  title: {
    padding: theme.spacing(5),
    display: 'flex',
    alignItems: 'center',
    '& > :first-child': {
      marginRight: 10,
    },
    '& h3': {
      marginRight: 15,
    },
    '& button': {
      marginRight: 7,
    },
  },
  body: {
    flexGrow: 1,
    display: 'flex',
    flexDirection: 'column',
  },
}));

export const useItemInfoStyles = makeStyles(theme => ({
  panel: {
    margin: '0 20px 5px 20px',
    backgroundColor: 'inherit',
    color: 'inherit',
    display: 'flex',
    flexDirection: 'column',
    minWidth: '60%',
  },
  field: {
    backgroundColor: 'inherit',
    color: 'inherit',
    padding: 15,
    flexGrow: 1,
    '& .MuiFab-sizeSmall': {
      marginTop: -6,
    },
  },
  heading: {
    marginBottom: 11,
    marginLeft: 10,
  },
  paragraph: {
    padding: 20,
    paddingLeft: 30,
    paddingRight: 30,
    backgroundColor: theme.palette.secondary.light,
    color: theme.palette.text.primary,
    borderRadius: 4,
  },
  title: {
    paddingTop: 10,
    fontWeight: 450,
    marginLeft: 15,
  },
}));
