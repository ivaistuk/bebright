INSERT INTO roles (role_id, role_name) VALUES (1, 'ADMIN');
INSERT INTO roles (role_id, role_name) VALUES (2, 'EDUCATOR');
INSERT INTO roles (role_id, role_name) VALUES (3, 'STUDENT');

INSERT INTO ranks (rank_id, rank_level_required, rank_title) VALUES (1, 1, 'TODDLER');
INSERT INTO ranks (rank_id, rank_level_required, rank_title) VALUES (2, 10, 'ELEMENTARY SCHOOL');
INSERT INTO ranks (rank_id, rank_level_required, rank_title) VALUES (3, 50, 'HIGH SCHOOL');
INSERT INTO ranks (rank_id, rank_level_required, rank_title) VALUES (4, 75, 'FRESHMAN');
INSERT INTO ranks (rank_id, rank_level_required, rank_title) VALUES (5, 90, 'UNDERGRADUATE');
INSERT INTO ranks (rank_id, rank_level_required, rank_title) VALUES (6, 100, 'BACHELORS DEGREE');
INSERT INTO ranks (rank_id, rank_level_required, rank_title) VALUES (7, 110, 'POSTGRADUATE');
INSERT INTO ranks (rank_id, rank_level_required, rank_title) VALUES (8, 120, 'MASTER OF SCIENCE');
INSERT INTO ranks (rank_id, rank_level_required, rank_title) VALUES (9, 150, 'DR.SC.');
INSERT INTO ranks (rank_id, rank_level_required, rank_title) VALUES (10, 160, 'GENIUS');
INSERT INTO ranks (rank_id, rank_level_required, rank_title) VALUES (11, 200, 'EINSTEIN');


INSERT INTO badges (badge_id, badge_name) VALUES (1, 'Procrastinator');
INSERT INTO badges (badge_id, badge_name) VALUES (2, 'Diligent student');
INSERT INTO badges (badge_id, badge_name) VALUES (3, 'Curious creature');
INSERT INTO badges (badge_id, badge_name) VALUES (4, 'Favourite');
INSERT INTO badges (badge_id, badge_name) VALUES (5, 'New face');
INSERT INTO badges (badge_id, badge_name) VALUES (6, 'Speed runner');
INSERT INTO badges (badge_id, badge_name) VALUES (7, 'Champion');
INSERT INTO badges (badge_id, badge_name) VALUES (8, 'Jack of all trades');
INSERT INTO badges (badge_id, badge_name) VALUES (9, 'Unique');
INSERT INTO badges (badge_id, badge_name) VALUES (10, 'Explorer');
INSERT INTO badges (badge_id, badge_name) VALUES (11, 'Coder');
INSERT INTO badges (badge_id, badge_name) VALUES (12, 'Star is born');
INSERT INTO badges (badge_id, badge_name) VALUES (13, 'Traveler');
INSERT INTO badges (badge_id, badge_name) VALUES (14, 'Happy puppy');
INSERT INTO badges (badge_id, badge_name) VALUES (15, 'Unavailable');
INSERT INTO badges (badge_id, badge_name) VALUES (16, 'Level up frenzy');
INSERT INTO badges (badge_id, badge_name) VALUES (17, 'Master of googling');
INSERT INTO badges (badge_id, badge_name) VALUES (18, 'Dostoevsky');
INSERT INTO badges (badge_id, badge_name) VALUES (19, 'Need more classrooms?');
INSERT INTO badges (badge_id, badge_name) VALUES (20, '你好');