package hr.fer.finaltheisis.bebright.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.awt.*;
import java.sql.Timestamp;
import java.util.Set;

@Entity
@Table(name = "assignments")
public class Assignment {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "assignment_id")
    private Long id;

    @NotEmpty
    @Column(name="assignment_title")
    private String title;

    @Column(name ="assignment_description")
    private String description;

    @Column(name = "assignment_file_name")
    private String fileName;

    @Column(name = "assignment_active_until")
    private Timestamp activeUntil;

    @CreationTimestamp
    @Column(name = "assignment_created_ts")
    private Timestamp created;

    @UpdateTimestamp
    @Column(name = "assignment_modified_ts")
    private Timestamp modified;

    @Column(name = "assignment_archived_ts")
    private Timestamp archived = null;

    @ManyToOne
    @JoinColumn(name = "classroom_id", nullable = false)
    private Classroom classroom;

    @ManyToOne
    @JoinColumn(name = "organizer_id", referencedColumnName = "user_id", nullable = false)
    private User organizer;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "assignment")
    private Set<Submission> submissions;

    @Column(name = "assignment_points")
    private Long points = null;

    public Assignment() {}

    public Assignment(String title, Timestamp activeUntil, String description, String fileName, User organizer, Classroom classroom) {
        this.activeUntil = activeUntil;
        this.title = title;
        this.description = description;
        this.fileName = fileName;
        this.organizer = organizer;
        this.classroom = classroom;
    }

    public Assignment(String title, Timestamp activeUntil, String description, String fileName, User organizer, Classroom classroom, Long points) {
        this.activeUntil = activeUntil;
        this.title = title;
        this.description = description;
        this.fileName = fileName;
        this.organizer = organizer;
        this.classroom = classroom;
        this.points = points;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Timestamp getActiveUntil() {
        return activeUntil;
    }

    public void setActiveUntil(Timestamp activeUntil) {
        this.activeUntil = activeUntil;
    }

    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    public Timestamp getModified() {
        return modified;
    }

    public void setModified(Timestamp modified) {
        this.modified = modified;
    }

    public Timestamp getArchived() {
        return archived;
    }

    public void setArchived(Timestamp archived) {
        this.archived = archived;
    }

    public Classroom getClassroom() {
        return classroom;
    }

    public void setClassroom(Classroom classroom) {
        this.classroom = classroom;
    }

    public User getOrganizer() {
        return organizer;
    }

    public void setOrganizer(User organizer) {
        this.organizer = organizer;
    }

    public Set<Submission> getSubmissions() {
        return submissions;
    }

    public void setSubmissions(Set<Submission> submissions) {
        this.submissions = submissions;
    }

    public Long getPoints() {
        return points;
    }

    public void setPoints(Long points) {
        this.points = points;
    }
}
