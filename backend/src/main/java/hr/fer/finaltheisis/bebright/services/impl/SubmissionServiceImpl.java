package hr.fer.finaltheisis.bebright.services.impl;

import hr.fer.finaltheisis.bebright.dtos.submission.CreateSubmissionDTO;
import hr.fer.finaltheisis.bebright.dtos.submission.EditSubmissionDTO;
import hr.fer.finaltheisis.bebright.dtos.user.UpdateUserDTO;
import hr.fer.finaltheisis.bebright.exceptions.NoSuchEntityException;
import hr.fer.finaltheisis.bebright.models.Assignment;
import hr.fer.finaltheisis.bebright.models.Submission;
import hr.fer.finaltheisis.bebright.models.User;
import hr.fer.finaltheisis.bebright.repositories.SubmissionRepository;
import hr.fer.finaltheisis.bebright.services.AchievementService;
import hr.fer.finaltheisis.bebright.services.AssignmentService;
import hr.fer.finaltheisis.bebright.services.SubmissionService;
import hr.fer.finaltheisis.bebright.services.UserService;
import hr.fer.finaltheisis.bebright.utils.FileIO;
import hr.fer.finaltheisis.bebright.utils.RoleNames;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.nio.file.Path;
import java.security.Principal;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
public class SubmissionServiceImpl implements SubmissionService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SubmissionServiceImpl.class);

    private final SubmissionRepository submissionRepository;
    private final UserService userService;
    private final AssignmentService assignmentService;
    private final AchievementService achievementService;

    @Autowired
    public SubmissionServiceImpl(SubmissionRepository submissionRepository, UserService userService,
                                 AssignmentService assignmentService, AchievementService achievementService) {
        this.submissionRepository = submissionRepository;
        this.userService = userService;
        this.assignmentService = assignmentService;
        this.achievementService = achievementService;
    }

    @Override
    public Submission findSubmissionById(Long id) {
        LOGGER.info("Finding assignment with id: " + id);
        return submissionRepository
                .findById(id).orElseThrow(() -> new  NoSuchEntityException("No Submission could be found!"));
    }

    private static final String SUBMISSION_FILES = "src/main/resources/public/files/submissions/";

    @Override
    public void saveSubmissionFile(byte[] file, Long id) {
        if(file == null) return;
        Submission submission = findSubmissionById(id);
        if(!submission.getFileName().isEmpty()) {
            LOGGER.info("Storing file...");
            Path path = Path.of(SUBMISSION_FILES + "submission_" + id.toString() + "_"
                    + submission.getFileName());
            FileIO.writeFile(path, file, submission.getFileName());
        }
    }

    @Override
    public ResponseEntity<Resource> getSubmissionFileResource(Long id) {
        Submission submission = findSubmissionById(id);
        if(submission.getFileName() != null) {
            LOGGER.info("Fetching file for download...");
            Path path = Path.of(SUBMISSION_FILES + "submission_" + id.toString() + "_"
                    + submission.getFileName());
            return FileIO.getFileResource(path, submission.getFileName());
        }
        return ResponseEntity.ok(null);
    }


    @Override
    public Long createSubmission(CreateSubmissionDTO dto, Principal principal) {
        LOGGER.info("Creating a new submission submission");
        User student = userService.findUserByUsername(principal.getName());
        Assignment assignment = assignmentService.findAssignmentById(dto.getAssignmentId());
        Submission submission = submissionRepository.save(new Submission(assignment, student, dto.getUploadedText(), dto.getFileName()));
        return submission.getId();
    }


    @Override
    public void updateSubmission(EditSubmissionDTO dto) {
        Submission submission = findSubmissionById(dto.getId());
        LOGGER.info("Updating submission with id: " + submission.getId());
        submission.setPoints(dto.getPoints());
        submissionRepository.save(submission);

        User student = submission.getStudent();
        userService.updateUser(new UpdateUserDTO(student,
                achievementService.getLevel(userService.calculateUserExperience(student)) + 1));
    }

    @Override
    public void deleteSubmission(Long id) {
        LOGGER.info("Deleting submission with id: " + id);
        Submission submission = findSubmissionById(id);
        submissionRepository.delete(submission);
    }

    private Submission getLatestSubmission(Assignment assignment, User student) {
        var submissions = submissionRepository.findByAssignmentAndStudent(assignment, student);
        return submissions.stream().max(Comparator.comparing(Submission::getCreated))
                .orElseThrow(() -> new NoSuchEntityException("There's no submissions for this assignment and this user"));
    }

    @Override
    public Set<Submission> getAssignmentSubmissions(Long assignmentId) {
        Assignment assignment = assignmentService.findAssignmentById(assignmentId);
        Set<User> students = assignment.getSubmissions().stream().map(Submission::getStudent).collect(Collectors.toSet());
        LOGGER.info("Finding submissions of assignment: " + assignment.getTitle());
        Set<Submission> latest = new HashSet<>();
        for(var s : students)
            latest.add(getLatestSubmission(assignment, s));
        return latest;
    }

    @Override
    public Set<Submission> getUserSubmissions(Long assignmentId, Principal principal) {
        Assignment assignment = assignmentService.findAssignmentById(assignmentId);
        User student = userService.getCurrentUser(principal, RoleNames.ADMIN, RoleNames.STUDENT);
        return new HashSet<>(submissionRepository.findByAssignmentAndStudent(assignment, student));
    }

    private Set<Submission> extractSubmissions(Set<Assignment> assignments, Predicate<Submission> predicate) {
        return assignments.stream().map(Assignment::getId)
                .map(this::getAssignmentSubmissions)
                .flatMap(Set::stream).filter(predicate)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<Submission> findUngradedEducatorSubmissions(Principal principal) {
        LOGGER.info("Finding all ungraded submissions of an educator assignments: " + principal.getName());
        User user = userService.getCurrentUser(principal, RoleNames.ADMIN, RoleNames.EDUCATOR);
        return extractSubmissions(user.getAssignments(), submission -> submission.getPoints() == null);
    }

    @Override
    public Set<Submission> findGradedEducatorSubmissions(Principal principal) {
        LOGGER.info("Finding all graded submissions of an educator assignments: " + principal.getName());
        User user = userService.getCurrentUser(principal, RoleNames.ADMIN, RoleNames.EDUCATOR);
        return extractSubmissions(user.getAssignments(), submission -> submission.getPoints() != null);
    }
}
