package hr.fer.finaltheisis.bebright.services.impl;

import hr.fer.finaltheisis.bebright.dtos.assignement.GetAssignmentDTO;
import hr.fer.finaltheisis.bebright.dtos.classroom.CreateClassroomDTO;
import hr.fer.finaltheisis.bebright.dtos.classroom.CreateFileDTO;
import hr.fer.finaltheisis.bebright.dtos.classroom.EditClassroomDTO;
import hr.fer.finaltheisis.bebright.exceptions.NoSuchEntityException;
import hr.fer.finaltheisis.bebright.models.*;
import hr.fer.finaltheisis.bebright.repositories.ClassroomRepository;
import hr.fer.finaltheisis.bebright.services.*;
import hr.fer.finaltheisis.bebright.utils.FileIO;
import hr.fer.finaltheisis.bebright.utils.RoleNames;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.nio.file.Path;
import java.security.Principal;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ClassroomServiceImpl implements ClassroomService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

    private final ClassroomRepository classroomRepository;
    private final ClassOrganizationService classOrganizationService;
    private final ClassParticipationService classParticipationService;
    private final UserService userService;
    private final FileService fileService;

    @Autowired
    public ClassroomServiceImpl(ClassroomRepository classroomRepository, ClassOrganizationService classOrganizationService,
                                ClassParticipationService classParticipationService, UserService userService, FileService fileService) {
        this.classroomRepository = classroomRepository;
        this.classOrganizationService = classOrganizationService;
        this.classParticipationService = classParticipationService;
        this.userService = userService;
        this.fileService = fileService;
    }

    @Override
    public Classroom findClassroomById(Long id) {
        LOGGER.info("Searching for classroom with id: " + id);
        return classroomRepository.findById(id)
                .orElseThrow(() -> new NoSuchEntityException("No classroom by given id!"));
    }

    @Override
    public void createClassroom(CreateClassroomDTO dto, Principal principal) {
        LOGGER.info("Creating new classroom with name: " + dto.getName());
        User organizer = userService.getCurrentUser(principal, RoleNames.EDUCATOR);
        Classroom classroom = classroomRepository.save(new Classroom(dto.getName(), dto.getPicture()));
        classOrganizationService.addUserAsClassOrganizer(classroom, organizer);
    }

    @Override
    public void updateClassroom(EditClassroomDTO dto) {
        Classroom classroom = findClassroomById(dto.getId());
        LOGGER.info("Updating classroom with name: " + classroom.getName());
        classroom.setName(dto.getName());
        classroom.setBackground(dto.getPicture());
        classroomRepository.save(classroom);
    }

    @Override
    public void deleteClassroom(Long id) {
        Classroom classroom = findClassroomById(id);
        LOGGER.info("Deleting classroom with name: " + classroom.getName());
        classroomRepository.delete(classroom);
    }

    public void addUserAsOrganizerOfClassroom(Long userId, Long classId) {
        LOGGER.info("Adding user with id: " + userId + " as organizer for classroom with id: " + classId);

        classOrganizationService.addUserAsClassOrganizer(findClassroomById(classId),
                userService.findUserById(userId));
    }

    @Override
    public void addStudentToClassroom(Long userId, Long classId) {
        LOGGER.info("Adding user with id: " + userId + " as participant for classroom with id: " + classId);

        classParticipationService.addUserAsClassParticipant(findClassroomById(classId),
                userService.findUserById(userId));
    }

    @Override
    public void removeOrganizersFromClassroom(Long userId, Long classId) {
        LOGGER.info("Removing organizer with id: " + userId + " from classroom with id: " + classId);
        classOrganizationService.removeUserFromClassOrganization(findClassroomById(classId), userService.findUserById(userId));
    }

    @Override
    public void removeStudentFromClassroom(Long userId, Long classId) {
        LOGGER.info("Removing student with id: " + userId + " from classroom with id: " + classId);
        classParticipationService.removeUserFromClassParticipation(findClassroomById(classId), userService.findUserById(userId));
    }

    @Override
    public Set<User> findAllOrganizersOfClassroom(Long id) {
        LOGGER.info("Finding all organizers of a classroom with id: " + id);
        Classroom classroom = findClassroomById(id);
        return classroom.getClassOrganizationGroup().stream().map(ClassOrganization::getOrganizer).collect(Collectors.toSet());
    }

    @Override
    public Set<User> findAllStudentsOfClassroom(Long id) {
        LOGGER.info("Finding all students of a classroom with id: " + id);
        Classroom classroom = findClassroomById(id);
        return classroom.getStudentClassGroup().stream().map(ClassParticipation::getStudent).collect(Collectors.toSet());
    }

    @Override
    public Set<GetAssignmentDTO> findAllAssignmentsOfClassroom(Long id, Principal principal) {
        LOGGER.info("Finding all assignments of a classroom with id: " + id);

        User user = userService.findUserByUsername(principal.getName());
        Classroom classroom = findClassroomById(id);
        return classroom.getAssignments().stream().map(assignment -> new GetAssignmentDTO(assignment, user)).collect(Collectors.toSet());
    }

    private static final String CLASSROOM_FILES = "src/main/resources/public/files/";

    @Override
    public Long addFileToClassroom(CreateFileDTO dto, Long classId) {
        return fileService.createFile(dto, findClassroomById(classId)).getId();
    }

    @Override
    public void removeFileFromClassroom(Long fileId) {
        fileService.deleteFile(fileId);
    }

    @Override
    public void saveClassroomFile(byte[] file, Long fileId, Long classId) {
        if(file == null) return;
        LOGGER.info("Storing file...");
        Path path = Path.of(CLASSROOM_FILES + "classroom_" + classId + "_file_"
                + fileId);
        FileIO.writeFile(path, file, "file_" + fileId);
    }

    @Override
    public ResponseEntity<Resource> getClassroomFileResource(Long fileId, Long classId) {
        LOGGER.info("Fetching file for download...");
        Path path = Path.of(CLASSROOM_FILES + "classroom_" + classId + "_file_" + fileId);
        File file = fileService.findFileById(fileId);
        return FileIO.getFileResource(path, file.getFileName());
    }

}
