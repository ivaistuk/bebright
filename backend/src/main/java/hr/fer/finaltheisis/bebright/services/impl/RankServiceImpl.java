package hr.fer.finaltheisis.bebright.services.impl;

import hr.fer.finaltheisis.bebright.models.Rank;
import hr.fer.finaltheisis.bebright.repositories.RankRepository;
import hr.fer.finaltheisis.bebright.services.RankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RankServiceImpl implements RankService {

    private final RankRepository rankRepository;

    @Autowired
    public RankServiceImpl(RankRepository rankRepository) {
        this.rankRepository = rankRepository;
    }

    @Override
    public Rank findRankByLevel(Long level) {
        var ranks = rankRepository.findAll();
        Rank maxRank = null;
        long maxRequiredLevel = 1;
        for(var r : ranks)
            if(r.getLevelRequired() >= maxRequiredLevel && level >= r.getLevelRequired()) {
                maxRank = r;
                maxRequiredLevel = r.getLevelRequired();
            }
        return maxRank;
    }
}
