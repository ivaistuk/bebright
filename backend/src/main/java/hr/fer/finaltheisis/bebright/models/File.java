package hr.fer.finaltheisis.bebright.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "files")
public class File {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "file_id")
    private Long id;

    @CreationTimestamp
    @Column(name = "file_created_ts")
    private Timestamp created;

    @UpdateTimestamp
    @Column(name = "file_modified_ts")
    private Timestamp modified;

    @Column(name = "file_archived_ts")
    private Timestamp archived = null;

    @Column(name = "file_folder")
    private String folder;

    @Column(name = "file_name")
    private String fileName;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "classroom_id")
    private Classroom classroom;

    public File() {}

    public File(String folder, String fileName, Classroom classroom) {
        this.folder = folder;
        this.fileName = fileName;
        this.classroom = classroom;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    public Timestamp getModified() {
        return modified;
    }

    public void setModified(Timestamp modified) {
        this.modified = modified;
    }

    public Timestamp getArchived() {
        return archived;
    }

    public void setArchived(Timestamp archived) {
        this.archived = archived;
    }

    public String getFolder() {
        return folder;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Classroom getClassroom() {
        return classroom;
    }

    public void setClassroom(Classroom classroom) {
        this.classroom = classroom;
    }
}
