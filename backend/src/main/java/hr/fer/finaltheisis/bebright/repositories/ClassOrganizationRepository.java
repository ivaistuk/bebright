package hr.fer.finaltheisis.bebright.repositories;

import hr.fer.finaltheisis.bebright.models.ClassOrganization;
import hr.fer.finaltheisis.bebright.models.Classroom;
import hr.fer.finaltheisis.bebright.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClassOrganizationRepository extends JpaRepository<ClassOrganization, Long> {
    ClassOrganization findByClassroomAndOrganizer(Classroom classroom, User organizer);
}
