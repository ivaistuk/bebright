package hr.fer.finaltheisis.bebright.repositories;

import hr.fer.finaltheisis.bebright.models.Assignment;
import hr.fer.finaltheisis.bebright.models.Submission;
import hr.fer.finaltheisis.bebright.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface SubmissionRepository extends JpaRepository<Submission, Long> {
    List<Submission> findByAssignmentAndStudent(Assignment assignment, User student);

}
