package hr.fer.finaltheisis.bebright.controllers;

import hr.fer.finaltheisis.bebright.dtos.assignement.CreateAssignmentDTO;
import hr.fer.finaltheisis.bebright.dtos.assignement.EditAssignmentDTO;
import hr.fer.finaltheisis.bebright.models.Submission;
import hr.fer.finaltheisis.bebright.services.AssignmentService;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Set;

@RestController
@RequestMapping("/api/assignment")
public class AssignmentController {

    private AssignmentService assignmentService;

    @Autowired
    public AssignmentController(AssignmentService assignmentService) {
        this.assignmentService = assignmentService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getAssignment(@PathVariable Long id) {
        return ResponseEntity.ok(assignmentService.findAssignmentById(id));
    }

    @PostMapping("/create")
    public ResponseEntity<?> createAssignment(@RequestBody CreateAssignmentDTO dto,  Principal principal) {
        return ResponseEntity.status(HttpStatus.CREATED).body(assignmentService.createAssignment(dto, principal));
    }

    @PostMapping("/{id}/file/upload")
    public ResponseEntity<?> saveAssignmentFile(@RequestBody byte[] file, @PathVariable Long id) {
        assignmentService.saveAssignmentFile(file, id);
        return ResponseEntity.ok("File stored");
    }

    @GetMapping("/{id}/file/download")
    public ResponseEntity<Resource> downloadAssignmentFile(@PathVariable Long id) {
        return assignmentService.getAssignmentFileResource(id);
    }

    @PutMapping("/update")
    public ResponseEntity<?> updateAssignment(@Valid @RequestBody EditAssignmentDTO dto) {
        assignmentService.updateAssignment(dto);
        return ResponseEntity.ok("Assignment successfully updated");
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteAssignment(@PathVariable Long id) {
        assignmentService.deleteAssignment(id);
        return ResponseEntity.ok("Assignment successfully deleted");
    }
}
