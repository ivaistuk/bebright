package hr.fer.finaltheisis.bebright.services;

import hr.fer.finaltheisis.bebright.models.ClassOrganization;
import hr.fer.finaltheisis.bebright.models.Classroom;
import hr.fer.finaltheisis.bebright.models.User;
import org.springframework.stereotype.Service;

@Service
public interface ClassOrganizationService {
    void addUserAsClassOrganizer(Classroom classroom, User user);
    void removeUserFromClassOrganization(Classroom classroom, User user);
}
