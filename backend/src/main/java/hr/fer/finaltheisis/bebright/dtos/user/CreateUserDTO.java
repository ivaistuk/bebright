package hr.fer.finaltheisis.bebright.dtos.user;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.sql.Date;

public class CreateUserDTO {

    @NotBlank
    @Size(max = 100)
    private String name;

    @NotBlank
    @Size(max = 150)
    private String familyName;

    @NotBlank
    @Size(max = 100)
    private String username;

    @NotBlank
    @Size(max = 100)
    private String password;

    @Email
    @NotBlank
    private String email;

    private Date birthDate;
    private byte[] picture;

    @NotBlank
    private String roleName;

    public CreateUserDTO(@NotBlank @Size(max = 100) String name, @NotBlank @Size(max = 150) String familyName,
                         @NotBlank @Size(max = 100) String username, @NotBlank @Size(max = 100) String password,
                         @Email @NotBlank String email, Date birthDate, byte[] picture, @NotBlank String roleName) {
        this.name = name;
        this.familyName = familyName;
        this.username = username;
        this.password = password;
        this.email = email;
        this.birthDate = birthDate;
        this.picture = picture;
        this.roleName = roleName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }
}
