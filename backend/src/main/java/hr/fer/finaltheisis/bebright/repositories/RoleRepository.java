package hr.fer.finaltheisis.bebright.repositories;

import hr.fer.finaltheisis.bebright.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Long> {

    Optional<Role> findByName(String name);
}
