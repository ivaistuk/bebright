package hr.fer.finaltheisis.bebright.controllers.handle;

import com.auth0.jwt.exceptions.TokenExpiredException;
import hr.fer.finaltheisis.bebright.exceptions.*;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.util.HashMap;
import java.util.Map;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class RestExceptionHandler {

    private Map<String, String> makeProps(String message, String status, String error) {
        Map<String, String> props = new HashMap<>();
        props.put("message", message);
        props.put("status", status);
        props.put("error", error);
        return props;
    }

    @ExceptionHandler({IllegalRequestData.class, UserAlreadyExistsException.class,
            UserNotFoundException.class, NoSuchEntityException.class, FileAlreadyExists.class})
    protected ResponseEntity<?> handleIllegalData(Exception e, WebRequest request) {
        return new ResponseEntity<>(makeProps(e.getMessage(), "400", "Bad Request: " + e.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({AuthenticationException.class, TokenExpiredException.class, AuthorizationException.class})
    protected ResponseEntity<?> handleFailedAuthentication(Exception e, WebRequest request) {
        return new ResponseEntity<>(makeProps(e.getMessage(), "401", "Unauthorized"), HttpStatus.BAD_REQUEST);
    }


    @ExceptionHandler({CouldNotOpenFileException.class})
    protected ResponseEntity<?> handleServerError(Exception e, WebRequest request) {
        return new ResponseEntity<>(makeProps(e.getMessage(), "500", "Internal Server Error"), HttpStatus.INTERNAL_SERVER_ERROR);
    }

}

