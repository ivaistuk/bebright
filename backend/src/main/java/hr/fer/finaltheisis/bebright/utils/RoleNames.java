package hr.fer.finaltheisis.bebright.utils;

public class RoleNames {
    public static final String STUDENT = "STUDENT";
    public static final String ADMIN = "ADMIN";
    public static final String EDUCATOR = "EDUCATOR";
}