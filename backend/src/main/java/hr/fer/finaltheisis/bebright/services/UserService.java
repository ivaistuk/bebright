package hr.fer.finaltheisis.bebright.services;

import hr.fer.finaltheisis.bebright.dtos.assignement.GetAssignmentDTO;
import hr.fer.finaltheisis.bebright.dtos.user.CreateUserDTO;
import hr.fer.finaltheisis.bebright.dtos.user.RankedUserDTO;
import hr.fer.finaltheisis.bebright.dtos.user.UpdateUserDTO;
import hr.fer.finaltheisis.bebright.models.*;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;
import java.util.Set;

@Service
public interface UserService {

    User findUserByUsername(String username);
    User findUserById(Long id);
    void createUser(CreateUserDTO dto);
    void updateUser(UpdateUserDTO dto);
    void deleteUser(Long id);
    User getCurrentUser(Principal principal, String... allowedRoles);
    Set<Classroom> findAllClassroomsOfCurrentUser(Principal principal);
    List<Badge> findAllBadgesOfStudent(Long id);
    Set<User> findStudents(String keyword);
    Set<User> findOrganizers(String keyword);
    Set<GetAssignmentDTO> findAllUserAssignments(Principal principal);
    Set<Submission> findAllUserSubmissions(Principal principal);
    Long calculateUserExperience(User user);
    List<RankedUserDTO> getAllStudentsRanked();
    void addBadgeToUser(Long userId, Long id);
}
