package hr.fer.finaltheisis.bebright.dtos.submission;

import javax.validation.constraints.NotNull;

public class CreateSubmissionDTO {
    @NotNull
    private Long assignmentId;
    private String fileName;
    private String uploadedText;


    public CreateSubmissionDTO(Long assignmentId, String fileName, String uploadedText) {
        this.assignmentId = assignmentId;
        this.fileName = fileName;
        this.uploadedText = uploadedText;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getUploadedText() {
        return uploadedText;
    }

    public void setUploadedText(String uploadedText) {
        this.uploadedText = uploadedText;
    }

    public Long getAssignmentId() {
        return assignmentId;
    }

    public void setAssignmentId(Long assignmentId) {
        this.assignmentId = assignmentId;
    }
}
