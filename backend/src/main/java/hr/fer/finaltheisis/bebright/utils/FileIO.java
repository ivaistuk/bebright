package hr.fer.finaltheisis.bebright.utils;

import hr.fer.finaltheisis.bebright.exceptions.CouldNotOpenFileException;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

public class FileIO {
    public static void writeFile(Path path, byte[] file, String fileName) {
        try(OutputStream os = Files.newOutputStream(path, StandardOpenOption.CREATE)) {
            String content = new String(file);
            StringBuilder number = new StringBuilder();
            for(char c : content.toCharArray()) {
                if(Character.isDigit(c) || c == '-')
                    number.append(String.valueOf(c));
                else if(!Character.isDigit(c) && !number.toString().equals("")) {
                    os.write(Byte.parseByte(number.toString()));
                    number = new StringBuilder();
                }
            }
        } catch (IOException e) {
            throw new CouldNotOpenFileException("Something went wrong while opening file " + fileName);
        }
    }

    public static ResponseEntity<Resource> getFileResource(Path path, String fileName) {
        try {
            HttpHeaders headers = new HttpHeaders(); headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName);
            return ResponseEntity.ok()
                    .headers(headers)
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .body(new ByteArrayResource(Files.readAllBytes(path)));
        } catch (IOException e) {
            throw new CouldNotOpenFileException("No such file: " + fileName);
        }
    }
}
