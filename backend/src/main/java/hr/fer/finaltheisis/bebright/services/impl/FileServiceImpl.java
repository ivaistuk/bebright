package hr.fer.finaltheisis.bebright.services.impl;

import hr.fer.finaltheisis.bebright.dtos.classroom.CreateFileDTO;
import hr.fer.finaltheisis.bebright.exceptions.NoSuchEntityException;
import hr.fer.finaltheisis.bebright.models.Classroom;
import hr.fer.finaltheisis.bebright.models.File;
import hr.fer.finaltheisis.bebright.repositories.FileRepository;
import hr.fer.finaltheisis.bebright.services.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FileServiceImpl implements FileService {

    private final FileRepository fileRepository;

    @Autowired
    public FileServiceImpl(FileRepository fileRepository) {
        this.fileRepository = fileRepository;
    }

    @Override
    public File createFile(CreateFileDTO dto, Classroom classroom) {
        return fileRepository.save(new File(dto.getFolder(), dto.getFileName(), classroom));
    }

    @Override
    public void deleteFile(Long id) {
        File file = findFileById(id);
        fileRepository.delete(file);
    }

    @Override
    public File findFileById(Long id) {
        return fileRepository.findById(id)
                .orElseThrow(() -> new NoSuchEntityException("No file found"));
    }
}
