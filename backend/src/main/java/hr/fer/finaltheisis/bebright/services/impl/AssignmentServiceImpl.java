package hr.fer.finaltheisis.bebright.services.impl;

import hr.fer.finaltheisis.bebright.dtos.assignement.CreateAssignmentDTO;
import hr.fer.finaltheisis.bebright.dtos.assignement.EditAssignmentDTO;
import hr.fer.finaltheisis.bebright.exceptions.NoSuchEntityException;
import hr.fer.finaltheisis.bebright.models.Assignment;
import hr.fer.finaltheisis.bebright.models.Classroom;
import hr.fer.finaltheisis.bebright.models.User;
import hr.fer.finaltheisis.bebright.repositories.AssignmentRepository;
import hr.fer.finaltheisis.bebright.services.AssignmentService;
import hr.fer.finaltheisis.bebright.services.ClassroomService;
import hr.fer.finaltheisis.bebright.services.UserService;
import hr.fer.finaltheisis.bebright.utils.FileIO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.nio.file.Path;
import java.security.Principal;

@Service
public class AssignmentServiceImpl implements AssignmentService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AssignmentServiceImpl.class);

    private final AssignmentRepository assignmentRepository;
    private final ClassroomService classroomService;
    private final UserService userService;

    @Autowired
    public AssignmentServiceImpl(AssignmentRepository assignmentRepository, ClassroomService classroomService, UserService userService) {
        this.assignmentRepository = assignmentRepository;
        this.classroomService = classroomService;
        this.userService = userService;
    }

    private static final String ASSIGNMENT_FILES = "src/main/resources/public/files/assignments/";

    @Override
    public void saveAssignmentFile(byte[] file, Long id) {
        if(file == null) return;
        Assignment assignment = findAssignmentById(id);
        if(!assignment.getFileName().isEmpty()) {
            LOGGER.info("Storing file...");
            Path path = Path.of(ASSIGNMENT_FILES + "assignment_" + id.toString() + "_"
                    + assignment.getFileName());
            FileIO.writeFile(path, file, assignment.getFileName());
        }

    }

    @Override
    public ResponseEntity<Resource> getAssignmentFileResource(Long id) {
        Assignment assignment = findAssignmentById(id);
        if(assignment.getFileName() != null) {
            LOGGER.info("Fetching file for download...");
            Path path = Path.of(ASSIGNMENT_FILES + "assignment_" + id.toString() + "_"
                    + assignment.getFileName());
            return FileIO.getFileResource(path, assignment.getFileName());
        }
        return ResponseEntity.ok(null);
    }

    private static final Long MAX_POINTS = 1500L;

    @Override
    public Long createAssignment(CreateAssignmentDTO dto, Principal principal) {
        LOGGER.info("Creating assignment with name: " + dto.getTitle());
        User organizer = userService.findUserByUsername(principal.getName());
        Classroom classroom = classroomService.findClassroomById(dto.getClassroomId());
        Assignment assignment = assignmentRepository.save(new Assignment(dto.getTitle(), dto.getActiveUntil(), dto.getDescription(),
                dto.getFileName(), organizer, classroom, dto.getPoints() <= MAX_POINTS ? dto.getPoints() : MAX_POINTS));
        return assignment.getId();
    }

    @Override
    public void updateAssignment(EditAssignmentDTO dto) {
        Assignment assignment = findAssignmentById(dto.getId());
        LOGGER.info("Updating assignment with name: " + assignment.getTitle());
        assignment.setActiveUntil(dto.getActiveUntil());
        assignment.setTitle(dto.getTitle());
        assignment.setDescription(dto.getDescription());
        assignment.setFileName(dto.getFileName());
        assignment.setPoints(dto.getPoints());

        assignmentRepository.save(assignment);
    }

    @Override
    public void deleteAssignment(Long id) {
        LOGGER.info("Deleting assignment with id: " + id);
        Assignment assignment = findAssignmentById(id);
        assignmentRepository.delete(assignment);
    }

    @Override
    public Assignment findAssignmentById(Long id) {
        LOGGER.info("Finding assignment with id: " + id);
        return assignmentRepository.findById(id).orElseThrow(() -> new  NoSuchEntityException("No Assignment could be found!"));
    }

}
