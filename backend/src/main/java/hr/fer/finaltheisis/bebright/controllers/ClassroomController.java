package hr.fer.finaltheisis.bebright.controllers;

import hr.fer.finaltheisis.bebright.dtos.classroom.CreateClassroomDTO;
import hr.fer.finaltheisis.bebright.dtos.classroom.CreateFileDTO;
import hr.fer.finaltheisis.bebright.dtos.classroom.EditClassroomDTO;
import hr.fer.finaltheisis.bebright.services.ClassroomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;

@RestController
@RequestMapping("/api/classroom")
public class ClassroomController {

    private ClassroomService classroomService;

    @Autowired
    public ClassroomController(ClassroomService classroomService) {
        this.classroomService = classroomService;
    }

    @PostMapping("/create")
    public ResponseEntity<?> createClassroom(@Valid @RequestBody CreateClassroomDTO dto, Principal principal) {
        classroomService.createClassroom(dto, principal);
        return ResponseEntity.status(HttpStatus.CREATED).body("Classroom successfully created!");
    }

    @PutMapping("/update")
    public ResponseEntity<?> updateClassroom(@Valid @RequestBody EditClassroomDTO dto) {
        classroomService.updateClassroom(dto);
        return ResponseEntity.ok("Classroom successfully updated!");
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteClassroom(@PathVariable Long id) {
        classroomService.deleteClassroom(id);
        return ResponseEntity.status(HttpStatus.CREATED).body("Classroom successfully created!");
    }


    @PostMapping("/{id}/add/organizer")
    public ResponseEntity<?> addOrganizerToClassroom(@PathVariable("id") Long classId, @RequestBody Long id) {
        classroomService.addUserAsOrganizerOfClassroom(id, classId);
        return ResponseEntity.ok("User successfully added to class organization.");
    }

    @PostMapping("/{id}/add/student")
    public ResponseEntity<?> addStudentToClassroom(@PathVariable("id") Long classId, @RequestBody Long id) {
        classroomService.addStudentToClassroom(id, classId);
        return ResponseEntity.ok("Student successfully added to classroom.");
    }

    @PostMapping("/{id}/remove/organizer")
    public ResponseEntity<?> removeOrganizersFromClassroom(@PathVariable("id") Long classId, @RequestBody Long id) {
        classroomService.removeOrganizersFromClassroom(id, classId);
        return ResponseEntity.ok("User successfully removed from class organization.");
    }

    @PostMapping("/{id}/remove/student")
    public ResponseEntity<?> removeStudentFromClassroom(@PathVariable("id") Long classId, @RequestBody Long id) {
        classroomService.removeStudentFromClassroom(id, classId);
        return ResponseEntity.ok("Student successfully removed from classroom.");
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getClassroom(@PathVariable("id") Long id) {
        return ResponseEntity.ok(classroomService.findClassroomById(id));
    }

    @GetMapping("/{id}/organizers")
    public ResponseEntity<?> getClassroomOrganizers(@PathVariable("id") Long id) {
        return ResponseEntity.ok(classroomService.findAllOrganizersOfClassroom(id));
    }

    @GetMapping("/{id}/students")
    public ResponseEntity<?> getClassroomStudents(@PathVariable("id") Long id) {
        return ResponseEntity.ok(classroomService.findAllStudentsOfClassroom(id));
    }

    @GetMapping("/{id}/assignments")
    public ResponseEntity<?> getAssignments(@PathVariable("id") Long id, Principal principal) {
        return ResponseEntity.ok(classroomService.findAllAssignmentsOfClassroom(id, principal));
    }


    @PostMapping("/{id}/add/file")
    public ResponseEntity<?> addFileToClassroom(@RequestBody CreateFileDTO dto, @PathVariable("id") Long id) {
        return ResponseEntity.ok(classroomService.addFileToClassroom(dto, id));
    }

    @DeleteMapping("/remove/file/{fileId}")
    public ResponseEntity<?> removeFileFromClassroom(@PathVariable("fileId") Long fileId) {
        classroomService.removeFileFromClassroom(fileId);
        return ResponseEntity.ok("File removed!");
    }

    @PostMapping("/{classId}/file/upload/{fileId}")
    public ResponseEntity<?> saveClassroomFile(@RequestBody byte[] file,
                                               @PathVariable("classId") Long classId,
                                               @PathVariable("fileId") Long fileId) {
        classroomService.saveClassroomFile(file, fileId, classId);
        return ResponseEntity.ok("File stored");
    }

    @GetMapping("/{classId}/file/download/{fileId}")
    public ResponseEntity<Resource> downloadClassroomFile(@PathVariable("classId") Long classId,
                                                          @PathVariable("fileId") Long fileId)  {
        return classroomService.getClassroomFileResource(fileId, classId);
    }
}
