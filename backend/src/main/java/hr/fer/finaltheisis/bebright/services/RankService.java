package hr.fer.finaltheisis.bebright.services;

import hr.fer.finaltheisis.bebright.models.Rank;
import org.springframework.stereotype.Service;

@Service
public interface RankService {
    Rank findRankByLevel(Long level);
}
