package hr.fer.finaltheisis.bebright.dtos.user;

import hr.fer.finaltheisis.bebright.models.User;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Date;

public class UpdateUserDTO {
    @NotNull
    private Long id;
    @Size(max = 100)
    private String name;
    @Size(max = 150)
    private String familyName;
    @Size(max = 100)
    private String username;
    @Size(max = 100)
    private String password;
    @Email
    private String email;
    private Date birthDate;
    private byte[] picture = null;
    private Long level = null;

    public UpdateUserDTO() {
    }

    public UpdateUserDTO(Long id, String name, String familyName, String username, String password, String email, Date birthDate, byte[] picture) {
        this.id = id;
        this.name = name;
        this.familyName = familyName;
        this.username = username;
        this.password = password;
        this.email = email;
        this.birthDate = birthDate;
        this.picture = picture;
    }


    public UpdateUserDTO(User user, Long level) {
        id = user.getId();
        name = user.getName();
        familyName = user.getFamilyName();
        username = user.getUsername();
        password = null;
        email = user.getEmail();
        birthDate = user.getBirthDate();
        picture = user.getPicture();
        this.level = level;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    public Long getLevel() {
        return level;
    }

    public void setLevel(Long level) {
        this.level = level;
    }
}
