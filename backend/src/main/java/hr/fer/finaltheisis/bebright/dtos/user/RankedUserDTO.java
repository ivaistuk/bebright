package hr.fer.finaltheisis.bebright.dtos.user;
import hr.fer.finaltheisis.bebright.models.Rank;
import hr.fer.finaltheisis.bebright.models.Role;
import hr.fer.finaltheisis.bebright.models.User;
import java.sql.Date;
import java.sql.Timestamp;

public class RankedUserDTO {

    private Long id;
    private String name;
    private String familyName;
    private String username;
    private String email;
    private Date birthDate;
    private byte[] picture;
    private Long level;
    private Timestamp created;
    private Timestamp modified;
    private Timestamp archived;
    private Role role;
    private Rank rank;
    private Long required;
    private Long experience;


    public RankedUserDTO(User user, Long required, Long experience) {
        id = user.getId();
        name = user.getName();
        familyName = user.getFamilyName();
        username = user.getUsername();
        email = user.getEmail();
        birthDate = user.getBirthDate();
        picture = user.getPicture();
        level = user.getLevel();
        created = user.getCreated();
        modified = user.getModified();
        archived = user.getArchived();
        role = user.getRole();
        rank = user.getRank();
        this.required = required;
        this.experience = experience;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    public Long getLevel() {
        return level;
    }

    public void setLevel(Long level) {
        this.level = level;
    }

    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    public Timestamp getModified() {
        return modified;
    }

    public void setModified(Timestamp modified) {
        this.modified = modified;
    }

    public Timestamp getArchived() {
        return archived;
    }

    public void setArchived(Timestamp archived) {
        this.archived = archived;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Rank getRank() {
        return rank;
    }

    public void setRank(Rank rank) {
        this.rank = rank;
    }

    public Long getRequired() {
        return required;
    }

    public void setRequired(Long required) {
        this.required = required;
    }

    public Long getExperience() {
        return experience;
    }

    public void setExperience(Long experience) {
        this.experience = experience;
    }
}
