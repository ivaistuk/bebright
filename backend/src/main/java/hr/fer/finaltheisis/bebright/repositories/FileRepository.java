package hr.fer.finaltheisis.bebright.repositories;

import hr.fer.finaltheisis.bebright.models.File;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FileRepository extends JpaRepository<File, Long> {


}
