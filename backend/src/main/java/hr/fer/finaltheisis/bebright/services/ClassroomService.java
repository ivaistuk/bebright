package hr.fer.finaltheisis.bebright.services;

import hr.fer.finaltheisis.bebright.dtos.assignement.GetAssignmentDTO;
import hr.fer.finaltheisis.bebright.dtos.classroom.CreateClassroomDTO;
import hr.fer.finaltheisis.bebright.dtos.classroom.CreateFileDTO;
import hr.fer.finaltheisis.bebright.dtos.classroom.EditClassroomDTO;
import hr.fer.finaltheisis.bebright.models.*;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.Set;

@Service
public interface ClassroomService {

    Classroom findClassroomById(Long id);
    void createClassroom(CreateClassroomDTO dto, Principal principal);
    void updateClassroom(EditClassroomDTO dto);
    void deleteClassroom(Long id);

    void addUserAsOrganizerOfClassroom(Long userId, Long classId);
    void addStudentToClassroom(Long userId, Long id);
    Set<User> findAllOrganizersOfClassroom(Long id);
    Set<User> findAllStudentsOfClassroom(Long id);
    Set<GetAssignmentDTO> findAllAssignmentsOfClassroom(Long id, Principal principal);

    void removeOrganizersFromClassroom(Long userId, Long classId);
    void removeStudentFromClassroom(Long userId, Long classId);


    Long addFileToClassroom(CreateFileDTO dto, Long classId);
    void removeFileFromClassroom(Long fileId);

    void saveClassroomFile(byte[] file, Long fileId, Long classId);
    ResponseEntity<Resource> getClassroomFileResource(Long fileId, Long classId);
}
