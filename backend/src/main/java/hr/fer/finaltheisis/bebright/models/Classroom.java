package hr.fer.finaltheisis.bebright.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Set;

@Entity
@Table(name = "classrooms")
public class Classroom {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "classroom_id")
    private Long id;

    @Column(name = "classroom_name", nullable = false)
    private String name;

    @Column(name = "class_picture")
    private byte[] background;

    @JsonIgnore
    @CreationTimestamp
    @Column(name = "classroom_created_ts")
    private Timestamp created;

    @JsonIgnore
    @UpdateTimestamp
    @Column(name = "classroom_modified_ts")
    private Timestamp modified;

    @JsonIgnore
    @Column(name = "classroom_archived_ts")
    private Timestamp archived = null;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "classroom")
    private Set<Assignment> assignments;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "classroom")
    private Set<ClassParticipation> studentClassGroup;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "classroom")
    private Set<ClassOrganization> classOrganizationGroup;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "classroom")
    private Set<File> files;

    public Set<File> getFiles() {
        return files;
    }

    public void setFiles(Set<File> files) {
        this.files = files;
    }

    public Classroom() {}

    public Classroom(String name, byte[] picture) {
        this.name = name;
        this.background = picture;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getBackground() {
        return background;
    }

    public void setBackground(byte[] background) {
        this.background = background;
    }

    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    public Timestamp getModified() {
        return modified;
    }

    public void setModified(Timestamp modified) {
        this.modified = modified;
    }

    public Timestamp getArchived() {
        return archived;
    }

    public void setArchived(Timestamp archived) {
        this.archived = archived;
    }

    public Set<Assignment> getAssignments() {
        return assignments;
    }

    public void setAssignments(Set<Assignment> assignments) {
        this.assignments = assignments;
    }

    public Set<ClassParticipation> getStudentClassGroup() {
        return studentClassGroup;
    }

    public void setStudentClassGroup(Set<ClassParticipation> studentClassGroup) {
        this.studentClassGroup = studentClassGroup;
    }

    public Set<ClassOrganization> getClassOrganizationGroup() {
        return classOrganizationGroup;
    }

    public void setClassOrganizationGroup(Set<ClassOrganization> classOrganizationGroup) {
        this.classOrganizationGroup = classOrganizationGroup;
    }
}
