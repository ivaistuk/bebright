package hr.fer.finaltheisis.bebright.services.impl;

import hr.fer.finaltheisis.bebright.exceptions.NoSuchEntityException;
import hr.fer.finaltheisis.bebright.models.Role;
import hr.fer.finaltheisis.bebright.repositories.RoleRepository;
import hr.fer.finaltheisis.bebright.services.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl implements RoleService {

    private RoleRepository roleRepository;

    @Autowired
    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public Role findRoleByName(String name) {
        return roleRepository.findByName(name).orElseThrow(
                () -> new NoSuchEntityException("Role: " + name + " doesn't exist.")
        );
    }
}
