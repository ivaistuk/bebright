package hr.fer.finaltheisis.bebright.security;

public class JWTConstants {
    public static final String SECRET = "SecretKeyForGenOfJWT";
    public static final String HEADER = "Authorization";
    public static final String PREFIX = "Bearer ";
    public static final Long EXPIRATION_DATE = 86400000L;
}
