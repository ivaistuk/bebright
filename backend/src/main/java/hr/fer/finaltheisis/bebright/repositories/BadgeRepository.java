package hr.fer.finaltheisis.bebright.repositories;

import hr.fer.finaltheisis.bebright.models.Badge;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BadgeRepository extends JpaRepository<Badge, Long> {
}
