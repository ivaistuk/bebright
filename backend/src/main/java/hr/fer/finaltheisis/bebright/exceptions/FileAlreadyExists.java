package hr.fer.finaltheisis.bebright.exceptions;

public class FileAlreadyExists extends RuntimeException {
    public FileAlreadyExists() {
    }

    public FileAlreadyExists(String message) {
        super(message);
    }
}
