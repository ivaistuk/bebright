package hr.fer.finaltheisis.bebright.dtos.assignement;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Timestamp;

public class CreateAssignmentDTO {
    @NotNull
    private Long classroomId;
    @NotBlank
    @Size(min = 1, max = 150)
    private String title;

    private Timestamp activeUntil;
    private String description;
    private String fileName;
    private Long points;

    public CreateAssignmentDTO() {}

    public CreateAssignmentDTO(Long classroomId, String title, Timestamp activeUntil, String description, String fileName, Long points) {
        this.classroomId = classroomId;
        this.title = title;
        this.activeUntil = activeUntil;
        this.description = description;
        this.fileName = fileName;
        this.points = points;
    }

    public Long getClassroomId() {
        return classroomId;
    }

    public void setClassroomId(Long classroomId) {
        this.classroomId = classroomId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Timestamp getActiveUntil() {
        return activeUntil;
    }

    public void setActiveUntil(Timestamp activeUntil) {
        this.activeUntil = activeUntil;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Long getPoints() {
        return points;
    }

    public void setPoints(Long points) {
        this.points = points;
    }
}
