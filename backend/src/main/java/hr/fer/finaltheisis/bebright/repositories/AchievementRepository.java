package hr.fer.finaltheisis.bebright.repositories;

import hr.fer.finaltheisis.bebright.models.Achievement;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AchievementRepository extends JpaRepository<Achievement, Long> {
}
