package hr.fer.finaltheisis.bebright.controllers;

import hr.fer.finaltheisis.bebright.dtos.user.CreateUserDTO;
import hr.fer.finaltheisis.bebright.dtos.user.UpdateUserDTO;
import hr.fer.finaltheisis.bebright.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;

@RestController
@RequestMapping("/api/user")
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/create")
    public ResponseEntity<?> createUser(@Valid @RequestBody CreateUserDTO dto) {
        userService.createUser(dto);
        return ResponseEntity.status(HttpStatus.CREATED).body("Successful registration!");
    }

    @PutMapping("/update")
    public ResponseEntity<?> updateUser(@Valid @RequestBody UpdateUserDTO dto) {
        userService.updateUser(dto);
        return ResponseEntity.ok("Successful update!");
    }

    @PutMapping("/delete/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable Long id) {
        userService.deleteUser(id);
        return ResponseEntity.ok("Successfully delete user!");
    }

    @GetMapping("/me")
    public ResponseEntity<?> getCurrentUser(Principal principal) {
        return ResponseEntity.ok(userService.findUserByUsername(principal.getName()));
    }

    @GetMapping("/assignments")
    public ResponseEntity<?> getUserAssignments(Principal principal) {
        return ResponseEntity.ok(userService.findAllUserAssignments(principal));
    }

    @GetMapping("/classrooms")
    public ResponseEntity<?> getUserClassrooms(Principal principal) {
        return ResponseEntity.ok(userService.findAllClassroomsOfCurrentUser(principal));
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getUser(@PathVariable("id") Long id) {
        return ResponseEntity.ok(userService.findUserById(id));
    }

    @GetMapping("/{id}/badges")
    public ResponseEntity<?> getUserBadges(@PathVariable("id") Long id) {
        return ResponseEntity.ok(userService.findAllBadgesOfStudent(id));
    }

    @PostMapping("/search/student")
    public ResponseEntity<?> findStudents(@RequestBody String keyword) {
        return ResponseEntity.ok(userService.findStudents(keyword));
    }

    @PostMapping("/search/organizer")
    public ResponseEntity<?> findOrganizers(@RequestBody String keyword) {
        return ResponseEntity.ok(userService.findOrganizers(keyword));
    }

    @GetMapping("/submissions")
    public ResponseEntity<?> getUserSubmissions(Principal principal) {
        return ResponseEntity.ok(userService.findAllUserSubmissions(principal));
    }

    @GetMapping("/students/ranked")
    public ResponseEntity<?> getStudentsRanked() {
        return ResponseEntity.ok(userService.getAllStudentsRanked());
    }

    @GetMapping("/{id}/experience")
    public ResponseEntity<?> getUserExperience(@PathVariable Long id) {
        return ResponseEntity.ok(userService.calculateUserExperience(userService.findUserById(id)));
    }

    @PostMapping("/{id}/add/badge")
    public ResponseEntity<?> addStudentToClassroom(@PathVariable("id") Long userId, @RequestBody Long id) {
        userService.addBadgeToUser(userId, id);
        return ResponseEntity.ok("Student successfully added to classroom.");
    }
}
