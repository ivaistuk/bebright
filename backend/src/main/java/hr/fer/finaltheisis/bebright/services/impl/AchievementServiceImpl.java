package hr.fer.finaltheisis.bebright.services.impl;

import hr.fer.finaltheisis.bebright.models.Achievement;
import hr.fer.finaltheisis.bebright.models.Badge;
import hr.fer.finaltheisis.bebright.models.User;
import hr.fer.finaltheisis.bebright.repositories.AchievementRepository;
import hr.fer.finaltheisis.bebright.services.AchievementService;
import hr.fer.finaltheisis.bebright.services.BadgeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AchievementServiceImpl implements AchievementService {

    private final AchievementRepository achievementRepository;
    private final BadgeService badgeService;

    @Autowired
    public AchievementServiceImpl(AchievementRepository achievementRepository, BadgeService badgeService) {
        this.achievementRepository = achievementRepository;
        this.badgeService = badgeService;
    }

    @Override
    public Achievement addBadgeToUser(Badge badge, User user) {
        return achievementRepository.save(new Achievement(badge, user));
    }

    @Override
    public Long getPointsRequired(Long level) {
        return 25 * level * (1 + level);
    }

    @Override
    public Long getLevel(Long experience) {
        return ((long) (Math.floor(Math.sqrt(625 + 100 * experience) - 25))) / 50;
    }

    @Override
    public List<Badge> getAllBadges() {
        return badgeService.findAllBadges();
    }


}
