package hr.fer.finaltheisis.bebright.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.sql.Timestamp;
import java.util.Set;

@Entity
@Table(name = "badges")
public class Badge {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "badge_id")
    private Long id;

    @NotEmpty
    @Column(name = "badge_name")
    private String name;

    @Column(name = "badge_icon")
    private byte[] icon;

    @CreationTimestamp
    @Column(name = "badge_created_ts")
    private Timestamp created;

    @UpdateTimestamp
    @Column(name = "badge_modified_ts")
    private Timestamp modified;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "badge")
    private Set<Achievement> achievements;

    public Badge() {}

    public Badge(Long id, byte[] icon) {
        this.id = id;
        this.icon = icon;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public byte[] getIcon() {
        return icon;
    }

    public void setIcon(byte[] icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    public Timestamp getModified() {
        return modified;
    }

    public void setModified(Timestamp modified) {
        this.modified = modified;
    }

    public Set<Achievement> getAchievements() {
        return achievements;
    }

    public void setAchievements(Set<Achievement> achievements) {
        this.achievements = achievements;
    }
}
