package hr.fer.finaltheisis.bebright.services;

import hr.fer.finaltheisis.bebright.dtos.classroom.CreateFileDTO;
import hr.fer.finaltheisis.bebright.models.Classroom;
import hr.fer.finaltheisis.bebright.models.File;
import org.springframework.stereotype.Service;

@Service
public interface FileService {

    File createFile(CreateFileDTO dto, Classroom classroom);
    void deleteFile(Long id);
    File findFileById(Long id);
}
