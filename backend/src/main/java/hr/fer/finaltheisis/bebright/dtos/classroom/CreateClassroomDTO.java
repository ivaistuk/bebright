package hr.fer.finaltheisis.bebright.dtos.classroom;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class CreateClassroomDTO {

    @NotBlank
    @Size(min = 2, max = 150)
    private String name;
    private byte[] picture;

    public CreateClassroomDTO(String name, byte[] picture) {
        this.name = name;
        this.picture = picture;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }
}
