package hr.fer.finaltheisis.bebright.services.impl;

import hr.fer.finaltheisis.bebright.models.ClassOrganization;
import hr.fer.finaltheisis.bebright.models.ClassParticipation;
import hr.fer.finaltheisis.bebright.models.Classroom;
import hr.fer.finaltheisis.bebright.models.User;
import hr.fer.finaltheisis.bebright.repositories.ClassOrganizationRepository;
import hr.fer.finaltheisis.bebright.services.ClassOrganizationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClassOrganizationServiceImpl implements ClassOrganizationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClassOrganizationServiceImpl.class);

    private ClassOrganizationRepository classOrganizationRepository;

    @Autowired
    public ClassOrganizationServiceImpl(ClassOrganizationRepository classOrganizationRepository) {
        this.classOrganizationRepository = classOrganizationRepository;
    }

    @Override
    public void addUserAsClassOrganizer(Classroom classroom, User user) {
        ClassOrganization organization =  classOrganizationRepository.findByClassroomAndOrganizer(classroom, user);
        if(organization != null) return;
        LOGGER.info("Creating class organization record.");
        classOrganizationRepository.save(new ClassOrganization(classroom, user));
    }

    @Override
    public void removeUserFromClassOrganization(Classroom classroom, User user) {
        LOGGER.info("Removing class organization record.");
        ClassOrganization organization = classOrganizationRepository.findByClassroomAndOrganizer(classroom, user);
        classOrganizationRepository.delete(organization);
    }
}
