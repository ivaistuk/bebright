package hr.fer.finaltheisis.bebright.services.impl;

import hr.fer.finaltheisis.bebright.exceptions.NoSuchEntityException;
import hr.fer.finaltheisis.bebright.models.Badge;
import hr.fer.finaltheisis.bebright.repositories.BadgeRepository;
import hr.fer.finaltheisis.bebright.services.BadgeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BadgeServiceImpl implements BadgeService {
    private final BadgeRepository badgeRepository;

    @Autowired
    public BadgeServiceImpl(BadgeRepository badgeRepository) {
        this.badgeRepository = badgeRepository;
    }

    @Override
    public Badge findBadgeById(Long id) {
        return badgeRepository.findById(id)
                .orElseThrow(() -> new NoSuchEntityException("Ne postoji značka sa danom id-om!"));
    }

    @Override
    public List<Badge> findAllBadges() {
        return badgeRepository.findAll();
    }
}
