package hr.fer.finaltheisis.bebright.services;

import hr.fer.finaltheisis.bebright.models.ClassParticipation;
import hr.fer.finaltheisis.bebright.models.Classroom;
import hr.fer.finaltheisis.bebright.models.User;
import org.springframework.stereotype.Service;

@Service
public interface ClassParticipationService {
    void addUserAsClassParticipant(Classroom classroom, User user);
    void removeUserFromClassParticipation(Classroom classroom, User user);
}
