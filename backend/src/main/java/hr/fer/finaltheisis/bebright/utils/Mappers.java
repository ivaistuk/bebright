package hr.fer.finaltheisis.bebright.utils;

import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Mappers {

    public static <T, R> Set<T> mapSetWithMapper(Set<R> set, Function<R, T> mapper) {
        return set.stream().map(mapper).collect(Collectors.toSet());
    }

    public static <T, R> List<T> mapListWithMapper(Set<R> set, Function<R, T> mapper) {
        return set.stream().map(mapper).collect(Collectors.toList());
    }
}
