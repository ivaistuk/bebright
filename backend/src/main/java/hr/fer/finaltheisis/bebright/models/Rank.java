package hr.fer.finaltheisis.bebright.models;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "ranks")
public class Rank {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "rank_id")
    private Long id;

    @Column(name = "rank_title", nullable = false)
    private String title;

    @Column(name = "rank_icon")
    private byte[] icon;

    @Column(name = "rank_level_required", nullable = false, unique = true)
    private Long levelRequired;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "rank")
    private Set<User> users;

    public Rank() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getLevelRequired() {
        return levelRequired;
    }

    public void setLevelRequired(Long levelRequired) {
        this.levelRequired = levelRequired;
    }

    public Set<User> getUsers() { return users; }

    public void setUsers(Set<User> usersHavingRank) { this.users = usersHavingRank; }

    public byte[] getIcon() {
        return icon;
    }

    public void setIcon(byte[] icon) {
        this.icon = icon;
    }
}
