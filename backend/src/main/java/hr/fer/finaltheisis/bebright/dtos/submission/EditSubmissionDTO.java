package hr.fer.finaltheisis.bebright.dtos.submission;

import javax.validation.constraints.NotNull;

public class EditSubmissionDTO {

    @NotNull
    private Long id;
    private Long points;

    public EditSubmissionDTO(Long id, Long points) {
        this.id = id;
        this.points = points;
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public Long getPoints() {
        return points;
    }
    public void setPoints(Long points) {
        this.points = points;
    }
}
