package hr.fer.finaltheisis.bebright.services;

import hr.fer.finaltheisis.bebright.models.Role;
import org.springframework.stereotype.Service;

@Service
public interface RoleService {
    Role findRoleByName(String name);
}
