package hr.fer.finaltheisis.bebright.services;

import hr.fer.finaltheisis.bebright.models.Badge;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface BadgeService {

    Badge findBadgeById(Long id);
    List<Badge> findAllBadges();
}
