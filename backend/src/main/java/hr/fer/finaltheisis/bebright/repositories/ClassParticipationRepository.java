package hr.fer.finaltheisis.bebright.repositories;

import hr.fer.finaltheisis.bebright.models.ClassParticipation;
import hr.fer.finaltheisis.bebright.models.Classroom;
import hr.fer.finaltheisis.bebright.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClassParticipationRepository extends JpaRepository<ClassParticipation, Long> {
    ClassParticipation findByClassroomAndStudent(Classroom classroom, User student);
}
