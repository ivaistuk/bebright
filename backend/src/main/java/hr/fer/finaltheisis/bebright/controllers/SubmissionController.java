package hr.fer.finaltheisis.bebright.controllers;
import hr.fer.finaltheisis.bebright.dtos.submission.CreateSubmissionDTO;
import hr.fer.finaltheisis.bebright.dtos.submission.EditSubmissionDTO;
import hr.fer.finaltheisis.bebright.services.SubmissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
@RestController
@RequestMapping("/api/submission")
public class SubmissionController {

    private SubmissionService submissionService;

    @Autowired
    public SubmissionController(SubmissionService submissionService) {
        this.submissionService = submissionService;
    }


    @GetMapping("/{id}")
    public ResponseEntity<?> getSubmission(@PathVariable Long id) {
        return ResponseEntity.ok(submissionService.findSubmissionById(id));
    }

    @PostMapping("/create")
    public ResponseEntity<?> createSubmission(@RequestBody CreateSubmissionDTO dto, Principal principal) {
        return ResponseEntity.status(HttpStatus.CREATED).body(submissionService.createSubmission(dto, principal));
    }

    @PostMapping("/{id}/file/upload")
    public ResponseEntity<?> saveSubmissionFile(@RequestBody byte[] file, @PathVariable Long id) {
        submissionService.saveSubmissionFile(file, id);
        return ResponseEntity.ok("File stored");
    }

    @GetMapping("/{id}/file/download")
    public ResponseEntity<Resource> downloadSubmissionFile(@PathVariable Long id) {
        return submissionService.getSubmissionFileResource(id);
    }

    @PutMapping("/update")
    public ResponseEntity<?> updateSubmission(@Valid @RequestBody EditSubmissionDTO dto) {
        submissionService.updateSubmission(dto);
        return ResponseEntity.ok("Submission successfully updated");
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteSubmission(@PathVariable Long id) {
        submissionService.deleteSubmission(id);
        return ResponseEntity.ok("Submission successfully deleted");
    }

    @GetMapping("/assignment/{id}")
    public ResponseEntity<?> getAssignmentSubmissions(@PathVariable("id") Long assignmentId) {
        return ResponseEntity.ok(submissionService.getAssignmentSubmissions(assignmentId));
    }

    @GetMapping("/assignment/{id}/user")
    public ResponseEntity<?> getUserSubmissions(@PathVariable("id") Long assignmentId, Principal principal) {
        return ResponseEntity.ok(submissionService.getUserSubmissions(assignmentId, principal));
    }
    @GetMapping("/ungraded")
    public ResponseEntity<?> getEducatorUngradedSubmissions(Principal principal) {
        return ResponseEntity.ok(submissionService.findUngradedEducatorSubmissions(principal));
    }

    @GetMapping("/graded")
    public ResponseEntity<?> getEducatorGradedSubmissions(Principal principal) {
        return ResponseEntity.ok(submissionService.findGradedEducatorSubmissions(principal));
    }
}
