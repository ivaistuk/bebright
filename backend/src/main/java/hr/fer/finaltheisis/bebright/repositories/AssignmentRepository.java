package hr.fer.finaltheisis.bebright.repositories;

import hr.fer.finaltheisis.bebright.models.Assignment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AssignmentRepository extends JpaRepository<Assignment, Long> {
}
