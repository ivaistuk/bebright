package hr.fer.finaltheisis.bebright.services.impl;

import hr.fer.finaltheisis.bebright.models.ClassParticipation;
import hr.fer.finaltheisis.bebright.models.Classroom;
import hr.fer.finaltheisis.bebright.models.User;
import hr.fer.finaltheisis.bebright.repositories.ClassParticipationRepository;
import hr.fer.finaltheisis.bebright.services.ClassParticipationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClassParticipationServiceImpl implements ClassParticipationService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ClassParticipationServiceImpl.class);
    private ClassParticipationRepository classParticipationRepository;

    @Autowired
    public ClassParticipationServiceImpl(ClassParticipationRepository classParticipationRepository) {
        this.classParticipationRepository = classParticipationRepository;
    }

    @Override
    public void addUserAsClassParticipant(Classroom classroom, User user) {
        ClassParticipation participation =  classParticipationRepository.findByClassroomAndStudent(classroom, user);
        if(participation != null) return;
        LOGGER.info("Creating class participation record.");
        classParticipationRepository.save(new ClassParticipation(classroom, user));
    }

    @Override
    public void removeUserFromClassParticipation(Classroom classroom, User user) {
        LOGGER.info("Removing class participation record.");
        ClassParticipation participation =  classParticipationRepository.findByClassroomAndStudent(classroom, user);
        classParticipationRepository.delete(participation);
    }
}
