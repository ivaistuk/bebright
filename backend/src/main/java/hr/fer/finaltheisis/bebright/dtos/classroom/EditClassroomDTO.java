package hr.fer.finaltheisis.bebright.dtos.classroom;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class EditClassroomDTO {

    @NotNull
    private Long id;

    @NotBlank
    @Size(min = 2, max = 150)
    private String name;
    private byte[] picture;

    public EditClassroomDTO(Long id, String name, byte[] picture) {
        this.id = id;
        this.name = name;
        this.picture = picture;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }
}
