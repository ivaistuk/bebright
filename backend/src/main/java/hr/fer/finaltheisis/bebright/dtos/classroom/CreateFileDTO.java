package hr.fer.finaltheisis.bebright.dtos.classroom;

public class CreateFileDTO {

    private String fileName;
    private String folder;

    public CreateFileDTO(String fileName, String folder) {
        this.fileName = fileName;
        this.folder = folder;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFolder() {
        return folder;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }
}
