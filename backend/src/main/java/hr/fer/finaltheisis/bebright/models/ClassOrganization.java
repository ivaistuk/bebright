package hr.fer.finaltheisis.bebright.models;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "class_organizations")
public class ClassOrganization {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "class_organization_id")
    private Long id;

    @CreationTimestamp
    @Column(name = "class_organization_created_ts")
    private Timestamp created;

    @UpdateTimestamp
    @Column(name = "class_organization_modified_ts")
    private Timestamp modified;

    @Column(name = "class_organization_archived_ts")
    private Timestamp archived = null;

    @ManyToOne
    @JoinColumn(name = "organizer_id", referencedColumnName = "user_id", nullable = false)
    private User organizer;

    @ManyToOne
    @JoinColumn(name = "classroom_id", nullable = false)
    private Classroom classroom;

    public ClassOrganization() {}

    public ClassOrganization(Classroom classroom, User user) {
        this.organizer = user;
        this.classroom = classroom;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    public Timestamp getModified() {
        return modified;
    }

    public void setModified(Timestamp modified) {
        this.modified = modified;
    }

    public Timestamp getArchived() {
        return archived;
    }

    public void setArchived(Timestamp archived) {
        this.archived = archived;
    }

    public User getOrganizer() {
        return organizer;
    }

    public void setOrganizer(User organizer) {
        this.organizer = organizer;
    }

    public Classroom getClassroom() {
        return classroom;
    }

    public void setClassroom(Classroom classroom) {
        this.classroom = classroom;
    }
}
