package hr.fer.finaltheisis.bebright.exceptions;

public class CouldNotOpenFileException extends RuntimeException {
    public CouldNotOpenFileException() {
    }

    public CouldNotOpenFileException(String message) {
        super(message);
    }
}
