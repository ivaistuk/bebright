package hr.fer.finaltheisis.bebright.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private Long id;

    @NotEmpty
    @Column(name = "user_name")
    private String name;

    @NotEmpty
    @Column(name = "user_family_name")
    private String familyName;

    @NotEmpty
    @Column(name = "user_email", unique = true)
    private String email;

    @NotEmpty
    @Column(name = "user_username", unique = true)
    private String username;

    @Column(name = "user_birth")
    private Date birthDate;

    @Column(name = "user_picture")
    private byte[] picture;

    @NotEmpty
    @Column(name = "user_password")
    @JsonIgnore
    private String password;

    @CreationTimestamp
    @Column(name = "user_created_ts")
    @JsonIgnore
    private Timestamp created;

    @UpdateTimestamp
    @Column(name = "user_modified_ts")
    @JsonIgnore
    private Timestamp modified;

    @Column(name = "user_archived_ts")
    @JsonIgnore
    private Timestamp archived = null;

    @ManyToOne
    @JoinColumn(name = "role_id", nullable = false)
    @JsonIgnoreProperties(value = "users")
    private Role role;

    @Column(name = "user_level")
    private Long level;

    @ManyToOne
    @JoinColumn(name = "rank_id")
    @JsonIgnoreProperties(value = "users")
    private Rank rank;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    private Set<Achievement> achievements;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "organizer")
    private Set<Assignment> assignments;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "student")
    private Set<Submission> submissions;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "student")
    private Set<ClassParticipation> participations;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "organizer")
    private Set<ClassOrganization> organizations;

    public User() {}

    public User(@NotEmpty String name, @NotEmpty String familyName, @NotEmpty String email,
                @NotEmpty String username, Date birthDate, byte[] picture, @NotEmpty String password,
                Role role, Long level, Rank rank) {
        this.name = name;
        this.familyName = familyName;
        this.email = email;
        this.username = username;
        this.birthDate = birthDate;
        this.picture = picture;
        this.password = password;
        this.role = role;
        this.level = level;
        this.rank = rank;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    public Timestamp getModified() {
        return modified;
    }

    public void setModified(Timestamp modified) {
        this.modified = modified;
    }

    public Timestamp getArchived() {
        return archived;
    }

    public void setArchived(Timestamp archived) {
        this.archived = archived;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Long getLevel() {
        return level;
    }

    public void setLevel(Long level) {
        this.level = level;
    }

    public Rank getRank() {
        return rank;
    }

    public void setRank(Rank rank) {
        this.rank = rank;
    }

    public Set<Achievement> getAchievements() {
        return achievements;
    }

    public void setAchievements(Set<Achievement> achievements) {
        this.achievements = achievements;
    }

    public Set<Assignment> getAssignments() {
        return assignments;
    }

    public void setAssignments(Set<Assignment> assignments) {
        this.assignments = assignments;
    }

    public Set<Submission> getSubmissions() {
        return submissions;
    }

    public void setSubmissions(Set<Submission> submissions) {
        this.submissions = submissions;
    }

    public Set<ClassParticipation> getParticipations() {
        return participations;
    }

    public void setParticipations(Set<ClassParticipation> participations) {
        this.participations = participations;
    }

    public Set<ClassOrganization> getOrganizations() {
        return organizations;
    }

    public void setOrganizations(Set<ClassOrganization> organizations) {
        this.organizations = organizations;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return id.equals(user.id) && username.equals(user.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username);
    }

}
