package hr.fer.finaltheisis.bebright.exceptions;

public class IllegalRequestData extends RuntimeException {

    public IllegalRequestData() {
    }

    public IllegalRequestData(String message) {
        super(message);
    }
}
