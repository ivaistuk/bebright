package hr.fer.finaltheisis.bebright.services.impl;

import hr.fer.finaltheisis.bebright.dtos.assignement.GetAssignmentDTO;
import hr.fer.finaltheisis.bebright.dtos.user.CreateUserDTO;
import hr.fer.finaltheisis.bebright.dtos.user.RankedUserDTO;
import hr.fer.finaltheisis.bebright.dtos.user.UpdateUserDTO;
import hr.fer.finaltheisis.bebright.exceptions.*;
import hr.fer.finaltheisis.bebright.models.*;
import hr.fer.finaltheisis.bebright.repositories.UserRepository;
import hr.fer.finaltheisis.bebright.services.*;
import hr.fer.finaltheisis.bebright.utils.RoleNames;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.*;
import java.util.stream.Collectors;

import static hr.fer.finaltheisis.bebright.utils.Mappers.mapListWithMapper;
import static hr.fer.finaltheisis.bebright.utils.Mappers.mapSetWithMapper;

@Service
public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

    private final UserRepository userRepository;
    private final RoleService roleService;
    private final RankService rankService;
    private final AchievementService achievementService;
    private final BadgeService badgeService;
    private final PasswordEncoder encoder;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, RoleService roleService, RankService rankService, AchievementService achievementService, BadgeService badgeService, PasswordEncoder encoder) {
        this.userRepository = userRepository;
        this.roleService = roleService;
        this.rankService = rankService;
        this.achievementService = achievementService;
        this.badgeService = badgeService;
        this.encoder = encoder;
    }

    @Override
    public User getCurrentUser(Principal principal, String... allowedRoles) {
        User user = userRepository.findByUsername(principal.getName())
                .orElseThrow(() -> new AuthenticationException("Error while attempting user authentication"));
        if(allowedRoles != null && allowedRoles.length > 0)
            if(Arrays.stream(allowedRoles).noneMatch(role -> user.getRole().getName().equals(role)))
                throw new AuthorizationException();
        return user;
    }

    @Override
    public Set<Classroom> findAllClassroomsOfCurrentUser(Principal principal) {
        LOGGER.info("Finding all classrooms of a user: " + principal.getName());

        User user = findUserByUsername(principal.getName());
        if(user.getRole().getName().equals("STUDENT"))
            return mapSetWithMapper(user.getParticipations(), ClassParticipation::getClassroom);
        return mapSetWithMapper(user.getOrganizations(), ClassOrganization::getClassroom);
    }

    @Override
    public List<Badge> findAllBadgesOfStudent(Long id) {
        LOGGER.info("Finding all badges of a user with: " + id);

        User user = findUserById(id);
        return mapListWithMapper(user.getAchievements(), Achievement::getBadge);
    }

    @Override
    public Set<User> findStudents(String keyword) {
        LOGGER.info("Finding all students with keyword: " + keyword);
        var users = userRepository.findAll();
        return users.stream().filter(user -> user.getRole().getName().equals(RoleNames.STUDENT)
                && (user.getUsername().startsWith(keyword)
                    || user.getName().startsWith(keyword)
                        || user.getFamilyName().startsWith(keyword)))
                            .collect(Collectors.toSet());
    }

    @Override
    public Set<User> findOrganizers(String keyword) {
        LOGGER.info("Finding all organizers with keyword: " + keyword);
        var users = userRepository.findAll();
        return users.stream().filter(user -> user.getRole().getName().equals(RoleNames.EDUCATOR)
                        && (user.getUsername().startsWith(keyword)
                        || user.getName().startsWith(keyword)
                        || user.getFamilyName().startsWith(keyword)))
                .collect(Collectors.toSet());
    }

    @Override
    public Set<GetAssignmentDTO> findAllUserAssignments(Principal principal) {
        User user = findUserByUsername(principal.getName());
        if(user.getRole().getName().equals(RoleNames.EDUCATOR)) {
            LOGGER.info("Finding all assignments given by an educator with name: " + principal.getName());
            return user.getAssignments().stream().map(assignment -> new GetAssignmentDTO(assignment, user)).collect(Collectors.toSet());
        }
        LOGGER.info("Finding all assignments of a student: " + principal.getName());
        var classrooms = findAllClassroomsOfCurrentUser(principal);
        return classrooms.stream().map(Classroom::getAssignments).flatMap(Set::stream)
                .map(assignment -> new GetAssignmentDTO(assignment, user)).collect(Collectors.toSet());
    }
    private Set<Submission> userSubmissions(User user) {
        return user.getSubmissions();
    }

    @Override
    public Set<Submission> findAllUserSubmissions(Principal principal) {
        LOGGER.info("Finding all submissions of a student: " + principal.getName());
        User user = getCurrentUser(principal, RoleNames.ADMIN, RoleNames.STUDENT);
        return userSubmissions(user);
    }

    private Long getExperience(Set<Submission> submissions) {
        LOGGER.info("Calculating user experience");
        return submissions.stream().
                map(Submission::getPoints).filter(Objects::nonNull)
                .mapToLong(value -> value).sum();
    }

    @Override
    public Long calculateUserExperience(User user) {
        return getExperience(userSubmissions(user));
    }

    @Override
    public List<RankedUserDTO> getAllStudentsRanked() {
        return userRepository.findAll().stream()
                .filter(user -> user.getRole().getName().equals(RoleNames.STUDENT))
                .map(user -> new RankedUserDTO(user,
                        achievementService.getPointsRequired(user.getLevel()),
                        calculateUserExperience(user)))
                .sorted(Comparator.comparing(RankedUserDTO::getLevel)
                                  .thenComparing(RankedUserDTO::getExperience).reversed())
                .collect(Collectors.toList());
    }

    @Override
    public void addBadgeToUser(Long userId, Long id) {
        User user = findUserById(userId);
        Badge badge = badgeService.findBadgeById(id);
        achievementService.addBadgeToUser(badge, user);
    }

    @Override
    public User findUserByUsername(String username) {
        LOGGER.info("Looking for user with username: " + username);
        return userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("User with given username not found!"));
    }

    @Override
    public User findUserById(Long id) {
        LOGGER.info("Looking for user with id: " + id);
        return userRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException("User with given id not found!"));
    }

    @Override
    public void createUser(CreateUserDTO dto) {
        if(userRepository.existsByUsername(dto.getUsername()) || userRepository.existsByEmail(dto.getEmail()))
            throw new UserAlreadyExistsException("Korisničko ime je zauzeto!");

        if(dto.getUsername().contains(" "))
            throw new IllegalRequestData("Korisničko ime ne smije sadržavati prazne znakove");

        Role role = roleService.findRoleByName(dto.getRoleName());
        Rank rank = null;
        Long level = null;
        if(role.getName().equals("STUDENT")) {
            level = 1L;
            rank = rankService.findRankByLevel(level);
        }

        LOGGER.info("Storing created user");
        userRepository.save(new User(dto.getName(), dto.getFamilyName(), dto.getEmail(),
                            dto.getUsername(), dto.getBirthDate(), dto.getPicture(),
                            encoder.encode(dto.getPassword()), role, level, rank));
    }

    @Override
    public void updateUser(UpdateUserDTO dto) {
        User user = findUserById(dto.getId());
        if((!user.getUsername().equals(dto.getUsername()) && userRepository.existsByUsername(dto.getUsername()))
                || (!user.getEmail().equals(dto.getEmail()) && userRepository.existsByEmail(dto.getEmail())))
            throw new UserAlreadyExistsException("Korisničko ime je zauzeto!");

        user.setName(dto.getName());
        user.setFamilyName(dto.getFamilyName());
        user.setUsername(dto.getUsername());
        if(dto.getPassword() != null) user.setPassword(encoder.encode(dto.getPassword()));
        user.setEmail(dto.getEmail());
        user.setBirthDate(dto.getBirthDate());
        user.setPicture(dto.getPicture());
        if(dto.getLevel() != null) {
            user.setLevel(dto.getLevel());
            user.setRank(rankService.findRankByLevel(dto.getLevel()));
        }

        LOGGER.info("Updating user with username: " + user.getUsername());
        userRepository.save(user);
    }

    @Override
    public void deleteUser(Long id) {
        User user = findUserById(id);
        LOGGER.info("Deleting user with username: " + user.getUsername());
        userRepository.delete(user);
    }
}
