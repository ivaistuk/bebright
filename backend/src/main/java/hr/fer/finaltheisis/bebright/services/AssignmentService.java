package hr.fer.finaltheisis.bebright.services;

import hr.fer.finaltheisis.bebright.dtos.assignement.CreateAssignmentDTO;
import hr.fer.finaltheisis.bebright.dtos.assignement.EditAssignmentDTO;
import hr.fer.finaltheisis.bebright.models.Assignment;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.security.Principal;

@Service
public interface AssignmentService {
    Long createAssignment(CreateAssignmentDTO dto, Principal principal);
    void saveAssignmentFile(byte[] file, Long id);
    void updateAssignment(EditAssignmentDTO dto);
    void deleteAssignment(Long id);
    Assignment findAssignmentById(Long id);
    ResponseEntity<Resource> getAssignmentFileResource(Long id);
}
