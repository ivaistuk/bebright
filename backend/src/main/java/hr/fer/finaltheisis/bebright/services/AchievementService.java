package hr.fer.finaltheisis.bebright.services;

import hr.fer.finaltheisis.bebright.models.Achievement;
import hr.fer.finaltheisis.bebright.models.Badge;
import hr.fer.finaltheisis.bebright.models.User;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public interface AchievementService {
    Achievement addBadgeToUser(Badge badge, User user);
    Long getPointsRequired(Long level);
    Long getLevel(Long experience);
    List<Badge> getAllBadges();
}
