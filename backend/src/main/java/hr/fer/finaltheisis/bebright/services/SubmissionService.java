package hr.fer.finaltheisis.bebright.services;

import hr.fer.finaltheisis.bebright.dtos.submission.CreateSubmissionDTO;
import hr.fer.finaltheisis.bebright.dtos.submission.EditSubmissionDTO;
import hr.fer.finaltheisis.bebright.models.Assignment;
import hr.fer.finaltheisis.bebright.models.Submission;
import hr.fer.finaltheisis.bebright.models.User;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.Set;

@Service
public interface SubmissionService {
    Submission findSubmissionById(Long id);
    Long createSubmission(CreateSubmissionDTO dto, Principal principal);
    void saveSubmissionFile(byte[] file, Long id);
    ResponseEntity<Resource> getSubmissionFileResource(Long id);
    void updateSubmission(EditSubmissionDTO dto);
    void deleteSubmission(Long id);
    Set<Submission> getAssignmentSubmissions(Long assignmentId);
    Set<Submission> getUserSubmissions(Long assignmentId, Principal principal);
    Set<Submission> findUngradedEducatorSubmissions(Principal principal);
    Set<Submission> findGradedEducatorSubmissions(Principal principal);
}
