package hr.fer.finaltheisis.bebright.controllers;

import hr.fer.finaltheisis.bebright.services.AchievementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/achievement")
public class AchievementController {

    private final AchievementService achievementService;

    @Autowired
    public AchievementController(AchievementService achievementService) {
        this.achievementService = achievementService;
    }

    @GetMapping("/{level}")
    public ResponseEntity<?> getRequiredExperienceForLevel(@PathVariable Long level) {
        return ResponseEntity.ok(achievementService.getPointsRequired(level));
    }

    @GetMapping("/badge/all")
    public ResponseEntity<?> getAllBadges() {
        return ResponseEntity.ok(achievementService.getAllBadges());
    }

}
