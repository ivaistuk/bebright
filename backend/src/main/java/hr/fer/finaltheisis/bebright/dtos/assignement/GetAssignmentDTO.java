package hr.fer.finaltheisis.bebright.dtos.assignement;

import hr.fer.finaltheisis.bebright.models.Assignment;
import hr.fer.finaltheisis.bebright.models.Classroom;
import hr.fer.finaltheisis.bebright.models.Submission;
import hr.fer.finaltheisis.bebright.models.User;
import hr.fer.finaltheisis.bebright.utils.RoleNames;

import java.sql.Timestamp;

public class GetAssignmentDTO {

    private Long id;
    private String title;
    private String description;
    private String fileName;
    private Timestamp activeUntil;
    private Timestamp created;
    private Timestamp modified;
    private Timestamp archived;
    private Long points;
    private Classroom classroom;

    private Boolean submitted;
    private Long earned = null;

    public GetAssignmentDTO(Assignment assignment, User user) {
        id = assignment.getId();
        title = assignment.getTitle();
        description = assignment.getDescription();
        fileName = assignment.getFileName();
        activeUntil = assignment.getActiveUntil();
        created = assignment.getCreated();
        modified = assignment.getModified();
        archived = assignment.getArchived();
        points = assignment.getPoints();
        classroom = assignment.getClassroom();
        submitted = false;

        if(user.getRole().getName().equals(RoleNames.STUDENT)) {
            submitted = assignment.getSubmissions().stream().anyMatch(submission -> user.getSubmissions().contains(submission));
            if(submitted) {
                earned =  user.getSubmissions().stream()
                        .filter(submission -> submission.getPoints() != null && assignment.getSubmissions().contains(submission))
                        .map(Submission::getPoints).max(Long::compareTo).orElse(null);
            }
        }

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Timestamp getActiveUntil() {
        return activeUntil;
    }

    public void setActiveUntil(Timestamp activeUntil) {
        this.activeUntil = activeUntil;
    }

    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    public Timestamp getModified() {
        return modified;
    }

    public void setModified(Timestamp modified) {
        this.modified = modified;
    }

    public Timestamp getArchived() {
        return archived;
    }

    public void setArchived(Timestamp archived) {
        this.archived = archived;
    }

    public Long getPoints() {
        return points;
    }

    public void setPoints(Long points) {
        this.points = points;
    }

    public Boolean getSubmitted() {
        return submitted;
    }

    public void setSubmitted(Boolean submitted) {
        this.submitted = submitted;
    }

    public Classroom getClassroom() {
        return classroom;
    }

    public void setClassroom(Classroom classroom) {
        this.classroom = classroom;
    }

    public Long getEarned() {
        return earned;
    }

    public void setEarned(Long earned) {
        this.earned = earned;
    }
}
